-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 19 Sep 2018 pada 14.03
-- Versi server: 10.2.17-MariaDB
-- Versi PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u956116597_hango`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `destinations`
--

CREATE TABLE `destinations` (
  `id` int(11) NOT NULL,
  `nama_destinasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan_destinasi` text COLLATE utf8_unicode_ci NOT NULL,
  `foto_destinasi` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.png',
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `destinations`
--

INSERT INTO `destinations` (`id`, `nama_destinasi`, `keterangan_destinasi`, `foto_destinasi`, `username`, `created_at`, `updated_at`) VALUES
(2, 'N Seoul Tower', '<p><strong>Menara Seoul N</strong>&nbsp;adalah sebuah&nbsp;pemancar radio&nbsp;yang terletak di&nbsp;Seoul,&nbsp;Korea Selatan. Gedung ini dibangun pada tahun&nbsp;1969, dan dibuka untuk umum pada tahun&nbsp;1980, tinggi menara ini mencapai 236.7&nbsp;m&nbsp;(777&nbsp;kaki) dari dasar dan berada di ketinggian 479.7&nbsp;m&nbsp;(1,574&nbsp;kaki) di atas permukaan laut. Menara ini juga dinamai&nbsp;<strong>Menara Namsan</strong>&nbsp;atau&nbsp;<strong>Menara Seoul</strong>&nbsp;saja. Namun setelah pemilik menara bekerja sama dengan&nbsp;CJ Corporation, menara ini dinamai&nbsp;<em>Menara Seoul N</em>&nbsp;(nama resmi&nbsp;<strong>Menara Seoul CJ</strong>).</p>\r\n\r\n<p>Kebanyakan pengunjung menaiki&nbsp;kereta gantung&nbsp;Namsan&nbsp;untuk naik, lalu berjalan kaki sampai ke menara. Di menara ini terdapat&nbsp;toko&nbsp;oleh-oleh dan sebuah restoran di lantai bawah. Pengunjung harus membayar ketika naik ke atas menara. Terdapat empat&nbsp;balkon pengamatan&nbsp;(di balkon pengamatan ke-4, terdapat&nbsp;restoran berputar, dan berputar sekali selama 48 menit), dan juga toko oleh-oleh dan dua restoran di atasnya. Pengunjung dapat melihat hampir seluruh kota Seoul. Di dekatnya juga terdapat menara transmisi.</p>', '2-09140411.jpg', 'bagus', '2018-09-08 11:19:17', '2018-09-14 16:12:20'),
(3, 'Myeongdong', '<p><strong>Myeongdong</strong>&nbsp;atau&nbsp;<strong>Myongdong</strong>&nbsp;adalah sebuah permukiman di kota Seoul (<em>dong</em>) di divisi adminstratif&nbsp;Jung-gu, berlokasi antara&nbsp;Chungmuro,&nbsp;Euljiro, dan&nbsp;Namdaemun-ro. Wilayahnya mencakup 0,91&nbsp;km&sup2; dengan populasi 2.986 jiwa, dan sebagian besar wilayahnya adalah distrik komersil dan bisnis.</p>\r\n\r\n<p>Myeongdong adalah salah satu distrik belanja Seoul yang terbesar, banyak terdapat toko-toko yang menjual barang-barang dengan harga sedang sampai mahal dari produksi dalam negeri maupun internasional. Myeongdong juga adalah pusat&nbsp;fashion&nbsp;dan kehidupan malam dari anak-anak muda Seoul. Distrik ini mempunyai jalan kota termahal ke-9 di dunia dalam hal sewa floorspace.</p>\r\n\r\n<p>Myeongdong memiliki 4 Toserba terkemuka:&nbsp;Migliore,&nbsp;Lotte Department Store, Avatar, dan High Harriet. Wilayah ini juga adalah pusat utama dari badan jasa finansial. Beberapa perusahaan asuransi terkemuka juga berkedudukan di sini termasuk:&nbsp;Citibank,&nbsp;SK Corporation,&nbsp;Kookmin Bank,&nbsp;Korea Exchange Bank,&nbsp;Lone Star Funds,&nbsp;Sumitomo Mitsui Banking Corporation,&nbsp;AIG Korea Insurance,&nbsp;Hana Bank, dan&nbsp;HSBC. Pusat keuangan Seoul terbagi antara daerah ini dan Yeouido di mana letak Korean Stock Exchange (Bursa Efek Korea).</p>\r\n\r\n<p>Di distrik ini terdapat situs dari banyak Kedutaan Besar negara-negara sahabat, kantor pusat YWCA, badan&nbsp;UNESCO&nbsp;dan&nbsp;Katedral Myeongdong; katedral Katolik tertua di Korea. Di Myeongdong kadang-kadang bisa disaksikan unjuk rasa.</p>', '3-09140415.JPG', 'bagus', '2018-09-08 11:21:38', '2018-09-14 16:16:34'),
(4, 'K-Star Road', '<p>K-Star Road adalah proyek khusus dari Gangnam-gu untuk wisatawan asing yang mencintai budaya Korea dan hallyu ( gelombang Korea) dan para bintang. Seperti namanya, terdapata toko-toko yang sering dikunjungi oleh bintang hallyu di sekitar Cheongdam-dong, Gangnam-gu, Sinsa-dong, dan daerah Nonhyun-dong.<br />\r\n<br />\r\nDari bagian barat Apgujeong ada Galleria Department Store di sepanjang jalur 400 meter yang mengarah ke markas SM Entertainment, pengunjung bisa melihat patung lucu yang bergaya bintang K-pop terkenal seperti TVXQ, SNSD, SHINee, EXO, 4 Minute, FT Island , Super Junior, CN Blue, dan 2 PM. Tidak hanya itu, Apgujeong Rodeo Station dihiasi dengan seni grafiti, serta dinding dan lantai dihiasi dengan karya seni dan nama khusus dan lempengan dari bintang hallyu.</p>', '4-09140425.jpg', 'bagus', '2018-09-08 11:22:06', '2018-09-14 16:25:48'),
(5, 'SUM Cafe & Market', '<p>Sebagai salah satu agensi kenamaan di Korea, SM Entertainment tentunya tidak hanya pandai menjalankan bidang hiburan saja, karena nyatanya untuk urusan marketing mereka dipercaya menjadi nomor satu.</p>\r\n\r\n<p>Baru-baru ini SM membuka caf&eacute;/market baru yang terletak di tengah kota Seoul, tepatnya di daerah Chungdam. Hal ini tentunya sangat menguntungkan mereka, mengingat banyak turis yang menjadikan Seoul sebagai destinasi wisata, khususnya para Kpopers.</p>\r\n\r\n<p>Begitu masuk kita akan disambut oleh bagian caf&eacute; yang bersih dan luas, dan bukan SM namanya kalau tidak menempatkan artis mereka sebagai dekorasi untuk menyegarkan para SM-Stans.</p>\r\n\r\n<p>Karena bentuknya yang gabungan dari caf&eacute; dan&nbsp;<em>market,&nbsp;</em>tentunya kita akan banyak melihat banyak&nbsp;<em>merchandise&nbsp;</em>yang dijajakan di setiap sudut ruangan.&nbsp;</p>', '5-09140428.jpg', 'bagus', '2018-09-08 11:22:24', '2018-09-14 16:29:43'),
(6, 'JYP Entertainment', '<p><strong>JYP Entertainment</strong>&nbsp;adalah sebuah&nbsp;perusahaan rekaman&nbsp;K-pop&nbsp;yang berdiri pada tahun 1997 oleh&nbsp;Park Jin-young.Label dari salah satu tiga besar perusahaan rekaman korea bersama dengan&nbsp;YG Entertainment&nbsp;dan&nbsp;SM Entertainment&nbsp;karena pangsa pasar yang kuat dan operasi internasional. Sebuah rumah bagi artis seperti&nbsp;Rain, Park Ji YooN, dan g.o.d (semuanya sudah keluar), dan artis JYP saat ini termasuk&nbsp;Wonder Girls,&nbsp;2PM,&nbsp;Miss A, Baek Ah Yeon, Bernard Park, G.soul, Lee Sunmi, Park Jimin dan Baek Yerin&nbsp;15&amp;,&nbsp;GOT7,&nbsp;DAY6, dan&nbsp;TWICE.</p>', '6-09140433.jpg', 'bagus', '2018-09-08 11:22:40', '2018-09-14 16:33:53'),
(7, 'SMTown Coex Artrium', '', 'default.png', 'bagus', '2018-09-08 11:22:55', '2018-09-14 16:42:09'),
(9, 'Hongdae', '<p>Muda-mudi dengan pakaian bergaya, dandanan ala idola K-Pop, dan kafe-kafe lucu untuk cuci mata dapat ditemui dengan mudah di jalan Hongdae, nama lokasi yang tak asing bagi yang ingin merasakan sensasi jadi anak gaul Seoul, Korea Selatan. Hongdae sebenarnya adalah jalanan yang terletak di dekat Hongik Univeristy yang terkenal dengan jurusan seni dan arsitektur. Hongik University atau dalam bahasa korea disebut Hong Ik Dae Hag Gyo.&nbsp;</p>\r\n\r\n<p>Gerai yang menjual pakaian dan kosmetik dengan gaya anak muda Korea Selatan menjamur di sini. Selain itu ada pula kafe-kafe berdekorasi lucu yang menarik untuk disinggahi. Pilihan lain adalah jajanan kaki lima yang memang cocok untuk bujet mahasiswa.&nbsp;</p>\r\n\r\n<p>Makin malam, jalan Hongdae semakin ramai. Uniknya beragam pemandangan ala anak muda akan terlihat di sini. Mulai dari romansa muda-mudi yang memadu kasih di tengah jalan, peramal jalanan yang ramai diserbu anak muda, sampai pemuda yang muntah karena mabuk sehabis minum alkohol, semua dapat dilihat di jalan Hongdae. Berkunjung ke jalan Hongdae serasa nostalgia masa muda yang penuh keceriaan.&nbsp;</p>', '9-09140443.jpg', 'bagus', '2018-09-08 11:23:28', '2018-09-14 16:45:29'),
(10, 'Gyeongbuk Palace', '<p><strong>Istana Gyeongbok</strong>&nbsp;adalah sebuah istana yang terletak di sebelah utara kota&nbsp;Seoul&nbsp;(Gangbuk),&nbsp;Korea Selatan. Istana ini termasuk dari 5 istana besar dan merupakan yang terbesar yang dibangun oleh&nbsp;Dinasti Joseon.</p>\r\n\r\n<p>Istana Gyeongbok aslinya didirikan tahun 1394 oleh&nbsp;Jeong do jeon, seorang arsitek. Istana ini hancur pada saat&nbsp;invasi Jepang ke Korea&nbsp;tahun&nbsp;1592-1598&nbsp;dan dibangun lagi selama tahun 1860-an dengan 330 buah komplek bangunan dengan 5.792 kamar. Berdiri di wilayah seluas 410.000 meter persegi, Istana Gyeongbok adalah simbol keagungan kerajaan dan rakyat Korea. Setelah pembunuhan&nbsp;Maharani Myeongseong&nbsp;oleh&nbsp;mata-mata&nbsp;Jepang&nbsp;pada tahun&nbsp;1895,&nbsp;Raja Gojong&nbsp;meninggalkan istana ini bersama anggota keluarganya yang lain dan tidak akan pernah kembali.</p>\r\n\r\n<p>Pada tahun&nbsp;1911, pemerintahan Jepang yang sedang menjajah Korea menghancurkan semua bangunannya kecuali 10 bangunan utama, dan membangun Bangunan Pemerintahan Utama Jepang untuk gubernur jenderal Korea di depan Ruangan Tahta.</p>\r\n\r\n<p>Bangunan utama dari Istana Gyeongbok termasuk&nbsp;Geunjeongjeon, Ruangan Tahta Raja (yang merupakan harta nasional Korea Selatan nomor 223) dan&nbsp;Paviliun Gyeonghoeru&nbsp;(harta nasional nomor 224) yang memiliki kolam bunga teratai dan bertiangkan 48 buah tonggak granit.</p>\r\n\r\n<p>Istana Gyeongbok saat ini dibuka untuk umum dan Museum Nasional Rakyat Korea (National Folk Museum of Korea) berdiri di dalamnya.</p>\r\n\r\n<p>Banyak rakyat Korea yang berharap pemerintahnya dapat mengembalikan bentuk asli istana. Berkat kerja keras arkeolog, 330 bangunan berhasil dibangun kembali. Saat ini gerbang masuk istana (Gwanghwamun) sedang direnovasi untuk dibuat kembali seperti pada asalnya dan diperkirakan selesai tahun 2009.</p>', '10-09140446.jpg', 'bagus', '2018-09-08 11:23:42', '2018-09-14 16:47:12'),
(11, 'Gwanghwamun', '', 'default.png', 'bagus', '2018-09-08 11:23:53', '2018-09-08 11:27:47'),
(12, 'Bukchon Village', '', 'default.png', 'bagus', '2018-09-08 11:24:09', '2018-09-14 16:54:01'),
(13, 'Inha University', '', 'default.png', 'bagus', '2018-09-08 11:24:22', '2018-09-14 16:54:30'),
(14, 'Deoksugung Palace', '', 'default.png', 'bagus', '2018-09-08 11:24:40', '2018-09-08 11:24:44'),
(15, 'Han River', '', 'default.png', 'bagus', '2018-09-08 11:24:52', '2018-09-08 11:24:56'),
(16, 'Meeting Point at Incheon Airport', '', 'default.png', 'bagus', '2018-09-08 11:25:05', '2018-09-08 11:25:10'),
(17, 'Freeday', '', 'default.png', 'bagus', '2018-09-08 11:25:24', '2018-09-08 11:25:29'),
(18, 'Back to Airport', '', 'default.png', 'bagus', '2018-09-08 11:25:36', '2018-09-08 11:25:40'),
(20, 'Mall', '', 'default.png', 'bagus', '2018-09-08 11:28:37', '2018-09-08 11:28:41'),
(21, 'Namiseom Island', '', 'default.png', 'bagus', '2018-09-08 11:28:59', '2018-09-14 16:55:47'),
(22, 'Banpo Bridge', '', 'default.png', 'bagus', '2018-09-08 11:29:23', '2018-09-08 11:29:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL,
  `nama_galeri` varchar(255) NOT NULL DEFAULT 'Liburan bersama Haneul Go!',
  `jenis` varchar(15) NOT NULL DEFAULT 'Traveling',
  `foto_galeri` varchar(125) NOT NULL,
  `username` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `galleries`
--

INSERT INTO `galleries` (`id`, `nama_galeri`, `jenis`, `foto_galeri`, `username`, `created_at`, `updated_at`) VALUES
(1, 'Alvina - Greeting Card Namsan Tower', 'Greeting Card', '1-09120943.jpeg', 'bagus', '2018-09-12 09:43:32', '2018-09-12 09:43:32'),
(2, 'Chaca Park - Greeting Card Namsan Tower', 'Greeting Card', '2-09120944.jpeg', 'bagus', '2018-09-12 09:44:06', '2018-09-12 09:44:06'),
(3, 'Chriisen_nona - Greeting Card Namsan Tower', 'Greeting Card', '3-09120944.jpeg', 'bagus', '2018-09-12 09:44:24', '2018-09-12 09:44:24'),
(4, 'Citra Wanodaya - Greeting Card Namsan Tower', 'Greeting Card', '4-09120944.jpeg', 'bagus', '2018-09-12 09:44:49', '2018-09-12 09:44:49'),
(5, 'Citra Wanodaya - Greeting Card Namsan Tower', 'Greeting Card', '5-09120944.jpeg', 'bagus', '2018-09-12 09:44:54', '2018-09-12 09:44:54'),
(6, 'Dhanti - Greeting Card Namsan Tower', 'Greeting Card', '6-09120945.jpeg', 'bagus', '2018-09-12 09:45:10', '2018-09-12 09:45:10'),
(7, 'Diah - Greeting Card Namsan Tower', 'Greeting Card', '7-09120945.jpeg', 'bagus', '2018-09-12 09:45:18', '2018-09-12 09:45:18'),
(8, 'Fanny Anindya Putri - Greeting Card Namsan Tower', 'Greeting Card', '8-09120945.jpeg', 'bagus', '2018-09-12 09:45:35', '2018-09-12 09:45:35'),
(9, 'Fanny Anindya Putri - Greeting Card Namsan Tower', 'Greeting Card', '9-09120945.jpeg', 'bagus', '2018-09-12 09:45:36', '2018-09-12 09:45:36'),
(10, 'Faraphudtunembahpudji - Greeting Card Namsan Tower', 'Greeting Card', '10-09120946.jpeg', 'bagus', '2018-09-12 09:46:01', '2018-09-12 09:46:01'),
(11, 'Faraphudtunembahpudji - Greeting Card Namsan Tower', 'Greeting Card', '11-09120946.jpeg', 'bagus', '2018-09-12 09:46:05', '2018-09-12 09:46:05'),
(12, 'Gvacsa Delima - Greeting Card Namsan Tower', 'Greeting Card', '12-09120946.jpeg', 'bagus', '2018-09-12 09:46:24', '2018-09-12 09:46:24'),
(13, 'Husnul Khotimah - Greeting Card Namsan Tower', 'Greeting Card', '13-09120946.jpeg', 'bagus', '2018-09-12 09:46:43', '2018-09-12 09:46:43'),
(14, 'Ika Prayasti - Greeting Card Namsan Tower', 'Greeting Card', '14-09120946.jpeg', 'bagus', '2018-09-12 09:46:57', '2018-09-12 09:46:57'),
(15, 'Ika Prayasti - Greeting Card Namsan Tower', 'Greeting Card', '15-09120947.jpeg', 'bagus', '2018-09-12 09:47:04', '2018-09-12 09:47:04'),
(16, 'Indah Sari - Greeting Card Namsan Tower', 'Greeting Card', '16-09120947.jpeg', 'bagus', '2018-09-12 09:47:20', '2018-09-12 09:47:20'),
(17, 'Kimda_ - Greeting Card Namsan Tower', 'Greeting Card', '17-09120947.jpeg', 'bagus', '2018-09-12 09:47:33', '2018-09-12 09:47:33'),
(18, 'Miftakhul Janah - Greeting Card Namsan Tower', 'Greeting Card', '18-09120947.jpeg', 'bagus', '2018-09-12 09:47:50', '2018-09-12 09:47:50'),
(19, 'Nadya Uzara - Greeting Card Namsan Tower', 'Greeting Card', '19-09120948.jpeg', 'bagus', '2018-09-12 09:48:09', '2018-09-12 09:48:09'),
(20, 'Nadya Uzara - Greeting Card Namsan Tower', 'Greeting Card', '20-09120948.jpeg', 'bagus', '2018-09-12 09:48:15', '2018-09-12 09:48:15'),
(21, 'Nashwa Aulia - Greeting Card Namsan Tower', 'Greeting Card', '21-09120948.jpeg', 'bagus', '2018-09-12 09:48:29', '2018-09-12 09:48:29'),
(22, 'Ndv_932 - Greeting Card Namsan Tower', 'Greeting Card', '22-09120948.jpeg', 'bagus', '2018-09-12 09:48:42', '2018-09-12 09:48:42'),
(23, 'Ndv_932 - Greeting Card Namsan Tower', 'Greeting Card', '23-09120948.jpeg', 'bagus', '2018-09-12 09:48:42', '2018-09-12 09:48:42'),
(24, 'Raisa_w1 - Greeting Card Namsan Tower', 'Greeting Card', '24-09120948.jpeg', 'bagus', '2018-09-12 09:48:56', '2018-09-12 09:48:56'),
(25, 'Rikeu Relawati - Greeting Card Namsan Tower', 'Greeting Card', '25-09120949.jpeg', 'bagus', '2018-09-12 09:49:08', '2018-09-12 09:49:08'),
(26, 'S. Vitta - Greeting Card Namsan Tower', 'Greeting Card', '26-09120949.jpeg', 'bagus', '2018-09-12 09:49:19', '2018-09-12 09:49:19'),
(27, 'Salwa Nirrma Fitry - Greeting Card Namsan Tower', 'Greeting Card', '27-09120949.jpeg', 'bagus', '2018-09-12 09:49:33', '2018-09-12 09:49:33'),
(28, 'Uke - Greeting Card Namsan Tower', 'Greeting Card', '28-09120949.jpeg', 'bagus', '2018-09-12 09:49:45', '2018-09-12 09:49:45'),
(29, 'Vidya Apriliarf - Greeting Card Namsan Tower', 'Greeting Card', '29-09120950.jpeg', 'bagus', '2018-09-12 09:50:08', '2018-09-12 09:50:08'),
(30, 'Winda Novia - Greeting Card Namsan Tower', 'Greeting Card', '30-09120950.jpeg', 'bagus', '2018-09-12 09:50:52', '2018-09-12 09:50:52'),
(31, 'Yayu Canis - Greeting Card Namsan Tower', 'Greeting Card', '31-09120951.jpeg', 'bagus', '2018-09-12 09:51:06', '2018-09-12 09:51:06'),
(32, 'Order pre-launch Haneul Shop', 'Haneul Shop', '32-09170918.jpg', 'bagus', '2018-09-17 09:18:26', '2018-09-17 09:18:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurs`
--

CREATE TABLE `kurs` (
  `kode_kurs` int(2) NOT NULL,
  `country_from` varchar(3) NOT NULL,
  `country_to` varchar(3) NOT NULL,
  `kurs` decimal(20,9) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kurs`
--

INSERT INTO `kurs` (`kode_kurs`, `country_from`, `country_to`, `kurs`, `created_at`, `updated_at`) VALUES
(1, 'KRW', 'IDR', '12.757900000', '2018-07-16 07:24:39', NULL),
(2, 'IDR', 'KRW', '0.078382700', '2018-07-16 07:24:39', NULL),
(3, 'KRW', 'USD', '0.000886381', '2018-07-16 07:24:39', NULL),
(4, 'USD', 'KRW', '1128.180000000', '2018-07-16 07:24:39', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `read_pengguna` tinyint(1) NOT NULL COMMENT '0=Belum Dibaca, 1=Sudah Dibaca, 2=Dihapus',
  `read_admin` tinyint(1) NOT NULL COMMENT '0=Belum Dibaca, 1=Sudah Dibaca, 2=Dihapus',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `messages`
--

INSERT INTO `messages` (`id`, `username`, `read_pengguna`, `read_admin`, `created_at`, `updated_at`) VALUES
(9, 'bagus', 1, 1, '2018-09-10 16:30:39', '2018-09-10 16:34:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `message_details`
--

CREATE TABLE `message_details` (
  `id` int(11) NOT NULL,
  `kode_pesan` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `pesan` text NOT NULL,
  `status_user` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=Belum dihapus, 2=Dihapus',
  `status_admin` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=Belum dihapus, 2=Dihapus',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `message_details`
--

INSERT INTO `message_details` (`id`, `kode_pesan`, `username`, `pesan`, `status_user`, `status_admin`, `created_at`, `updated_at`) VALUES
(26, 9, 'bagus', 'lolo', 1, 1, '2018-09-10 16:30:40', '2018-09-10 16:30:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `kode_order` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `jumlah_orang` int(11) NOT NULL,
  `kode_paket` int(11) NOT NULL,
  `harga_paket` int(11) NOT NULL,
  `fitur_tambahan` varchar(150) NOT NULL,
  `keterangan` text NOT NULL,
  `total` bigint(11) NOT NULL,
  `rek_tujuan` int(11) NOT NULL,
  `tgl_transfer` datetime NOT NULL,
  `nominal_transfer` bigint(20) NOT NULL,
  `tgl_berangkat` datetime NOT NULL,
  `tgl_pulang` datetime NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'Pemesanan',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `packages`
--

CREATE TABLE `packages` (
  `kode_paket` int(11) NOT NULL,
  `nama_paket` varchar(125) NOT NULL,
  `deskripsi_paket` varchar(15) NOT NULL,
  `harga_paket` int(11) NOT NULL,
  `tipe` varchar(15) NOT NULL DEFAULT 'Feature',
  `foto_paket` varchar(100) NOT NULL DEFAULT 'default.png',
  `username` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `packages`
--

INSERT INTO `packages` (`kode_paket`, `nama_paket`, `deskripsi_paket`, `harga_paket`, `tipe`, `foto_paket`, `username`, `created_at`, `updated_at`) VALUES
(1, 'Paket KPOP Hemat', 'Only', 3999999, 'Package', '1-270426.jpeg', 'bagus', '2018-07-17 12:41:02', '2018-09-14 16:09:48'),
(2, 'Paket Regular 5D4N', 'Start from', 4399999, 'Package', '2-270426.jpeg', 'bagus', '2018-07-17 12:41:02', '2018-08-27 16:26:11'),
(3, 'Visa', 'Only', 949999, 'Feature', '3-270442.png', 'bagus', '2018-08-27 16:11:40', '2018-08-27 16:42:57'),
(9, '', '', 0, 'Developing', 'default.png', 'bagus', '2018-09-10 11:21:24', '2018-09-10 11:21:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `package_details`
--

CREATE TABLE `package_details` (
  `id` int(11) NOT NULL,
  `kode_paket` int(11) NOT NULL,
  `hari` int(2) NOT NULL,
  `destination` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `package_details`
--

INSERT INTO `package_details` (`id`, `kode_paket`, `hari`, `destination`, `created_at`, `updated_at`) VALUES
(2, 1, 1, '[\"16\",\"3\",\"2\"]', '2018-08-11 10:36:00', '2018-09-14 16:09:48'),
(3, 1, 2, '[\"8\",\"9\",\"6\",\"4\",\"20\",\"7\",\"5\"]', '2018-08-11 10:36:00', '2018-09-14 16:09:48'),
(4, 1, 3, '[\"12\",\"14\",\"11\",\"10\",\"15\",\"13\"]', '2018-08-11 10:36:00', '2018-09-14 16:09:48'),
(5, 1, 4, '\"Freeday\"', '2018-08-11 10:36:00', '2018-09-14 16:09:48'),
(6, 1, 5, '\"Back to Airport\"', '2018-08-11 10:36:00', '2018-09-14 16:09:48'),
(7, 2, 1, '[\"16\",\"19\",\"3\"]', '2018-08-11 10:39:53', '2018-08-27 16:26:11'),
(8, 2, 2, '[\"10\",\"11\",\"12\",\"7\",\"8\",\"9\"]', '2018-08-11 10:39:53', '2018-08-27 16:26:11'),
(9, 2, 3, '[\"21\",\"15\",\"22\"]', '2018-08-11 10:39:53', '2018-08-27 16:26:11'),
(10, 2, 4, '\"Freeday\"', '2018-08-11 10:39:53', '2018-08-27 16:26:11'),
(11, 2, 5, '\"Back to Airport\"', '2018-08-11 10:39:53', '2018-08-27 16:26:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penggunas`
--

CREATE TABLE `penggunas` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(80) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `money` bigint(20) NOT NULL,
  `jenis_kelamin` tinyint(1) NOT NULL DEFAULT 1,
  `password` varchar(80) NOT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'Member',
  `kode_unik` varchar(20) NOT NULL,
  `foto` varchar(25) NOT NULL DEFAULT 'default.png',
  `last_login` datetime NOT NULL,
  `last_ip` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penggunas`
--

INSERT INTO `penggunas` (`id`, `username`, `email`, `nama_lengkap`, `money`, `jenis_kelamin`, `password`, `status`, `kode_unik`, `foto`, `last_login`, `last_ip`, `created_at`, `updated_at`) VALUES
(1, 'ade', 'ade@haneul-go.com', 'ade', 0, 1, '8010c6fe3f3e2c9a16a3273d8d6baaba', 'Developer', '', 'default.png', '0000-00-00 00:00:00', '', '2018-08-27 13:30:04', '2018-08-27 13:30:04'),
(2, 'bagus', 'bagus@haneul-go.com', 'Bagus Aulia Al Ilhami', 0, 1, 'b460bb7aa9f29824de57a7e322cfd94c', 'Developer', '', 'admin-231630.jpg', '2018-09-19 16:39:02', '27.131.7.244', '2018-06-08 13:42:01', '2018-09-19 16:39:02'),
(4, 'dia', 'dia@haneul-go.com', 'dia', 0, 2, '4a9e09b85dfe6ba573a52874375a07d0', 'Admin', '', 'default.png', '0000-00-00 00:00:00', '', '2018-08-27 13:30:56', '2018-09-08 17:30:16'),
(6, 'hendrik', 'hendrik@haneul-go.com', 'Hendrik', 0, 1, '6b21906224781d10785f7768f5a66217', 'Admin', '', 'default.png', '0000-00-00 00:00:00', '', '2018-08-27 13:31:45', '2018-08-27 13:31:45'),
(7, 'tempe', 'tempe@tempe', 'tempe cak anduk', 0, 1, 'b460bb7aa9f29824de57a7e322cfd94c', 'Member', '', 'tempe-09041041.jpg', '2018-09-07 18:49:18', '117.102.66.56', '2018-07-17 08:02:50', '2018-09-07 18:49:18'),
(8, 'vicky', 'vicky@haneul-go.com', 'vicky', 0, 1, '6c39b29006edf5eb47b1524bd8531383', 'Admin', '', 'default.png', '0000-00-00 00:00:00', '', '2018-08-27 13:32:50', '2018-08-27 13:32:50'),
(29, 'AlishaPutri', 'alishatania18@gmail.com', 'Alisha Putri', 0, 1, '634e1e0dc7a4dde0276d9126fec45ebb', 'Member', '', 'default.png', '0000-00-00 00:00:00', '', '2018-09-06 11:42:43', '2018-09-06 11:45:52'),
(30, 'Yuncha', 'kyuncha24@gmail.com', 'Kartika', 0, 1, 'd3d535059efa4b6ecbb4ca52ef240092', 'Member', 'fLnoxUfMaNeGxxNXri', 'default.png', '0000-00-00 00:00:00', '', '2018-09-15 10:21:00', '2018-09-15 10:21:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `nama_produk` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `harga_produk` bigint(20) NOT NULL,
  `stok` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan_produk` text COLLATE utf8_unicode_ci NOT NULL,
  `kategori_produk` text COLLATE utf8_unicode_ci NOT NULL,
  `foto_produk` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.png',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `nama_produk`, `harga_produk`, `stok`, `username`, `keterangan_produk`, `kategori_produk`, `foto_produk`, `created_at`, `updated_at`) VALUES
(1, 'Album ', 246000, 0, 'bagus', 'Album terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(5, 'Album ', 246000, 0, 'bagus', 'Album terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(6, 'Album ', 246000, 0, 'bagus', 'Album terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(7, 'Album ', 246000, 0, 'bagus', 'Album terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(8, 'Album ', 246000, 0, 'bagus', 'Album terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(9, 'Album ', 246000, 0, 'bagus', 'Album terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(10, 'Album ', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(11, 'Album ', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(12, 'Album ', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(13, 'Album ', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(14, 'Album ', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(15, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(16, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(17, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(18, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(19, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(20, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(21, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(22, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(23, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(24, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(25, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52'),
(26, 'Album BTS', 246000, 0, 'bagus', 'Album BTS terbaru 2018', '[\"2\"]', '1-09081018.jpeg', '2018-09-07 00:00:03', '2018-09-10 16:03:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `product_categories`
--

INSERT INTO `product_categories` (`id`, `nama_kategori`, `username`, `created_at`, `updated_at`) VALUES
(2, 'Album', '', '2018-09-10 15:55:44', '2018-09-10 15:55:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekenings`
--

CREATE TABLE `rekenings` (
  `id` int(11) NOT NULL,
  `nama_bank` varchar(10) NOT NULL,
  `no_rek` varchar(30) NOT NULL,
  `pemilik_rek` varchar(25) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rekenings`
--

INSERT INTO `rekenings` (`id`, `nama_bank`, `no_rek`, `pemilik_rek`, `created_at`, `updated_at`) VALUES
(1, 'BCA', '085-127-0000', 'Vicki Ardiansah', '2018-07-17 12:40:32', '2018-08-19 03:52:24'),
(3, 'BNI', '015-315-3134', 'Vicki Ardiansah', '2018-09-07 16:30:58', '2018-09-07 16:30:58'),
(4, 'Mandiri', '131-00-1384546-8', 'Vicki Ardiansah', '2018-09-07 16:31:29', '2018-09-07 16:31:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `kode_pembelian` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `total` bigint(20) NOT NULL,
  `rek_tujuan` int(11) NOT NULL,
  `tgl_transfer` datetime NOT NULL,
  `nominal_transfer` bigint(20) NOT NULL,
  `status` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pemesanan',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `shop_details`
--

CREATE TABLE `shop_details` (
  `id` int(11) NOT NULL,
  `kode_pembelian` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `id_produk` int(11) NOT NULL,
  `harga_produk` bigint(20) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `jumlah_pembelian` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `nama_slide` varchar(125) NOT NULL,
  `foto_slide` varchar(125) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `testimonies`
--

CREATE TABLE `testimonies` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `testimoni` text NOT NULL,
  `status` tinyint(1) DEFAULT 0 COMMENT '0=Tidak Dipublikasikan, 1=Dipublikasikan',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `testimonies`
--

INSERT INTO `testimonies` (`id`, `username`, `testimoni`, `status`, `created_at`, `updated_at`) VALUES
(2, 'tempe', 'Capicola nisi flank sed minim sunt aliqua rump pancetta leberkas venison eiusmod.', 0, '2018-07-10 08:55:30', '2018-08-27 15:38:30'),
(3, 'tempe', 'Nisi commodo bresaola, leberkas venison eiusmod bacon occaecat labore', 0, '2018-09-04 10:00:57', '2018-09-04 10:04:53'),
(7, 'tempe', 'Nisi commodo bresaola, leberkas venison eiusmod bacon occaecat', 0, '2018-09-04 10:20:18', '2018-09-04 10:20:18');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kurs`
--
ALTER TABLE `kurs`
  ADD PRIMARY KEY (`kode_kurs`);

--
-- Indeks untuk tabel `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `message_details`
--
ALTER TABLE `message_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`kode_order`);

--
-- Indeks untuk tabel `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`kode_paket`);

--
-- Indeks untuk tabel `package_details`
--
ALTER TABLE `package_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penggunas`
--
ALTER TABLE `penggunas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rekenings`
--
ALTER TABLE `rekenings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `shop_details`
--
ALTER TABLE `shop_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `testimonies`
--
ALTER TABLE `testimonies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `kurs`
--
ALTER TABLE `kurs`
  MODIFY `kode_kurs` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `message_details`
--
ALTER TABLE `message_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `packages`
--
ALTER TABLE `packages`
  MODIFY `kode_paket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `package_details`
--
ALTER TABLE `package_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `penggunas`
--
ALTER TABLE `penggunas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `rekenings`
--
ALTER TABLE `rekenings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `shop_details`
--
ALTER TABLE `shop_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `testimonies`
--
ALTER TABLE `testimonies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
