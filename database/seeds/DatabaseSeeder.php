<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Destination::class, 15)->create();
        factory(App\Models\Gallery::class, 15)->create();
    }
}
