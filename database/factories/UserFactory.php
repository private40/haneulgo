<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Pengguna::class, function (Faker $faker) {
    $status = array('Member', 'Admin', 'Developer');
    $gender = array(1, 2);

    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'nama_lengkap' => $faker->name,
        'money' => 0,
        'jenis_kelamin' => array_rand($gender, 1),
        'password' => 'b460bb7aa9f29824de57a7e322cfd94c', //huhu
        'status' => array_rand($status, 1),
    ];
});

$factory->define(App\Models\Destination::class, function (Faker $faker) {
    return [
        'nama_destinasi' => $faker->word,
        'keterangan_destinasi' => $faker->paragraph,
        'username' => function() {
          return factory('App\Models\Pengguna')->create()->username;
        },
    ];
});

$factory->define(App\Models\Gallery::class, function (Faker $faker) {
    return [
        'nama_galeri' => $faker->sentence,
        'jenis' => 'Traveling',
        'foto_galeri' => 'default.png',
        'username' => function() {
          return factory('App\Models\Pengguna')->create()->username;
        },
    ];
});

$factory->define(App\Models\Message::class, function (Faker $faker) {
    return [
        'username' => function() {
          return factory('App\Models\Pengguna')->create()->username;
        },
        'read_pengguna' => 0,
        'read_admin' => 0,
    ];
});


$factory->define(App\Models\Message_detail::class, function (Faker $faker) {
    return [
        'kode_pesan' => function() {
          return factory('App\Models\Message')->create()->id;
        },
        'pesan' => $faker->sentence,
        'status_user' => 1,
        'status_admin' => 1,
    ];
});

$factory->define(App\Models\Order::class, function (Faker $faker) {
    $feature = array(1, 2, 3);
    $status = array('Pemesanan', 'Pembayaran', 'Terkonfirmasi', 'Selesai');
    $addFeature = array();

    for ($i=0; $i < array_rand($feature, 1); $i++) {
        $addFeature[] = function() {
          return factory('App\Models\Feature')->create()->id;
        };
    }

    $dataFaker['username'] = function() {
                return factory('App\Models\Pengguna')->create()->username;
              };
    $dataFaker['jumlah_orang'] = $faker->randomDigitNotNull;
    $dataFaker['kode_paket'] = function() {
                return factory('App\Models\Package')->create()->kode_paket;
              };
    $dataFaker['harga_paket'] = $faker->numberBetween(10000, 100000);
    $dataFaker['fitur_tambahan'] = json_encode($addFeature);
    $dataFaker['keterangan'] = $faker->sentence;
    $dataFaker['total'] = $faker->numberBetween(50000, 200000);
    $dataFaker['rek_tujuan'] = function() {
                return factory('App\Models\Rekening')->create()->id;
              },
    $dataFaker['tgl_transfer'] = $faker->dateTime;
    $dataFaker['nominal_transfer'] = $faker->numberBetween(50000, 200000);
    $dataFaker['tgl_berangkat'] = $faker->dateTimeThisMonth;
    $dataFaker['tgl_pulang'] = $faker->DateTimeInInterval($dataFaker['tgl_berangkat'], '+5 days');
    $dataFaker['status'] = array_rand($status, 1);

    return $dataFaker;
});
