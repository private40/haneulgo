<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 25);
            $table->integer('jumlah_orang');
            $table->integer('kode_paket');
            $table->bigInteger('harga_paket');
            $table->string('fitur_tambahan', 150);
            $table->text('keterangan');
            $table->bigInteger('total');
            $table->integer('rek_tujuan');
            $table->dateTime('tgl_transfer');
            $table->bigInteger('nominal_transfer');
            $table->dateTime('tgl_berangkat');
            $table->dateTime('tgl_pulang');
            $table->string('status', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
