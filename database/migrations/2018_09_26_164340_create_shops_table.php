<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_pembelian', 25);
            $table->string('username', 25);
            $table->bigInteger('total_barang');
            $table->bigInteger('total_ongkir');
            $table->bigInteger('total');
            $table->integer('rek_tujuan');
            $table->dateTime('tgl_transfer');
            $table->bigInteger('nominal_transfer');
            $table->string('status', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
