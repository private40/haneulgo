<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggunas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 25);
            $table->string('email', 80);
            $table->string('nama_lengkap', 50);
            $table->bigInteger('money');
            $table->tinyInteger('jenis_kelamin');
            $table->string('password', 80);
            $table->string('status', 12);
            $table->string('kode_unik', 20);
            $table->string('foto', 25);
            $table->dateTime('last_login');
            $table->string('last_ip', 25);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggunas');
    }
}
