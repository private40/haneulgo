var _url       = $('meta[name="default"]').attr('content');
var _token     = $('meta[name="csrf-token"]').attr('content');

function read(kode_pesan)
{
    if(kode_pesan){
      $.ajax({
        url  : _url + "/admin/read_message",
        data : { kode_pesan, _token },
        type : "POST",
        success : function(done){
          $('#count_pesan')     .html('');
        }, error : function(fail){
          alert("Terjadi kesalahan \n" + fail.responseText);
        }
      });
    }
}

  function login(url)
  {
      var username   = $('#username').val();
      var password   = $('#password').val();

      $.ajax({
        url     : url,
        type    : "POST",
        data    : { username, password, _token },
        success : function(vein){
          var dante = JSON.parse(vein);
          $('#username').val("");
          $('#password').val("");
          $("#div_login").removeClass('hidden');
          $("#error_login").html(dante.teks);
          $("#div_login").addClass(dante.kelas);

          if(dante.url != ""){
            $("#div_login").removeClass('alert-warning');
            window.location = dante.url;
          }else{
            $("#div_login").removeClass('alert-success');
          }
        },
        error   : function (herz){
          alert(herz.responseText);
        }
      })
  }

  function loginpage(url)
  {
      var username   = $('#user').val();
      var password   = $('#pass').val();

      $.ajax({
        url     : url,
        type    : "POST",
        data    : { username, password, _token },
        success : function(vein){
          var dante = JSON.parse(vein);
          $('#username').val("");
          $('#password').val("");
          $("#page_login").removeClass('hidden');
          $("#errorpage_login").html(dante.teks);
          $("#page_login").addClass(dante.kelas);

          if(dante.url != ""){
            $("#page_login").removeClass('alert-warning');
            window.location = dante.url;
          }else{
            $("#page_login").removeClass('alert-success');
          }
        },
        error   : function (herz){
          alert(herz.responseText);
        }
      })
  }
  
  function editTesti(id)
  {
      $.ajax({
        url  : _url + "/testimony/detail",
        data : { id, _token },
        type : "POST",
        success : function(done){
          var data = JSON.parse(done);
          $('#id_testi').val(id);
          $('#testimoni').val(data.testimoni);
          $('#testimoni').focus();
        },
        error   : function(fail){
          alert(fail.responseText);
        }
      })
  }
  
  function deleteTesti(id)
  {
      $.ajax({
        url  : _url + "/testimony/detail",
        data : { id, _token },
        type : "POST",
        success : function(done){
          var data = JSON.parse(done);
          $('#isi_testi').html(data.testimoni);
          $('#delete_testi').attr('href', _url + "/testimony/delete/" +  id);
        },
        error   : function(fail){
          alert(fail.responseText);
        }
      })
  }
  
  function destroy()
  {
    $.ajax({
      url   : _url + '/destroy',
      data  : { _token },
      type  : "POST",
      error : function(error){
        alert("Terjadi Kesalahan \n" + error.responseText);
      }
    });
    
    setTimeout(function () {
        $("#pemberitahuan").addClass("animated");
        $("#pemberitahuan").addClass("fadeOut");
      }, 4000
    );
    setTimeout(function () {
      $("#pemberitahuan").addClass("hidden");
        }, 4500
    );
  }

  function daftar(url)
  {
      var nama_lengkap = $("#nama_lengkap").val();
      var email        = $("#email").val();
      var username     = $("#user").val();
      var password     = $("#pass").val();
      var info_user    = $("#info_user").html();
      var _token       = $('meta[name="csrf-token"]').attr('content');

      if(nama_lengkap  == ""){ $("#nama_lengkap").focus(); }
      else if(email    == ""){ $("#email").focus(); }
      else if(username == ""){ $("#user").focus(); }
      else if(password == ""){ $("#password").focus(); }
      else if(email.indexOf('@') < 0) { $("#email").focus(); }
      else if(info_user.indexOf('telah digunakan.') > 0) { $("#user").focus(); }
      else{
          $.ajax({
            url  : url,
            data : { nama_lengkap, email, username, password, _token },
            type : "POST",
            success : function(done){
              window.location = JSON.parse(done);
            },
            error   : function(fail){
              alert(fail.responseText);
            }
          })
      }
  }

  function get_text(key, id){
    var user = $("#" + id).val();

    if(key == 48){ user = user + "0"; }
    else if(key == 49){ user = user + "1"; }
    else if(key == 50){ user = user + "2"; }
    else if(key == 51){ user = user + "3"; }
    else if(key == 52){ user = user + "4"; }
    else if(key == 53){ user = user + "5"; }
    else if(key == 54){ user = user + "6"; }
    else if(key == 55){ user = user + "7"; }
    else if(key == 56){ user = user + "8"; }
    else if(key == 57){ user = user + "9"; }
    else if(key == 97){ user = user + "a"; }
    else if(key == 98){ user = user + "b"; }
    else if(key == 99){ user = user + "c"; }
    else if(key == 100){ user = user + "d"; }
    else if(key == 101){ user = user + "e"; }
    else if(key == 102){ user = user + "f"; }
    else if(key == 103){ user = user + "g"; }
    else if(key == 104){ user = user + "h"; }
    else if(key == 105){ user = user + "i"; }
    else if(key == 106){ user = user + "j"; }
    else if(key == 107){ user = user + "k"; }
    else if(key == 108){ user = user + "l"; }
    else if(key == 109){ user = user + "m"; }
    else if(key == 110){ user = user + "n"; }
    else if(key == 111){ user = user + "o"; }
    else if(key == 112){ user = user + "p"; }
    else if(key == 113){ user = user + "q"; }
    else if(key == 114){ user = user + "r"; }
    else if(key == 115){ user = user + "s"; }
    else if(key == 116){ user = user + "t"; }
    else if(key == 117){ user = user + "u"; }
    else if(key == 118){ user = user + "v"; }
    else if(key == 119){ user = user + "w"; }
    else if(key == 120){ user = user + "x"; }
    else if(key == 121){ user = user + "y"; }
    else if(key == 122){ user = user + "z"; }
    // else if(key == 59){ user = user; }
    else{ user = "character is not allowed."; }
    // user = user + "(" + key + ")";
    return user;
  }

  var specialKeys = new Array();
  specialKeys.push(8); //Backspace
  function pengguna(e) {
      var keyCode = e.which ? e.which : e.keyCode
      var ret     = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 97 && keyCode <= 122) || specialKeys.indexOf(keyCode) != -1);

      if(ret == true)
      {
        var user = get_text(e.keyCode, "user");
        $("#error").addClass('hidden');
        var _url    = $('meta[name="default"]').attr('content');
        var _token  = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
          url     : _url + '/admin/check_user',
          data    : { user, _token },
          type    : "POST",
          success : function(done){
            if(done == "ada"){
              $("#info_user").removeClass("boleh");
              $("#info_user").addClass("info");
              $("#info_user").html("Username " + user + " telah digunakan.");
            }else{
              $("#info_user").removeClass("info");
              $("#info_user").addClass("boleh");
              $("#info_user").html("Username " + user + " tersedia.");
            }
          },
          error   : function(elg){
            alert(elg.responseText);
          }
        });
      }else{
        $("#error").removeClass('hidden');
        $("#info_user").addClass("hidden");
      }
      return ret;
  }

  function Rupiah(nominal){
    var sisa_string = nominal.toString(),
        sisa_sisa    = sisa_string.length % 3,
        rupiah_sisa  = sisa_string.substr(0, sisa_sisa),
        ribuan_sisa  = sisa_string.substr(sisa_sisa).match(/\d{3}/g);

    if (ribuan_sisa) {
        separator = sisa_sisa ? "." : "";
        rupiah_sisa += separator + ribuan_sisa.join(".");
    };

    return rupiah_sisa;
  }

  var specialKeys = new Array();
  specialKeys.push(8); //Backspace
  function numeric(e) {
      var keyCode = e.which ? e.which : e.keyCode
      var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
      document.getElementById("error").style.display = ret ? "none" : "none";
      return ret;
  }

  var handleDataTableButtons = function() {
      "use strict";
      0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
        dom: "Bfrtip",
        buttons: [{
          extend: "copy",
          className: "btn-sm"
        }, {
          extend: "csv",
          className: "btn-sm"
        }, {
          extend: "excel",
          className: "btn-sm"
        }, {
          extend: "pdf",
          className: "btn-sm"
        }, {
          extend: "print",
          className: "btn-sm"
        }],
        responsive: !0
      })
    },
    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons()
        }
      }
    }();

  $(document).ready(function() {
    $('#datatable').dataTable();
    $('#datatable-keytable').DataTable({
      keys: true
    });
    $('#datatable-testimony').DataTable({
      keys: true
    });
    $('#datatable-responsive').DataTable();
    $('#datatable-scroller').DataTable({
      ajax: "datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });
    var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });

    $("#nama_lengkap").focusout(function(){
      var nama = $("#nama_lengkap").val();

      if(nama == ""){
        $("#error_nama_lengkap").removeClass('hidden');
      }else{
        $("#error_nama_lengkap").addClass('hidden');
      }
    });

    $("#email").focusout(function(){
      var email = $("#email").val();
      var at    = email.indexOf("@");

      if(email == ""){
        $("#error_email").removeClass('hidden');
        $("#info_email").html('Email harus diisi.');
      }else if(at < 0){
        $("#error_email").removeClass('hidden');
        $("#info_email").html('Email tidak valid.');
      }else{
        $("#error_email").addClass('hidden');
      }
    });

    $("#user").focusout(function(){
      var user  = $("#user").val();
      var done  = check_user(user);

      if(done == "ada"){
        $("#user").focus();
        $("#info_user").removeClass("boleh");
        $("#info_user").addClass("info");
        $("#info_user").html("Username " + user + " telah digunakan.");
      }else{
        $("#info_user").removeClass("info");
        $("#info_user").addClass("boleh");
        $("#info_user").html("Username " + user + " tersedia.");
      }
    });

    $("#jumlah_orang").change(function(){
      var harga  = $("#harga_paket").val();
      var jumlah = $("#jumlah_orang").val();
      var total  = harga * jumlah;
      total      = Rupiah(total);
      $("#total").val(total);
    });

    $('.feature').click(function(){
      var check  = $('input[name="fitur[]"]:checked').length;
      var harga  = $("#harga_paket").val();
      var jumlah = $("#jumlah_orang").val();

      if(check > 0){
        var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });

        $.ajax({
          url     : _url + '/calculate',
          data    : { harga, jumlah, val, _token },
          type    : "POST",
          success : function(done){
            $("#total").val(done);
          },
          error   : function(elg){
            alert(elg.responseText);
          }
        });
      }else{
        var total  = harga * jumlah;
        total      = Rupiah(total);
        $("#total").val(total);
      }
    })
  })
