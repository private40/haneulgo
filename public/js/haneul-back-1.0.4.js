var _url    = $('meta[name="url"]').attr('content');
var _token  = $('meta[name="csrf-token"]').attr('content');
var home    = $('meta[name="home"]').attr('content');

function detail_transaksi(kode_order){
  $.ajax({
    url  : _url + "/order_detail",
    data : { kode_order, _token },
    type : "POST",
    success : function(done){
      var detail  = JSON.parse(done);
      var tipe    = $('#tipe_order').html();

      $('#span_title')      .html(kode_order);
      $('#kode_order')      .html(kode_order);
      $('#created_at')      .html(detail.created_at);
      $('#username')        .html(detail.username);
      $('#nama_lengkap')    .html(detail.nama_lengkap);
      $('#email')           .html(detail.email);
      $('#tgl_travel')      .html(detail.tgl_travel);
      $('#nama_paket')      .html(detail.nama_paket);
      $('#jumlah_orang')    .html(detail.jumlah_orang);
      $('#harga_paket')     .html(detail.harga_paket);
      $('#nominal_transfer').html(detail.nominal_transfer);
      $('#tujuan_transfer') .html(detail.tujuan_transfer);
      $('#tgl_transfer')    .html(detail.tgl_transfer);
      $('#status')          .html(detail.status);
      $('#foto')            .attr('src', detail.foto);

      if(tipe == "Pemesanan"){
        $('#total_biaya')     .val(detail.total);
        $('#keterangan')      .val(detail.keterangan);
      }else{
        $('#total_biaya')     .html(detail.total_biaya);
        $('#keterangan')      .html(detail.keterangan);
      }
    }, error : function(fail){
      alert("Terjadi kesalahan \n" + fail.responseText);
    }
  });
}

function konfirmDetail()
{
    var kode_order  = $('#span_title').html();
    var status      = "Terkonfirmasi";

    $.ajax({
      url  : _url + "/order/konfirm",
      data : { _token, kode_order, status },
      type : 'POST',
      success : function(done){
        location.reload();
      },
      error   : function(fail){
        alert('Terjadi kesalahan : \n' + fail.responseText);
      }
    })
}

function perbaruiOrder()
{
    var kode_order  = $('#span_title').html();
    var total_biaya = $('#total_biaya').val();
    var keterangan  = $('#keterangan').val();

    $.ajax({
      url  : _url + "/order/perbarui",
      data : { _token, kode_order, total_biaya, keterangan },
      type : 'POST',
      success : function(done){
        location.reload();
      },
      error   : function(fail){
        alert('Terjadi kesalahan : \n' + fail.responseText);
      }
    })
}

function orderSelesai()
{
    var kode_order  = $('#span_title').html();
    var status      = "Selesai";

    $.ajax({
      url  : _url + "/order/konfirm",
      data : { _token, kode_order, status },
      type : 'POST',
      success : function(done){
        location.reload();
      },
      error   : function(fail){
        alert('Terjadi kesalahan : \n' + fail.responseText);
      }
    })
}

function showInfo()
{
    $("#divBack").removeClass("hidden");
    $("#divBack").addClass("animated");
    $("#divBack").addClass("fadeIn");
    setTimeout(function () {
      $("#divBack").addClass("animated");
      $("#divBack").addClass("fadeOut");
        }, 4000
    );
    setTimeout(function () {
      $("#divBack").addClass("hidden");
        }, 4500
    );
}

function deletePengguna(username)
{
    $(".span_user").html(username);
    $('#konfirm_delete').attr('href', _url + '/pengguna/delete/' + username);
}

function detailPengguna(username, tipe)
{
  $.ajax({
    url  : _url + "/detail_pengguna",
    data : { username, _token },
    type : "POST",
    success : function(done){
      var detail  = JSON.parse(done);

      if(tipe == "Admin"){
        $('#span_title')      .html(username);
        $('#username')        .val(username);
        $('#nama_lengkap')    .val(detail.nama_lengkap);
        $('#email')           .val(detail.email);
        $('#jenis_kelamin')   .val(detail.jenis_kelamin);
        $('#status')          .val(detail.status);
        $('#created_at')      .val(detail.created_at);
        $('#count_travel')    .val(detail.count_travel);
        $('#foto')            .attr('src', detail.foto);
      }else{
        $('#span_title')      .html(username);
        $('#username')        .html(username);
        $('#nama_lengkap')    .html(detail.nama_lengkap);
        $('#email')           .html(detail.email);
        $('#jenis_kelamin')   .html(detail.jenis_kelamin);
        $('#status')          .html(detail.status);
        $('#created_at')      .html(detail.created_at);
        $('#count_travel')    .html(detail.count_travel);
        $('#foto')            .attr('src', detail.foto);
      }
    }, error : function(fail){
      alert("Terjadi kesalahan \n" + fail.responseText);
    }
  });
}

function read(kode_pesan)
{
    if(kode_pesan){
      $.ajax({
        url  : _url + "/read_message",
        data : { kode_pesan, _token },
        type : "POST",
        success : function(done){
          $('#count_pesan')     .html('');
        }, error : function(fail){
          alert("Terjadi kesalahan \n" + fail.responseText);
        }
      });
    }
}

function formRekening(id)
{
    if(id == '')
    {
        $('#judul').html('Rekening Baru');
        $('#id').val('');
        $('#nama_bank').val('');
        $('#no_rek').val('');
        $('#pemilik_rek').val('');
    }else{
      $.ajax({
        url  : _url + '/rekening/detail',
        data : { _token, id},
        type : 'POST',
        success : function(done){
            var data = JSON.parse(done);
            $('#id').val(id);
            $('#nama_bank').val(data.nama_bank);
            $('#no_rek').val(data.no_rek);
            $('#pemilik_rek').val(data.pemilik_rek);
            $('#judul').html('Ubah Rekening - ' + data.nama_bank);
        },
        error   : function(fail){
            alert('Terjadi Kesalahan :\n' + fail.responseText);
        },
      })
    }
}

function formKategori(id, nama_kategori)
{
    if(id == ''){
        $('#judul').html('Kategori Baru');
        $('#id').val('');
        $('#nama_kategori').val('');
    }else{
        $('#judul').html('Ubah Kategori - ' + nama_kategori);
        $('#id').val(id);
        $('#nama_kategori').val(nama_kategori);
    }
}

function showGallery(id, foto)
{
    var new_home = home.replace("/index.php", "");
    $("#img_gallery").attr('src', new_home + '/images/gallery/' + id + '/' + foto);
    $(".title_gallery").html(foto);
}

function formGallery(id)
{
    $.ajax({
        url  : _url + '/gallery/detail',
        data : { _token, id},
        type : 'POST',
        success : function(done){
            var data = JSON.parse(done);
            $('#id').val(id);
            $('#nama_galeri').val(data.nama_galeri);
            $('#jenis').val(data.jenis);
            $('.title_gallery').html(data.foto_galeri);
        },
        error   : function(fail){
            alert('Terjadi Kesalahan :\n' + fail.responseText);
        },
      })
}

function deleteGallery(id, foto)
{
    var lokasi = 'gallery/' + id + '/' + foto;
    $('.title_gallery').html(foto);
    
    $("#lokasi").val(lokasi);
}

function formFeature(kode_paket)
{
    if(kode_paket == '')
    {
        $('#judul').html('Fitur Paket Baru');
        $('#kode_paket').val('');
        $('#nama_paket').val('');
        $('#harga_paket').val('');
    }else{
      $.ajax({
        url  : _url + '/feature/detail',
        data : { _token, kode_paket},
        type : 'POST',
        success : function(done){
            var data = JSON.parse(done);
            $('#kode_paket').val(kode_paket);
            $('#nama_paket').val(data.nama_paket);
            $('#harga_paket').val(data.harga_paket);
            $('#judul').html('Ubah Fitur Paket - ' + data.nama_paket);
        },
        error   : function(fail){
            alert('Terjadi Kesalahan :\n' + fail.responseText);
        },
      })
    }
}

function deleteItem(id, nama, jenis)
{
    $("#konfirm_delete").attr('href', _url + jenis + '/delete/' + id);
    $('.span_name').html(nama);
}

function get_text(key, id){
  var user = $("#" + id).val();

  if(key == 48){ user = user + "0"; }
  else if(key == 49){ user = user + "1"; }
  else if(key == 50){ user = user + "2"; }
  else if(key == 51){ user = user + "3"; }
  else if(key == 52){ user = user + "4"; }
  else if(key == 53){ user = user + "5"; }
  else if(key == 54){ user = user + "6"; }
  else if(key == 55){ user = user + "7"; }
  else if(key == 56){ user = user + "8"; }
  else if(key == 57){ user = user + "9"; }
  else if(key == 97){ user = user + "a"; }
  else if(key == 98){ user = user + "b"; }
  else if(key == 99){ user = user + "c"; }
  else if(key == 100){ user = user + "d"; }
  else if(key == 101){ user = user + "e"; }
  else if(key == 102){ user = user + "f"; }
  else if(key == 103){ user = user + "g"; }
  else if(key == 104){ user = user + "h"; }
  else if(key == 105){ user = user + "i"; }
  else if(key == 106){ user = user + "j"; }
  else if(key == 107){ user = user + "k"; }
  else if(key == 108){ user = user + "l"; }
  else if(key == 109){ user = user + "m"; }
  else if(key == 110){ user = user + "n"; }
  else if(key == 111){ user = user + "o"; }
  else if(key == 112){ user = user + "p"; }
  else if(key == 113){ user = user + "q"; }
  else if(key == 114){ user = user + "r"; }
  else if(key == 115){ user = user + "s"; }
  else if(key == 116){ user = user + "t"; }
  else if(key == 117){ user = user + "u"; }
  else if(key == 118){ user = user + "v"; }
  else if(key == 119){ user = user + "w"; }
  else if(key == 120){ user = user + "x"; }
  else if(key == 121){ user = user + "y"; }
  else if(key == 122){ user = user + "z"; }
  // else if(key == 59){ user = user; }
  else{ user = "character is not allowed."; }
  // user = user + "(" + key + ")";
  return user;
}

var specialKeys = new Array();
specialKeys.push(8); //Backspace
function pengguna(e) {
    var keyCode = e.which ? e.which : e.keyCode
    var ret     = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 97 && keyCode <= 122) || specialKeys.indexOf(keyCode) != -1);

    if(ret == true)
    {
      var user = get_text(e.keyCode, "user");

      $("#error").addClass('hidden');
      $.ajax({
        url     : _url + '/check_user',
        data    : { user, _token },
        type    : "POST",
        success : function(done){
          if(done == "ada"){
            $("#info_user").removeClass("boleh");
            $("#info_user").addClass("info");
            $("#info_user").html("Username " + user + " telah digunakan.");
          }else{
            $("#info_user").removeClass("info");
            $("#info_user").addClass("boleh");
            $("#info_user").html("Username " + user + " tersedia.");
          }
        },
        error   : function(elg){
          alert(elg.responseText);
        }
      });
    }else{
      $("#error").removeClass('hidden');
      $("#info_user").addClass("hidden");
    }
    return ret;
}

var specialKeys = new Array();
specialKeys.push(8); //Backspace
function numeric(e) {
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    return ret;
}

function destroy()
{
    $.ajax({
      url   : home + '/destroy',
      data  : { _token },
      type  : "POST",
      error : function(error){
        alert("Terjadi Kesalahan \n" + error.responseText);
      }
    });

    setTimeout(function () {
        $("#pemberitahuan").addClass("animated");
        $("#pemberitahuan").addClass("fadeOut");
      }, 4000
    );
    setTimeout(function () {
      $("#pemberitahuan").addClass("hidden");
        }, 4500
    );
}
