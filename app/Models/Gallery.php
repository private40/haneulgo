<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Gallery extends Model
{
    protected $guarded = ['kode_kurs', 'created_at'];

    public static function getAutoID()
    {
        $count    = DB::table('galleries')->count();

        if($count > 0){
          $lastData = DB::table('galleries')->orderBy('id', 'desc')->first();
          $id       = $lastData->id + 1;
          return $id;
        }

        return "1";
    }
    
    public static function normalization($lokasi)
    {
        $normalisasi = explode("/", $lokasi);
        
        return $normalisasi;
    }
    
    public static function getJenis()
    {
        $jenis = DB::table('galleries')
                ->groupBy('jenis')
                ->get();
        $data  = array();
        $i     = 0;
                
        foreach($jenis as $jenis){
            $data[$jenis->jenis]['class'] = strtolower(str_replace(" ", "", $jenis->jenis));
            $data[$jenis->jenis]['name']  = $jenis->jenis;
            $i++;
        }
        
        return $data;
    }
    
    public static function orderByDesc()
    {
        return DB::table('galleries')
                ->orderBy('id', 'desc')
                ->get();
    }
}
