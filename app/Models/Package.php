<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Testimonie;

class Package extends Model
{
    protected $guarded = ['kode_paket', 'created_at'];

    public static function photoPackage($foto, $kode_paket)
    {
        $addressFoto  = array();
        if(!empty(strpos($foto, ","))){
            $dataFoto = explode(",", $foto);
        }else{
            $dataFoto[0] = $foto;
        }

        for ($i=0; $i < count($dataFoto); $i++) {
            $addressFoto[$i] = Testimonie::checkPhoto('package', $dataFoto[$i], $kode_paket);
        }

        return $addressFoto;
    }

    public static function getKodePaket()
    {
        $dataPaket = DB::table('packages')->select('*')->orderBy('kode_paket', 'desc')->first();

        return $dataPaket->kode_paket;
    }

    public static function actionTable($kode_paket, $nama_paket, $tipe)
    {
        $action  = '<td>';

        if($tipe == 'feature'){
            $action .= '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#form" onclick="formFeature('."'".$kode_paket."'".')"><i class="fa fa-pencil"></i> Edit</button>';
        }else{
            $action .= '<a href="'.url('admin/package/form/'.$kode_paket).'" class="btn btn-primary btn-xs" ><i class="fa fa-file-text"></i> Edit</a>';
        }

        $action .= '<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete" onclick="deleteItem('."'".$kode_paket."', '".$nama_paket."','/package'".')"><i class="fa fa-times"></i> Delete</button></td>';

        return $action;
    }
}
