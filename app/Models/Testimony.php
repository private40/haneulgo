<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Testimony extends Model
{
    protected $guarded = ['id', 'created_at'];

    public static function getPublication()
    {
        return DB::table('testimonies')
                    ->join('penggunas', 'testimonies.username', '=', 'penggunas.username')
                    ->select('testimonies.id', 'testimonies.username', 'testimonies.testimoni', 'penggunas.nama_lengkap', 'penggunas.foto')
                    ->offset(0)
                    ->limit(2)
                    ->where('testimonies.status', 1)
                    ->orderBy('id', 'DESC')
                    ->get();
    }

    public static function checkPhoto($folder, $photo, $code)
    {
        $separator = "/";
        $urlcode   = "";
        // $separator = substr($separator, 0, 1);
        if(!empty($code)){
          $urlcode = $code.'/';
        }

        if(!empty($code)){
          $code    = $code.$separator;
        }

    	  $thumb     = public_path().'/images'.$separator.$folder.$separator.$code.$photo;

        if(file_exists($thumb)){
            $thumb   = asset('images/'.$folder.'/'.$urlcode.$photo);
        }else{
       		  $thumb   = asset('images/'.$folder.'/default.png');
       	}

        return $thumb;
    }

    public static function getMoreDetail($list_testimoni = array())
    {
        $detail = array();
        foreach ($list_testimoni as $list) {
            $start   = date_create($list->created_at);
            $date    = date_format($start,"d M Y, H:i:s");
            $detail[$list->id]['tanggal'] = $date;
        }

        return $detail;
    }
}
