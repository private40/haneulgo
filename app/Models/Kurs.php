<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kurs extends Model
{
    protected $guarded = ['kode_kurs', 'created_at'];

    public static function getIcon()
    {
        $kurs['USD'] = '$';
        $kurs['KRW'] = '₩';
        $kurs['IDR'] = 'Rp.';

        return $kurs;
    }
}
