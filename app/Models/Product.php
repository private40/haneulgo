<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $guarded = ['id', 'created_at'];

    public static function actionTable($id, $nama_produk)
    {
        return '<td>
                    <a href="'.url('admin/shop/product/form/'.$id).'" class="btn btn-primary btn-xs" ><i class="fa fa-file-text"></i> Edit</a>
                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete" onclick="deleteItem('."'".$id."','".$nama_produk."','/shop/product'".')"><i class="fa fa-times"></i> Delete</button>
                </td>';
    }
}
