<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pengguna extends Model
{
    protected $guarded = ['created_at'];

    public static function encry($word)
    {
        return md5(substr(md5(substr(md5($word),0,9).'b9726dc'.substr(md5($word),9)),0,27).'f213a09b'.substr(md5(substr(md5($word),0,9).'b9726dc'.substr(md5($word),9)),27));
    }
    
    public static function generateKode()
    {
        $kode_unik = str_random(18);
        
        $dataKode  = DB::table('penggunas')->where('kode_unik', $kode_unik)->count();
        
        while($dataKode > 0){
            $kode_unik = str_random(18);
            $dataKode  = DB::table('penggunas')->where('kode_unik', $kode_unik)->count();
        }
        
        return $kode_unik;
    }
    
    public static function actionTable($username, $tipe)
    {
        $action  = '<td>';
        
        if($tipe == 'admin'){
            $action .= '<a href="'.url('admin/pengguna/form/'.$username).'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit </a>';
            $action .= '<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete" onclick="deletePengguna('."'".$username."'".')"><i class="fa fa-times"></i> Delete</button>';
        }else{
            $action .= ' <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail" onclick="detailPengguna('."'".$username."', '".$tipe."'".')"><i class="fa fa-file-text"></i> Show Data</button>';
        }
        
        return $action;
    }
}
