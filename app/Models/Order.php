<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\Package;

class Order extends Model
{
    protected $guarded = ['id', 'created_at'];

    public static function getUnfinish($username)
    {
        return DB::table('orders')
                  ->where('username', $username)
                  ->where('status', '!=', 'Selesai')
                  ->select('*');
    }

    public static function getDetail($kode_order)
    {
        return DB::table('orders')
                  ->join('packages',  'orders.kode_paket', '=', 'packages.kode_paket')
                  ->join('penggunas', 'orders.username',   '=', 'penggunas.username')
                  ->join('rekenings', 'orders.rek_tujuan', '=', 'rekenings.id')
                  ->select('orders.kode_order', 'orders.created_at', 'orders.username', 'penggunas.nama_lengkap', 'penggunas.email',
                        'penggunas.foto', 'orders.tgl_berangkat', 'orders.tgl_pulang', 'packages.nama_paket', 'orders.jumlah_orang',
                        'orders.harga_paket', 'orders.nominal_transfer', 'rekenings.nama_bank', 'rekenings.no_rek', 'orders.tgl_transfer',
                        'orders.keterangan', 'orders.total', 'orders.status', 'orders.fitur_tambahan')
                  ->where('orders.kode_order', $kode_order)
                  ->first();
    }

    public static function getDetailNoRek($kode_order)
    {
        return DB::table('orders')
                  ->join('packages',  'orders.kode_paket', '=', 'packages.kode_paket')
                  ->join('penggunas', 'orders.username',   '=', 'penggunas.username')
                  ->select('orders.kode_order', 'orders.created_at', 'orders.username', 'penggunas.nama_lengkap', 'penggunas.email',
                        'penggunas.foto', 'orders.tgl_berangkat', 'orders.tgl_pulang', 'packages.nama_paket', 'orders.jumlah_orang',
                        'orders.harga_paket', 'orders.nominal_transfer', 'orders.keterangan', 'orders.rek_tujuan', 'orders.total',
                        'orders.status', 'orders.tgl_transfer', 'orders.fitur_tambahan')
                  ->where('orders.kode_order', $kode_order)
                  ->first();
    }

    public static function countTravel($username)
    {
        return DB::table('orders')
                ->where('username', $username)
                ->where('status', 'Selesai')
                ->count();
    }

    public static function getMoreDetail($list_transaksi = array())
    {
        $detail = array();
        foreach ($list_transaksi as $list) {
            $start   = date_create($list->created_at);
            $date    = date_format($start,"d M Y, H:i:s");

            $detail[$list->kode_order]['tanggal'] = $date;
            $detail[$list->kode_order]['biaya']   = 'Rp. '.number_format($list->total,0,'','.').',-';;
        }

        return $detail;
    }

    public static function getOrderCode()
    {
        $dataOrder = DB::table('orders')->orderBy('created_at', 'DESC')->first();
        $dateNow   = date("ym");

        if($dataOrder){
          $kodeOrder = substr($dataOrder->kode_order, 6);
          $kodeOrder = $kodeOrder + 1;

          return "HG".$dateNow.$kodeOrder;
        }

        return "HG".$dateNow."182471217";
    }

    public static function labelStatus($status)
    {
        switch ($status) {
          case 'Selesai':
            $label = 'label-success';
            break;

          case 'Terkonfirmasi':
            $label = 'label-primary';
            break;

          case 'Pembayaran':
            $label = 'label-warning';
            break;

          default:
            $label = 'label-default';
            break;
        }

        return $label;
    }

    public static function generalTotal($total)
    {
        return str_replace('.', '', $total);
    }

    public static function fiturTambahan($fitur)
    {
        $fiturArray   = explode(",", $fitur);
        $banyakData   = count($fiturArray);
        $data         = '';

        for($urutanData = 0; $urutanData < $banyakData; $urutanData++) {
            if(!empty($fiturArray[$urutanData])){
                $dataPaket  = Package::where('kode_paket', $fiturArray[$urutanData])->first();

                if($urutanData == ($banyakData - 1)){
                  $data    .= $dataPaket->nama_paket." (Rp. ".number_format($dataPaket->harga_paket,0,'','.').',-)';
                }else{
                  $data    .= $dataPaket->nama_paket." (Rp. ".number_format($dataPaket->harga_paket,0,'','.').',-) ; ';
                }
            }
        }

        return $data;
    }
    
    public static function actionTable($kode_order, $tipe)
    {
        $action  = '<td>
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail" onclick="detail_transaksi('."'".$kode_order."'".')"><i class="fa fa-file-text"></i> Detail</button>';
        
        if($tipe == 'konfirmasi'){
            $action .= '<a href="'.url('admin/order/confirmation/'.$kode_order).'" class="btn btn-success btn-xs" ><i class="fa fa-check-circle"></i> Konfirmasi</a>';
        }
        
        $action .= '</td>';
                        
        return $action;
    }
}
