<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product_category extends Model
{
    protected $guarded = ['id', 'created_at'];

    public static function actionTable($id, $nama_kategori)
    {
        $dataProduct = DB::table('products')
                            ->where('kategori_produk', 'like', '%"'. $id .'%"')
                            ->count();
        
        $data  = '<td>'.
                  number_format($dataProduct,0,'','.')
                .' Produk</td>';
            
        $data .= '<td>
                  <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#form" onclick="formKategori('."'".$id."', '".$nama_kategori."'".')"><i class="fa fa-pencil"></i> Edit</button>
                  <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete" onclick="deleteItem('."'".$id."','".$nama_kategori."','/shop/category'".')"><i class="fa fa-times"></i> Delete</button>
                </td>';
        
        return $data;
    }
    
    public static function getProductCount()
    {
        $kategori = DB::table('product_categories')
                ->get();
        
        $countProduct = array();
                
        foreach($kategori as $kategori){
            $dataProduct = DB::table('products')
                            ->where('kategori_produk', 'like', '%"'. $kategori->id .'"%')
                            ->count();
            
            $countProduct[$kategori->id] = $dataProduct;
        }
        
        $countProduct['all'] = DB::table('products')->count();
        
        return $countProduct;
    }
    
    public static function getCategory($kategori_produk)
    {
        $kategori = json_decode($kategori_produk);
        $count    = count($kategori);
        $hasil    = "";
        
        if(is_array($kategori)){
            $i = 1;
            foreach($kategori as $kategori){
                $dataKategori = DB::table('product_categories')->where('id', $kategori)->first();
                $hasil .= $dataKategori->nama_kategori;
                if($i < $count){
                    $hasil .= ', ';
                }
                
                $i++;
            }
        }else{
            $dataKategori = DB::table('product_categories')->find($kategori);    
            $hasil = $dataKategori->nama_kategori;
        }
        
        return $hasil;
    }
}
