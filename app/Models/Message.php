<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\Testimony;

class Message extends Model
{
    protected $guarded = ['id', 'created_at'];

    public static function getUnread($username)
    {
      return DB::table('messages')
                  ->where('username', $username)
                  ->where('read_pengguna', 0)
                  ->select('*');
    }

    public static function getAll()
    {
      return DB::table('messages')
                  ->join('message_details', 'messages.id', '=', 'message_details.kode_pesan')
                  ->join('penggunas', 'messages.username', '=', 'penggunas.username')
                  ->select('messages.username', 'message_details.created_at', 'penggunas.foto', 'message_details.pesan', 'message_details.kode_pesan')
                  ->orderBy('message_details.created_at', 'DESC')
                  ->groupBy('messages.id')
                  ->get();
    }

    public static function getLast($num_rows)
    {
      return DB::table('messages')
                  ->join('message_details', 'messages.id', '=', 'message_details.kode_pesan')
                  ->join('penggunas', 'messages.username', '=', 'penggunas.username')
                  ->select('messages.username', 'messages.updated_at', 'penggunas.foto', 'message_details.pesan', 'message_details.kode_pesan', 'messages.read_admin', DB::raw('max(message_details.created_at) as created_at'))
                  ->limit($num_rows)
                  ->orderBy(DB::raw('max(message_details.created_at)'), 'DESC')
                  ->groupBy('messages.id')
                  ->get();
    }

    public static function getMessage($kode_pesan)
    {
      return DB::table('messages')
                  ->join('message_details', 'messages.id', '=', 'message_details.kode_pesan')
                  ->join('penggunas', 'message_details.username', '=', 'penggunas.username')
                  ->select('message_details.id', 'message_details.created_at', 'message_details.pesan', 'penggunas.status')
                  ->where('kode_pesan', $kode_pesan)
                  ->orderBy('message_details.created_at', 'ASC')
                  ->get();
    }

    public static function dateTimeDetail($msgList = array())
    {
        $detail        = array();
        foreach ($msgList as $msg) {
            $start   = date_create($msg->created_at);
            $timeMsg = date_format($start,"H:i:s");
            $dateMsg = date_format($start,"d M Y");

            $detail[$msg->id]['timeMsg']  = $timeMsg;
            $detail[$msg->id]['dateMsg']  = $dateMsg;
        }

        return $detail;
    }

    public static function msgDetail($msgList = array())
    {
        $detail        = array();
        foreach ($msgList as $msg) {
            $dataPesan = DB::table('message_details')->select('pesan')
                                ->where('kode_pesan', $msg->kode_pesan)
                                ->orderBy('message_details.created_at', 'DESC')->first();
            $pesan     = $dataPesan->pesan;
            $ket       = $msg->created_at;

            if(strlen($pesan) > 30)
            {
                $pesan = substr($pesan, 0, 30)."...";
            }

            $start  = date_create($msg->created_at);
        		$end    = Carbon::now();
        		$inter  = date_diff($start, $end);

        		if($inter->format("%a") == 0){
        			if($inter->format("%H") == 0){
        				if($inter->format("%i") == 0){
        					$ket = "a few seconds ago";
        				}else if($inter->format("%i") == 1){
        					$ket = "a minute ago";
        				}else{
        					$ket = $inter->format("%i minutes ago");
        				}
        			}else if($inter->format("%H") == 1){
        				$ket   = "an hour ago";
        			}else{
        				$ket   = $inter->format("%H hours ago");
        			}

        		}else if($inter->format("%a") == 1){
        			$ket   = "yesterday";
        		}else if($inter->format("%a") <= 7){
        			$ket   = $inter->format("%a days ago");
        		}else{
        			$ket   = date_format($start,"d M Y H:i:s");
        		}

            $timeMsg = date_format($start,"H:i:s");
            $dateMsg = date_format($start,"d M Y");

            $detail[$msg->kode_pesan]['pesan']    = $pesan;
            $detail[$msg->kode_pesan]['ketWaktu'] = $ket;
            $detail[$msg->kode_pesan]['timeMsg']  = $timeMsg;
            $detail[$msg->kode_pesan]['dateMsg']  = $dateMsg;
            $detail[$msg->kode_pesan]['foto']     = Testimony::checkPhoto('pengguna', $msg->foto, $msg->username);
        }

        return $detail;
    }
}
