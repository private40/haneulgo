<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
      protected $guarded = ['id', 'created_at'];
      
      public static function actionTable($id, $nama_bank)
      {
        return '<td>
                      <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#form" onclick="formRekening('."'".$id."'".')"><i class="fa fa-pencil"></i> Edit</button>
                      <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete" onclick="deleteItem('."'".$id."', '".$nama_bank."', '/rekening'".')"><i class="fa fa-times"></i> Delete</button>
                </td>';
      }
}
