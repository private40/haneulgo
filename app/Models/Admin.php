<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\Package;
use App\Models\Pengguna;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Rekening;

class Admin extends Model
{
  public static function deleteImage($lokasi)
  {
      $filename =public_path().'/images/'.$lokasi;
      if(file_exists($filename)){
          File::delete($filename);
      }
      return 'berhasil';
  }

  public static function saveToFile($text, $namaFile)
  {
      //memasukkan data kedalam file
      return Storage::put('page/'.$namaFile.".bgz", $text);
  }

  public static function getInfoPage($page)
  {
      $infoPage   = array();
      $namaFile   = $page.".bgz";
      //mengecek keberadaan file tersebut
      $fileExsist = Storage::disk('local')->exists('page/'.$namaFile);

      //jika ada, file dibaca.
      if($fileExsist){
          $infoPage['content'] = Storage::get('page/'.$namaFile);
      }else{
          $infoPage['content'] = '';
      }

      switch ($page) {
        case 'aturan_penggunaan':
          $infoPage['title']       = "Aturan Penggunaan";
          $infoPage['description'] = "Aturan Penggunaan Haneul Go.";
          break;

        case 'cara_pesan':
          $infoPage['title']       = "Cara Pesan";
          $infoPage['description'] = "Informasi seputar cara pemesanan travel.";
          break;

        case 'syarat_dan_ketentuan':
          $infoPage['title']       = "Syarat dan Ketentuan";
          $infoPage['description'] = "Informasi seputar syarat dan ketentuan pemesanan travel.";
          break;

        case 'hubungi_kami':
          $infoPage['title']       = "Hubungi Kami";
          $infoPage['description'] = "Daftar kontak Haneul Go.";
          break;

        case 'pusat_bantuan':
          $infoPage['title']       = "Pusat Bantuan";
          $infoPage['description'] = "Informasi seputar syarat dan ketentuan pemesanan travel.";
          break;

        case 'kebijakan_privasi':
          $infoPage['title']       = "Kebijakan Privasi";
          $infoPage['description'] = "Informasi seputar kebijakan privasi member Haneul Go.";
          break;

        case 'tentang_haneul':
          $infoPage['title']       = "Tentang";
          $infoPage['description'] = "Informasi seputar Haneul Go.";
          break;

        default:
          $infoPage['title']       = "Tentang";
          $infoPage['description'] = "Informasi seputar Haneul Go.";
          break;
      }

      return $infoPage;
  }

  /*
    This is script to generate table automatically
    $coloumnHeader = Coloumn Header of table
    $content       = Content of table. its from database result
    $coloumnIndewx = name of column of database table
    $tableType     = used to define model to get Action content
  */

  public static function createTable($columnHeader = array(), $content = array(), $columnIndex = array(), $tableType = '')
  {
        $no         = 1;
        $indexArray = array();
        $table      = '<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"><thead><tr>';

        foreach($columnHeader as $header){
            $table .= '<th>'.$header.'</th>';
        }

        $table .= '</tr> </thead> <tbody>';

        foreach($content as $content)
        {
            $table .= '<tr><td>'.$no.'</td>';

            foreach($columnIndex as $index){
                if($content->$index >= 1000){
                    $table .= '<td>Rp. '.number_format($content->$index,0,'','.').',-</td>';
                }elseif(strpos($index, "-") > 0){
                    $index = substr($index, strpos($index, "-") + 1);

                    if(strlen($content->$index) > 80){
                        $table .= '<td>'.substr(strip_tags($content->$index), 0, 80).'...</td>';
                    }else{
                        $table .= '<td>'.strip_tags($content->$index).'</td>';
                    }
                }else{
                    $table .= '<td>'.$content->$index.'</td>';
                }
            }

            switch ($tableType) {
                case 'products':
                    $table .= Product::actionTable($content->id, $content->nama_produk);
                    break;

                case 'admin':
                    $table .= Pengguna::actionTable($content->username, $tableType);
                    break;

                case 'member':
                    $table .= Pengguna::actionTable($content->username, $tableType);
                    break;

                case 'rekening':
                    $table .= Rekening::actionTable($content->id, $content->nama_bank);
                    break;

                case 'category':
                    $table .= Product_category::actionTable($content->id, $content->nama_kategori);
                    break;

                case 'destination':
                    $table .= Destination::actionTable($content->id, $content->nama_destinasi);
                    break;

                default:
                    $table .= Package::actionTable($content->kode_paket, $content->nama_paket, $tableType);
                    break;
            }

            $table .= '</tr>';
            $no++;
        }

        $table .= '</tbody> </table>';

        return $table;
  }
}
