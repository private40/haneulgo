<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use App\Models\Admin;
use App\Models\Pengguna;
use App\Models\Order;
use App\Models\Testimony;
use App\Models\Message;

class PenggunaController extends Controller
{
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();
    private $testimony    = array();
    private $photo_testi  = array();
    private $pesanCount   = '';

    public function __construct()
    {
        $this->unreadMsg      = Message::where('read_admin', 0)->count();
        $this->lastMsg        = Message::getLast(4);
        $this->msgDetail      = Message::msgDetail($this->lastMsg);
        $this->countConfirm   = Order::where('status', 'Pembayaran')->count();
        $this->testimony      = Testimony::getPublication();
        $this->pesanCount     = Message::getUnread(session()->get('username'))->count();

        foreach ($this->testimony as $testimony) {
          $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }
    }

    public function memberList()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $columnHeader         = array('No', 'Username', 'Email', 'Nama Lengkap', 'Action');
        $columnIndex          = array('username', 'email', 'nama_lengkap');
        $penggunaList         = Pengguna::where('status', 'Member')->get();

        $data['_modal']       = view('backend/pengguna/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $penggunaList, $columnIndex, 'member');
        $data['tipe']         = "Member";

        return view('backend/pengguna/MainPengguna', $data);
    }

    public function adminList()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $columnHeader         = array('No', 'Username', 'Email', 'Nama Lengkap', 'Action');
        $columnIndex          = array('username', 'email', 'nama_lengkap');
        $penggunaList         = Pengguna::where('status', 'Admin')->get();

        $data['_modal']       = view('backend/pengguna/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $penggunaList, $columnIndex, 'admin');
        $data['tipe']         = "Admin";

        return view('backend/pengguna/MainPengguna', $data);
    }

    public function password()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        return view('backend/pengguna/PassPengguna', $data);
    }

    public function upload(Request $request, $username)
    {
        $time      = Carbon::now();
        $image     = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        $directory = 'pengguna' . '/' . $username;
        $filename  = $username."-".date_format($time,'md').date_format($time,'Hi').".".$extension;
        $upload    = $image->storeAs($directory, $filename, 'public');

        if ($upload) {
            $dataPengguna   = Pengguna::where('username', $username)->first();
            $lokasi         = $directory.'/'.$dataPengguna->foto;
            Admin::deleteImage($lokasi);

            $data['foto']   = $filename;
            Pengguna::where('username', $username)->update($data);

            if($username == Session::get('username')){
              $dataPengguna = Pengguna::where('username', $username)->first();
              $data['foto'] = Testimony::checkPhoto('pengguna', $dataPengguna->foto, $dataPengguna->username);

              session($data);
            }
            return response()->json($upload, 200);
        }else{
            return response()->json('error', 400);
        }
    }

    public function store(Request $request)
    {
        $validate['username']      = 'required';
        $validate['email']         = 'required|email';
        $validate['nama_lengkap']  = 'required';
        $validate['jenis_kelamin'] = 'required';
        $msg_error['required']     = 'Tidak boleh kosong';
        $msg_error['numeric']      = 'Alamat email tidak valid';

        $this->validate($request, $validate, $msg_error);

        $data['username']      = strtolower($request->username);
        $data['email']         = $request->email;
        $data['nama_lengkap']  = $request->nama_lengkap;
        $data['jenis_kelamin'] = $request->jenis_kelamin;
        $id                    = $request->id;
        $dataPengguna          = Pengguna::where('username', $data['username'])->count();

        if(empty($id)){
            if($dataPengguna > 0){
                $flash['info'] = 'Username <strong>'.$data['username'].'</strong> telah digunakan! Data tidak bisa disimpan!';
                $flash['kelas']= 'warning';
                session($flash);

                return redirect()->back();
            }else{
                $data['password'] = Pengguna::encry($request->password);
                $data['status']   = 'Admin';
                Pengguna::create($data);
                $flash['info']    = 'Data <strong>'.$data['username'].'</strong> telah berhasil disimpan!';
            }
        }else{
            Pengguna::where('username', $data['username'])->update($data);
            $flash['info'] = 'Data <strong>'.$data['username'].'</strong> telah berhasil diperbarui!';
        }

        $flash['kelas'] = 'success';
        session($flash);

        if($data['username'] == Session::get('username')){
            return redirect()->back();
        }

        return redirect('admin/admin_list.html');
    }

    public function storePass(Request $request)
    {
        $dataPengguna         = Pengguna::where('username', session()->get('username'))->first();
        $passwordLama         = Pengguna::encry($request->password_lama);

        if($passwordLama == $dataPengguna->password){
          $data['password']   = Pengguna::encry($request->password);
          Pengguna::where('username', session()->get('username'))->update($data);

          $flash['info'] = 'Password berhasil diubah!';
          $flash['kelas']= 'success';
        }else{
          $flash['info'] = 'Password Lama tidak cocok! Password tidak bisa diperbarui!';
          $flash['kelas']= 'warning';
        }

        session($flash);
        return redirect()->back();
    }

    public function show(Request $request)
    {
        $dataPengguna         = Pengguna::where('username', $request->username)->first();
        $tgl                  = date_create($dataPengguna->created_at);
        $tglDaftar            = date_format($tgl, "d F Y, H:i:s");

        $data['nama_lengkap'] = $dataPengguna->nama_lengkap;
        $data['email']        = $dataPengguna->email;
        $data['jenis_kelamin']= ($dataPengguna->jenis_kelamin == 1 ? "Laki - Laki" : "Perempuan");
        $data['status']       = $dataPengguna->status;
        $data['created_at']   = $tglDaftar;
        $data['foto']         = Testimony::checkPhoto('pengguna', $dataPengguna->foto, $dataPengguna->username);
        $data['count_travel'] = Order::countTravel($request->username);

        echo json_encode($data);
    }

    public function edit($username)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        $data['jenis_kelamin']= 1;
        $data['menu_user']    = true;
        $data['info']         = Session::get('success');

        if($username != 'new'){
          $dataPengguna         = Pengguna::where('username', $username)->first();
          $tgl                  = date_create($dataPengguna->created_at);
          $tglDaftar            = date_format($tgl, "d F Y, H:i:s");

          $data['username']     = $username;
          $data['nama_lengkap'] = $dataPengguna->nama_lengkap;
          $data['email']        = $dataPengguna->email;
          $data['jenis_kelamin']= $dataPengguna->jenis_kelamin;
          $data['status']       = $dataPengguna->status;
          $data['created_at']   = $tglDaftar;
          $data['foto']         = Testimony::checkPhoto('pengguna', $dataPengguna->foto, $username);
          $data['id']           = $dataPengguna->id;
        }

        return view('backend/pengguna/FormPengguna', $data);
    }

    public function update(Request $request)
    {
        $username             = $request->username;
        if($username == session()->get('username')){
            $dataPengguna     = Pengguna::where('username', $username)->first();
            $password         = Pengguna::encry($request->password);

            if($password      == $dataPengguna->password){
                $validasi['nama_lengkap']  = 'required';
                $validasi['email']         = 'required|email';
                $validasi['jenis_kelamin'] = 'required';

                $msgError['required']      = 'Tidak boleh kosong!';
                $msgError['email']         = 'Alamat email tidak valid!';

                $this->validate($request,$validasi,$msgError);

                $data['nama_lengkap']      = $request->nama_lengkap;
                $data['email']             = $request->email;
                $data['jenis_kelamin']     = $request->jenis_kelamin;

                Pengguna::where('username',$username)->update($data);

                return redirect()->back();
            }else{
                Session::flush();
                return redirect('/login.html');
            }
        }else{
            Session::flush();
            return redirect('/login.html');
        }
    }

    public function destroy(Request $request, $username)
    {
        $dataPengguna   = Pengguna::where('username', $username)->first();
        $lokasi         = 'pengguna/'.$username.'/'.$dataPengguna->foto;

        Pengguna::where('username', $username)->delete();
        Admin::deleteImage($lokasi);

        $flash['info'] = 'Data <strong>'.$username.'</strong> berhasil dihapus!';
        $flash['kelas']= 'warning';
        session($flash);

        return redirect()->back();
    }

    public function login(Request $request)
    {
        $data['username']      = $request->username;
        $password              = Pengguna::encry($request->password);
        $dataPengguna          = Pengguna::where('username', $data['username'])->first();
        $dataEmail             = Pengguna::where('email', $data['username'])->first();
        $feedback['kelas']     = "alert-warning";

        if(!$dataPengguna and !$dataEmail)
        {
            $feedback['info']  = "<strong>Perhatian!</strong> Username atau Email Anda tidak terdaftar!";
            // $feedback['info']  = "<strong>Perhatian!</strong> Username tidak terdaftar!";
            session($feedback);
            return redirect('/login.html');
        }else{
            (!$dataPengguna ? $dataPengguna = $dataEmail : '');

            if($dataPengguna->password == $password)
            {
                $data['username']     = $dataPengguna->username;
                $data['foto']         = Testimony::checkPhoto('pengguna', $dataPengguna->foto, $dataPengguna->username);
                $data['nama_lengkap'] = $dataPengguna->nama_lengkap;
                $data['status']       = $dataPengguna->status;
                $kode_unik            = $dataPengguna->kode_unik;

                session($data);

                $login['last_login']  = Carbon::now();
                $login['last_ip']     = \Request::ip();
                Pengguna::where('username', $data['username'])->update($login);

                if(!empty($kode_unik)){
                    return redirect('/activation.html');
                }else if($data['status'] == "Member"){
                    return redirect('/pengguna/'.$data['username']);
                }else{
                    return redirect('/admin.html');
                }
            }else{
                $feedback['info']     = "<strong>Perhatian!</strong> Password Anda salah!";
                $feedback['info_user']     = $data['username'];
                session($feedback);
                return redirect('/login.html');
            }
        }
    }

    public function checkUsername(Request $request)
    {
        $dataPengguna        = Pengguna::where('username', $request->user)->first();

        if(!$dataPengguna)
        {
            echo "empty";
        }else{
            echo "ada";
        }
    }

    public function register(Request $request)
    {
        $validate['nama_lengkap']   = 'required';
        $validate['email']          = 'required|email';
        $validate['username']       = 'required';
        $validate['password']       = 'required';
        $msg_error['required']      = 'Tidak boleh kosong';
        $msg_error['email']         = 'Email tidak valid';

        $this->validate($request,$validate,$msg_error);

       $data['nama_lengkap'] = $request->nama_lengkap;
       $data['email']        = $request->email;
       $data['username']     = $request->username;
       $data['password']     = Pengguna::encry($request->password);
       $data['kode_unik']    = Pengguna::generateKode();
       $cek_pengguna         = Pengguna::where('username', $data['username'])->first();
       $cek_email            = Pengguna::where('email', $data['email'])->first();
       $url                  = '/pengguna/'.$data['username'];
       $now                  = Carbon::now();

       if(!$cek_pengguna){
          Pengguna::create($data);

            Mail::send('frontend.login.Activation', ['nama_lengkap' => $request->nama_lengkap, 'kode_unik' => $data['kode_unik'], 'email' => $data['email']], function ($message) use ($request)
            {
                $message->subject('Haneul Go Member Activation');
                $message->from('administrator@haneul-go.com', 'Admin Haneul Go');
                $message->to($request->email);
            });

          $user['user-temp'] = $data['username'];
          session($user);

          return redirect('/activation.html');
       }else{
          return redirect('/login.html');
       }
    }

    public function resendVerification()
    {
        $dataPengguna = Pengguna::where('username', session()->get('user-temp'))->first();
        Mail::send('frontend.login.Activation', ['nama_lengkap' => $dataPengguna->nama_lengkap, 'kode_unik' => $dataPengguna->kode_unik, 'email' => $dataPengguna->email], function ($message) use ($dataPengguna)
        {
            $message->subject('Haneul Go Member Activation');
            $message->from('administrator@haneul-go.com', 'Admin Haneul Go');
            $message->to($dataPengguna->email);
        });

        return redirect('/activation.html');
    }

    public function forgetPassword(Request $request)
    {
        $validate['email']          = 'required|email';
        $msg_error['required']      = 'Tidak boleh kosong';
        $msg_error['email']         = 'Email tidak valid';

        $this->validate($request,$validate,$msg_error);


    }

    public function showPengguna($username)
    {

        if(session()->get('status') != 'Member'){
          return redirect('/admin.html');
        }elseif($username == session()->get('username')){
          $dataPengguna            = Pengguna::where('username', $username)->first();
          $data['username']        = $username;
          $data['nama_lengkap']    = $dataPengguna->nama_lengkap;
          $data['email']           = $dataPengguna->email;
          $data['jenis_kelamin']   = $dataPengguna->jenis_kelamin;
          $data['foto']            = session()->get('foto');

          $data['testimony']       = $this->testimony;
          $data['photo_testi']     = $this->photo_testi;
          $data['menu_pengguna']   = true;
          $data['pesan']           = Message::getUnread($username)->count();
          $data['data_pesan']      = Message::where('username', $username)->first();
          $data['transaksi']       = Order::getUnfinish($username)->count();
          $data['list_transaksi']  = Order::where('username', $username)->orderBy('kode_order', 'DESC')->get();
          $data['detailTransaksi'] = Order::getMoreDetail($data['list_transaksi']);
          $data['list_testimoni']  = Testimony::where('username', $username)->orderBy('id', 'DESC')->get();
          $data['detailTestimoni'] = Testimony::getMoreDetail($data['list_testimoni']);
          $data['pesanCount']      = $this->pesanCount;
          $data['no']              = 1;
          $data['no_testi']        = 1;

          if($data['data_pesan']){
            $data['tglPesan']      = "";
            $data['kodePesan']     = $data['data_pesan']->id;
            $data['list_pesan']    = Message::getMessage($data['kodePesan']);
            $data['dateTimeMsg']   = Message::dateTimeDetail($data['list_pesan']);
            $data['dataMsg']       = Message::where('id', $data['kodePesan'])->first();
          }

          return view('frontend/pengguna/MainPengguna', $data);
        }else{
            return redirect('/');
        }
    }

    public function showMessage($username)
    {
        if(session()->get('status') != 'Member'){
          return redirect('/admin.html');
        }elseif($username == session()->get('username')){
          $data['username']        = $username;
          $data['testimony']       = $this->testimony;
          $data['photo_testi']     = $this->photo_testi;
          $data['menu_pengguna']   = true;
          $data['transaksi']       = Order::getUnfinish($username)->count();
          $data['pesan']           = Message::getUnread($username)->count();
          $data['data_pesan']      = Message::where('username', $username)->first();
          $data['pesanCount']      = $this->pesanCount;
          $data['no']              = 1;

          if($data['data_pesan']){
            $data['tglPesan']      = "";
            $data['kodePesan']     = $data['data_pesan']->id;
            $data['list_pesan']    = Message::getMessage($data['kodePesan']);
            $data['dateTimeMsg']   = Message::dateTimeDetail($data['list_pesan']);
            $data['dataMsg']       = Message::where('id', $data['kodePesan'])->first();
          }

          return view('frontend/pengguna/MessagePengguna', $data);
        }else{
            return redirect('/login.html');
        }
    }

    public function showOrder($username)
    {

        if(session()->get('status') != 'Member'){
          return redirect('/admin.html');
        }elseif($username == session()->get('username')){
          $data['username']        = $username;
          $data['testimony']       = $this->testimony;
          $data['photo_testi']     = $this->photo_testi;
          $data['menu_pengguna']   = true;
          $data['pesan']           = Message::getUnread($username)->count();
          $data['transaksi']       = Order::getUnfinish($username)->count();
          $data['list_transaksi']  = Order::where('username', $username)->orderBy('kode_order', 'DESC')->get();
          $data['detailTransaksi'] = Order::getMoreDetail($data['list_transaksi']);
          $data['pesanCount']      = $this->pesanCount;
          $data['no']              = 1;

          return view('frontend/pengguna/OrderPengguna', $data);
        }else{
            return redirect('/');
        }
    }

    public function showTestimony($username)
    {

        if(session()->get('status') != 'Member'){
          return redirect('/admin.html');
        }elseif($username == session()->get('username')){
          $data['username']        = $username;
          $data['testimony']       = $this->testimony;
          $data['photo_testi']     = $this->photo_testi;
          $data['menu_pengguna']   = true;
          $data['pesan']           = Message::getUnread($username)->count();
          $data['transaksi']       = Order::getUnfinish($username)->count();
          $data['list_testimoni']  = Testimony::where('username', $username)->orderBy('id', 'DESC')->get();
          $data['detailTestimoni'] = Testimony::getMoreDetail($data['list_testimoni']);
          $data['no_testi']        = 1;
          $data['pesanCount']      = $this->pesanCount;

          return view('frontend/pengguna/TestimonyPengguna', $data);
        }else{
            return redirect('/');
        }
    }

    public function remove(Request $request)
    {
        Admin::deleteImage($request->lokasi);
    }
}
