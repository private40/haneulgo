<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Order;
use App\Models\Testimony;
use App\Models\Message;
use App\Models\Rekening;
use App\Models\Kurs;
use App\Models\Pengguna;
use App\Models\Package;
use App\Models\Package_detail;

class OrderController extends Controller
{
    //backend
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $listRekening = array();
    private $listKurs     = array();
    private $countConfirm = array();

    //frontend
    private $testimony    = array();
    private $photo_testi  = array();

    public function __construct()
    {
        //backend
        $this->unreadMsg      = Message::where('read_admin', 0)->count();
        $this->lastMsg        = Message::getLast(4);
        $this->msgDetail      = Message::msgDetail($this->lastMsg);
        $this->listRekening   = Rekening::all();
        $this->listKurs       = Kurs::where('country_from', 'KRW')->get();
        $this->countConfirm   = Order::where('status', 'Pembayaran')->count();

        //frontend
        $this->testimony      = Testimony::getPublication();
        foreach ($this->testimony as $testimony) {
          $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }
    }

    public function orderList(Request $request)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['listRekening'] = $this->listRekening;
        $data['listKurs']     = $this->listKurs;
        $data['countConfirm'] = $this->countConfirm;
        $data['orderList']    = Order::where('status', 'Pemesanan')->get();
        $data['tipe']         = "Pemesanan";
        $data['label']        = "primary";
        return view('backend/order/MainOrder', $data);
    }

    public function confirmList()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['listRekening'] = $this->listRekening;
        $data['listKurs']     = $this->listKurs;
        $data['countConfirm'] = $this->countConfirm;
        $data['orderList']    = Order::where('status', 'Pembayaran')->get();
        $data['tipe']         = "Konfirmasi Pembayaran";
        $data['label']        = "warning";
        return view('backend/order/MainOrder', $data);
    }

    public function readyList()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['listRekening'] = $this->listRekening;
        $data['listKurs']     = $this->listKurs;
        $data['countConfirm'] = $this->countConfirm;
        $data['orderList']    = Order::where('status', 'Terkonfirmasi')->get();
        $data['tipe']         = "Siap Berangkat";
        $data['label']        = "success";
        return view('backend/order/MainOrder', $data);
    }

    public function completedList()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['listRekening'] = $this->listRekening;
        $data['listKurs']     = $this->listKurs;
        $data['countConfirm'] = $this->countConfirm;
        $data['orderList']    = Order::where('status', 'Selesai')->get();
        $data['tipe']         = "Travel Selesai";
        $data['label']        = "default";
        return view('backend/order/MainOrder', $data);
    }

    public function detail(Request $request)
    {
        $dataOrder                = Order::getDetail($request->kode_order);

        if(!$dataOrder){
            $dataOrder            = Order::getDetailNoRek($request->kode_order);
            $tglTransfer          = "belum dibayar";
            $data['nominal_transfer'] = 'Rp. 0,-';
            $data['tujuan_transfer']  = "belum dibayar";
        }else{
            $tgl                  = date_create($dataOrder->tgl_transfer);
            $tglTransfer          = date_format($tgl, "d F Y");
            $data['nominal_transfer'] = 'Rp. '.number_format($dataOrder->nominal_transfer,0,'','.').',-';
            $data['tujuan_transfer']  = $dataOrder->nama_bank." - ".$dataOrder->no_rek;
        }
        $tgl                      = date_create($dataOrder->created_at);
        $tglTransaksi             = date_format($tgl, "H:i:s, d F Y");
        $tgl                      = date_create($dataOrder->tgl_berangkat);
        $tglBerangkat             = date_format($tgl, "d F Y");
        $tgl                      = date_create($dataOrder->tgl_pulang);
        $tglPulang                = date_format($tgl, "d F Y");

        $data['created_at']       = $tglTransaksi;
        $data['username']         = $dataOrder->username;
        $data['nama_lengkap']     = $dataOrder->nama_lengkap;
        $data['email']            = $dataOrder->email;
        $data['foto']             = Testimony::checkPhoto('pengguna', $dataOrder->foto, $dataOrder->username);
        $data['tgl_travel']       = $tglBerangkat.' s/d '.$tglPulang;
        $data['nama_paket']       = $dataOrder->nama_paket;
        $data['jumlah_orang']     = $dataOrder->jumlah_orang;
        $data['harga_paket']      = 'Rp. '.number_format($dataOrder->harga_paket,0,'','.').',-';
        $data['total_biaya']      = 'Rp. '.number_format($dataOrder->total,0,'','.').',-';
        $data['total']            = $dataOrder->total;
        $data['tgl_transfer']     = $tglTransfer;
        $data['keterangan']       = $dataOrder->keterangan;
        $data['status']           = $dataOrder->status;

        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $kode_order               = $request->kode_order;
        $data['total']            = $request->total_biaya;
        $data['keterangan']       = $request->keterangan;
        Order::where('kode_order', $kode_order)->update($data);

        $flash['info']  = 'Data order <strong>'.$kode_order.'</strong> berhasil diperbarui!';
        $flash['kelas'] = 'success';
        session($flash);
    }

    public function updateDone(Request $request)
    {
        $kode_order               = $request->kode_order;
        $data['status']           = $request->status;
        Order::where('kode_order', $kode_order)->update($data);

        $flash['info']  = 'Data order '.$kode_order.' berhasil '.$data['status'].'!';
        $flash['kelas'] = 'success';
        session($flash);
    }

    public function confirmation($kode_order, Request $request)
    {
        $data['status']           = "Terkonfirmasi";
        Order::where('kode_order', $kode_order)->update($data);

        $flash['info']  = 'Order <strong>'.$kode_order.'</strong> berhasil dikonfirmasi!';
        $flash['kelas'] = 'success';
        session($flash);
    }

    public function store(Request $request)
    {
        $data['kode_order']     = Order::getOrderCode();
        $data['username']       = session()->get('username');
        $data['jumlah_orang']   = $request->jumlah_orang;
        $data['kode_paket']     = $request->kode_paket;
        $data['harga_paket']    = $request->harga_paket;
        $data['keterangan']     = (empty($request->keterangan) ? '-' : $request->keterangan);
        $data['total']          = Order::generalTotal($request->total);
        $data['tgl_berangkat']  = $request->tgl_berangkat;
        $start_date             = strtotime($data['tgl_berangkat']);
        $end_date               = strtotime("+5 day", $start_date);
        $data['tgl_pulang']     = date('Y-m-d', $end_date);
        $hasilFitur             = $request->input('fitur');
        $hasil                  = '';

        foreach ($hasilFitur as $fitur) {
            $hasil              = $hasil.','.$fitur;
        }

        $data['fitur_tambahan'] = $hasil;

        Order::create($data);

        return redirect('pay_order/'.$data['kode_order']);
    }

    public function pay(Request $request)
    {
        $kode_order               = $request->kode_order;
        $data['rek_tujuan']       = $request->rek_tujuan;
        $data['tgl_transfer']     = $request->tgl_transfer;
        $data['nominal_transfer'] = $request->nominal_transfer;
        $data['status']           = 'Pembayaran';

        Order::where('kode_order', $kode_order)->update($data);
        $request->session()->flash('info', 'Order <strong>'.$kode_order.'</strong> berhasil dibayar!');

        return redirect('order/'.$kode_order);
    }

    public function showOrder($kode_order)
    {
        $dataOrder                = Order::getDetailNoRek($kode_order);
        $data['testimony']        = Testimony::getPublication();
        $data['photo_testi']      = array();
        $data['pesanCount']       = Message::getUnread(session()->get('username'))->count();

        foreach ($data['testimony'] as $testimony) {
            $data['photo_testi'][$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }

        if((!$dataOrder) or ($dataOrder->username != session()->get('username')))
        {
            $data['title']        = "Detail Pemesanan";
            $data['description']  = "Detail pemesanan yang Anda cari tidak ditemukan!";
            $data['textBack']     = "Detail pemesanan yang Anda cari tidak ditemukan! <br><a href='".url('pengguna/'.session()->get('username'))."' > Klik disini untuk kembali ke halaman pengguna.</a>";

            return view('frontend/main/NotFound', $data);
        }

        $notes['syarat_ketentuan'] = Admin::getInfoPage('syarat_dan_ketentuan');
        $notes['hubungi_kami']     = Admin::getInfoPage('hubungi_kami');
        $data['_notes']            = view('frontend/main/NotesTravel', $notes);

        $tgl                      = date_create($dataOrder->created_at);
        $tglTransaksi             = date_format($tgl, "H:i:s, d F Y");
        $tgl                      = date_create($dataOrder->tgl_berangkat);
        $tglBerangkat             = date_format($tgl, "d F Y");
        $tgl                      = date_create($dataOrder->tgl_pulang);
        $tglPulang                = date_format($tgl, "d F Y");

        $data['kode_order']       = $kode_order;
        $data['created_at']       = $tglTransaksi;
        $data['username']         = $dataOrder->username;
        $data['nama_lengkap']     = $dataOrder->nama_lengkap;
        $data['email']            = $dataOrder->email;
        $data['foto']             = Testimony::checkPhoto('pengguna', $dataOrder->foto, $dataOrder->username);
        $data['tgl_travel']       = $tglBerangkat.' s/d '.$tglPulang;
        $data['nama_paket']       = $dataOrder->nama_paket;
        $data['jumlah_orang']     = $dataOrder->jumlah_orang;
        $data['harga_paket']      = 'Rp. '.number_format($dataOrder->harga_paket,0,'','.').',-';
        $data['total']            = 'Rp. '.number_format($dataOrder->total,0,'','.').',-';
        $data['keterangan']       = $dataOrder->keterangan;
        $data['status']           = $dataOrder->status;
        $data['label_status']     = Order::labelStatus($data['status']);

        if($dataOrder->rek_tujuan > 0){
            $tgl                      = date_create($dataOrder->tgl_transfer);
            $tglTransfer              = date_format($tgl, "d F Y");
            $dataRekening             = Rekening::where('id', $dataOrder->rek_tujuan)->first();
            $data['nominal_transfer'] = 'Rp. '.number_format($dataOrder->nominal_transfer,0,'','.').',-';
            $data['tujuan_transfer']  = $dataRekening->nama_bank." <br> <strong>".$dataRekening->no_rek.'</strong><br> a/n '.$dataRekening->pemilik_rek;
            $data['tgl_transfer']     = $tglTransfer;
        }else{
            $data['nominal_transfer'] = '<span class="italic">Pemesanan belum dibayar.</span>';
            $data['tujuan_transfer']  = '<span class="italic">Pemesanan belum dibayar.</span>';
            $data['tgl_transfer']     = '<span class="italic">Pemesanan belum dibayar.</span>';
        }

        return view('frontend/order/DetailOrder', $data);
    }

    public function payOrder($kode_order)
    {
        $dataOrder            = Order::getDetailNoRek($kode_order);

        if((!$dataOrder) or ($dataOrder->username != session()->get('username')))
        {
            return redirect('/');
        }

        $notes['syarat_ketentuan'] = Admin::getInfoPage('syarat_dan_ketentuan');
        $notes['hubungi_kami']     = Admin::getInfoPage('hubungi_kami');
        $data['_notes']            = view('frontend/main/NotesTravel', $notes);

        $tgl                      = date_create($dataOrder->created_at);
        $tglTransaksi             = date_format($tgl, "H:i:s, d F Y");
        $tgl                      = date_create($dataOrder->tgl_berangkat);
        $tglBerangkat             = date_format($tgl, "d F Y");
        $tgl                      = date_create($dataOrder->tgl_pulang);
        $tglPulang                = date_format($tgl, "d F Y");

        $data['kode_order']       = $kode_order;
        $data['created_at']       = $tglTransaksi;
        $data['username']         = $dataOrder->username;
        $data['nama_lengkap']     = $dataOrder->nama_lengkap;
        $data['email']            = $dataOrder->email;
        $data['foto']             = Testimony::checkPhoto('pengguna', $dataOrder->foto, $dataOrder->username);
        $data['tgl_travel']       = $tglBerangkat.' s/d '.$tglPulang;
        $data['nama_paket']       = $dataOrder->nama_paket;
        $data['jumlah_orang']     = $dataOrder->jumlah_orang;
        $data['harga_paket']      = 'Rp. '.number_format($dataOrder->harga_paket,0,'','.').',-';
        $data['total']            = 'Rp. '.number_format($dataOrder->total,0,'','.').',-';
        $data['fitur_tambahan']   = Order::fiturTambahan($dataOrder->fitur_tambahan);
        $data['nominal_transfer'] = $dataOrder->total;
        $data['keterangan']       = $dataOrder->keterangan;
        $data['status']           = $dataOrder->status;
        $data['label_status']     = Order::labelStatus($data['status']);
        $data['testimony']        = Testimony::getPublication();
        $data['photo_testi']      = array();
        $data['pesanCount']       = Message::getUnread(session()->get('username'))->count();
        $data['rekening']         = Rekening::all();

        foreach ($data['testimony'] as $testimony) {
            $data['photo_testi'][$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }

        return view('frontend/order/PayOrder', $data);
    }

    public function order($kode_paket)
    {
        $dataPaket               = Package::where('kode_paket', $kode_paket)->first();
        $data['pesanCount']      = Message::getUnread(session()->get('username'))->count();
        $data['testimony']       = $this->testimony;
        $data['photo_testi']     = $this->photo_testi;
        $data['packages']        = true;

        if(!$dataPaket){
            $data['title']       = "Paket Travel";
            $data['description'] = "Paket travel yang Anda cari tidak ditemukan!";
            $data['textBack']    = "Paket travel yang Anda cari tidak ditemukan! <br><a href='".url('packages.html')."' > Klik disini untuk melihat daftar paket</a>";

            return view('frontend/main/NotFound', $data);
        }

        $notes['syarat_ketentuan'] = Admin::getInfoPage('syarat_dan_ketentuan');
        $notes['hubungi_kami']     = Admin::getInfoPage('hubungi_kami');
        $data['_notes']            = view('frontend/main/NotesTravel', $notes);

        $detailPaket             = Package_detail::where('kode_paket', $kode_paket)->get();
        $data['kode_paket']      = $kode_paket;
        $data['nama_paket']      = $dataPaket->nama_paket;
        $data['deskripsi_paket'] = $dataPaket->deskripsi_paket;
        $data['harga_paket']     = 'Rp. '.number_format($dataPaket->harga_paket,0,'','.').',-';
        $data['harga']           = $dataPaket->harga_paket;
        $data['total']           = number_format($dataPaket->harga_paket,0,'','.');
        $data['foto']            = Package::photoPackage($dataPaket->foto_paket, $kode_paket);
        $data['pengguna']        = Pengguna::where('username', session()->get('username'))->first();
        $data['featureList']     = Package::where('tipe', 'feature')->get();

        foreach ($detailPaket as $detail) {
            $data['day_'.$detail->hari] = $detail->destination;
        }

        return view('frontend/order/Order', $data);
    }

    public function calculate(Request $request)
    {
        $total    = $request->harga * $request->jumlah;
        $val      = $request->val;
        foreach ($val as $value) {
            $dataPaket = Package::where('kode_paket', $value)->first();
            $total     = $total + ($dataPaket->harga_paket * $request->jumlah);
        }

        echo number_format($total,0,'','.');
    }
}
