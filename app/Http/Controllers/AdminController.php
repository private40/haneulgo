<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pengguna;
use App\Models\Rekening;
use App\Models\Kurs;

class AdminController extends Controller
{
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    public function __construct()
    {
        $this->unreadMsg    = Message::where('read_admin', 0)->count();
        $this->lastMsg      = Message::getLast(4);
        $this->msgDetail    = Message::msgDetail($this->lastMsg);
        $this->countConfirm = Order::where('status', 'Pembayaran')->count();
    }

    public function index()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        $data['countReady']   = Order::where('status', 'Terkonfirmasi')->count();
        $data['countNew']     = Pengguna::whereDate('created_at', Carbon::today())->count();
        $data['listConfirm']  = Order::where('status', 'Pembayaran')->get();
        $data['listRekening'] = Rekening::all();
        $data['listKurs']     = Kurs::where('country_from', 'KRW')->get();

        return view('backend/main/Dashboard', $data);
    }

    public function pageSetting($page)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        $data['page']         = Admin::getInfoPage($page);
        $data['nama_file']    = $page;

        return view('backend/main/Page', $data);
    }

    public function forbidden()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        return view('backend/main/403', $data);
    }

    public function storePage(Request $request)
    {
        $namaFile             = $request->nama_file;
        $text                 = $request->text;

        //menyimpan data ke dalam file
        $simpanData           = Admin::saveToFile($text, $namaFile);

        //pemberian informasi penyimpanan
        if(empty($simpanData)){
          $flash['info'] = 'Data <strong>'.$namaFile.'</strong> gagal disimpan!';
          $flash['kelas']= 'warning';
        }else{
          $flash['info'] = 'Data <strong>'.$namaFile.'</strong> berhasil disimpan.';
          $flash['kelas']= 'warning';
        }

        session($flash);
        return redirect()->back();
    }

    public function destroyFlash()
    {
        Session::forget('info');
        Session::forget('info_user');
        Session::forget('kelas');
        Session::forget('type');
    }
}
