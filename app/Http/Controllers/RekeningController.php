<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Message;
use App\Models\Order;
use App\Models\Rekening;

class RekeningController extends Controller
{

    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    public function __construct()
    {
        $this->unreadMsg      = Message::where('read_admin', 0)->count();
        $this->lastMsg        = Message::getLast(4);
        $this->msgDetail      = Message::msgDetail($this->lastMsg);
        $this->countConfirm   = Order::where('status', 'Pembayaran')->count();
    }

    public function index()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        
        $columnHeader         = array('No', 'Nama Bank', 'No Rekening', 'Pemilik Rekening', 'Action');
        $columnIndex          = array('nama_bank', 'no_rek', 'pemilik_rek');
        $rekeningList         = Rekening::all();
        
        $data['_modal']       = view('backend/rekening/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $rekeningList, $columnIndex, 'rekening');
        
        $data['rekeningList'] = Rekening::all();
        return view('backend/rekening/MainRekening', $data);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $validasi['nama_bank']   = 'required';
        $validasi['no_rek']      = 'required';
        $validasi['pemilik_rek'] = 'required';
        $msgError['required']    = 'Tidak boleh kosong!';
        
        $flash['info']  = 'Data tidak bisa disimpan! Terdapat kesalahan dalam pengisian data!';
        $flash['kelas'] = 'warning';
        session($flash);

        $this->validate($request,$validasi,$msgError);
        
        $id                  = $request->id;
        $data['nama_bank']   = $request->nama_bank;
        $data['no_rek']      = $request->no_rek;
        $data['pemilik_rek'] = $request->pemilik_rek;

        if(!empty($id)){
            Rekening::find($id)->update($data);
            $flash['info']   = 'Rekening <strong>'.$data['nama_bank'].'</strong> telah diperbarui!';
        }else{
            Rekening::create($data);
            $flash['info']   = 'Rekening <strong>'.$data['nama_bank'].'</strong> telah disimpan!';
        }

        $flash['kelas']     = 'success';
        session($flash);
        return redirect()->back();
    }

    public function show(Request $request)
    {
        $id                  = $request->id;
        $dataRekening        = Rekening::find($id);
        $data['nama_bank']   = $dataRekening->nama_bank;
        $data['no_rek']      = $dataRekening->no_rek;
        $data['pemilik_rek'] = $dataRekening->pemilik_rek;

        echo json_encode($data);
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        $dataRekening = Rekening::find($id);
        $dataRekening->delete();

        $flash['info'] = 'Rekening <strong>'.$dataRekening->nama_bank.'</strong> telah dihapus!';
        $flash['kelas']= 'warning';
        session($flash);

        return redirect()->back();
    }
}
