<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Message;
use App\Models\Package;
use App\Models\Pengguna;
use App\Models\Testimony;

class MainController extends Controller
{
    private  $testimony    = array();
    private  $photo_testi  = array();
    private  $pesanCount   = '';

    function __construct ()
    {
        $this->testimony   = Testimony::getPublication();
        $this->pesanCount  = Message::getUnread(session()->get('username'))->count();

        foreach ($this->testimony as $testimony) {
            $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }
    }

    public function index()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        return view('frontend/main/Home', $data);
    }

    public function packages()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['packages']    = true;
        $data['packageList'] = Package::where('tipe', 'Package')->get();
        $data['stylePackage']= array('table-two', 'table-one', 'table-four', 'table-three');
        $data['pesanCount']  = $this->pesanCount;

        return view('frontend/package/Package', $data);
    }

    public function register()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        return view('frontend/login/Register', $data);
    }

    public function login()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        return view('frontend/login/Login', $data);
    }

    public function page($page)
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;
        $data['page']        = Admin::getInfoPage($page);

        return view('frontend/main/Page', $data);
    }

    public function comingSoon()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        return view('frontend/main/ComingSoon', $data);
    }

    public function activeMsg()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        return view('frontend/main/ActivateMessage', $data);
    }

    public function activation($kode_unik)
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        $dataPengguna        = Pengguna::where('kode_unik', $kode_unik)->first();
        $data['username']    = $dataPengguna->username;

        $clear['kode_unik']  = '';
        Pengguna::where('username', $data['username'])->update($clear);

        return view('frontend/main/SuccessMessage', $data);
    }

    public function forgetPassword()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        return view('frontend/login/ForgetPassword', $data);
    }
}
