<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Message;
use App\Models\Message_detail;

class MessageController extends Controller
{
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    public function __construct()
    {
        $this->unreadMsg      = Message::where('read_admin', 0)->count();
        $this->lastMsg        = Message::getLast(4);
        $this->msgDetail      = Message::msgDetail($this->lastMsg);
        $this->countConfirm   = Order::where('status', 'Pembayaran')->count();
    }

    public function index()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        $data['messageList']  = Message::getLast(8);
        $data['messageDetail']= Message::msgDetail($data['messageList']);
        $data['allMsgCount']  = Message::all()->count();
        return view('backend/message/Message', $data);
    }

    public function show($kode_pesan)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        $data['messageList']  = Message::getLast(8);
        $data['messageDetail']= Message::msgDetail($data['messageList']);
        $data['allMsgCount']  = Message::all()->count();

        $data['tglPesan']     = "";
        $data['test']         = Message::getMessage($kode_pesan);
        $data['dateTimeMsg']  = Message::dateTimeDetail($data['test']);
        $data['dataMsg']      = Message::where('id', $kode_pesan)->first();
        $data['kodePesan']    = $kode_pesan;
        $data['menu_msg']     = true;

        if($data['dataMsg']->read_admin == 0)
        {
            $read['read_admin']   = 1;
            Message::where('id', $kode_pesan)->update($read);
        }

        return view('backend/message/Message', $data);
    }


    public function sendMessage(Request $request)
    {
        $dataPesan         = Message::where('username', session()->get('username'))->first();
        
        if(!$dataPesan){
            $message['username']      = session()->get('username');
            $message['read_pengguna'] = 1;
            $message['read_admin']    = 0;

            Message::create($message);

            $dataPesan           = Message::where('username', $message['username'])->first();
        }
        
        $data['kode_pesan'] = $dataPesan->id;
        $data['pesan']      = $request->pesan;
        $data['username']   = session()->get('username');
        Message_detail::create($data);
        
        $read['read_admin'] = 0;
        Message::where('id', $data['kode_pesan'])->update($read);
        
        return redirect()->to('pengguna/'.$data['username'].'/message');
    }

    public function store(Request $request)
    {
        if(empty($request->kode_pesan))
        {
            $message['username']      = session()->get('username');
            $message['read_pengguna'] = 1;

            Message::create($message);

            $dataPesan           = Message::where('username', $message['username'])->first();
            $data['kode_pesan']  = $dataPesan->id;
        }else{
            $data['kode_pesan']      = $request->kode_pesan;
        }

        $data['pesan']           = $request->pesan;
        $data['username']        = session()->get('username');

        Message_detail::create($data);

        if(session()->get('status') == "Member"){
          $read['read_admin']      = 0;
        }else{
          $read['read_pengguna']   = 0;
        }

        Message::where('id', $data['kode_pesan'])->update($read);

        return redirect()->back();
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }

    public function read(Request $request)
    {
        $username     = session()->get('username');
        $status       = session()->get('status');
        $kodePesan    = $request->kode_pesan;

        if($status == 'Member'){
            $read['read_pengguna'] = 1;
        }else{
            $read['read_admin']    = 1;
        }

        echo Message::where('id', $kodePesan)->update($read);
    }
}
