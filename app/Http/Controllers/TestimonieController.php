<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Message;
use App\Models\Testimony;

class TestimonyController extends Controller
{
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    public function __construct()
    {
        $this->unreadMsg      = Message::where('read_admin', 0)->count();
        $this->lastMsg        = Message::getLast(4);
        $this->msgDetail      = Message::msgDetail($this->lastMsg);
        $this->countConfirm   = Order::where('status', 'Pembayaran')->count();
    }

    public function index()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        $data['testimonyList']= Testimony::all();

        return view('backend/Testimony/MainTestimony', $data);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $id                  = $request->id;
        $data['testimoni']   = $request->testimoni;
        $data['username']    = session()->get('username');

        if(empty($id)){
            Testimony::create($data);
            $pesan    = "disimpan!";
        }else{
            Testimony::find($id)->update($data);
            $pesan    = "diperbarui!";
        }

        $flash['info'] = 'Testimoni Anda berhasil '.$pesan;
        $flash['kelas']= 'success';
        session($flash);

        return redirect()->back();
    }

    public function detail(Request $request)
    {
        $dataTestimony       = Testimony::find($request->id);
        $data['testimoni']   = $dataTestimony->testimoni;

        echo json_encode($data);
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $dataTestimony   = Testimony::find($id);

        if($dataTestimony->status == 0){
            $data['status'] = 1;
            $pesan          = "dipublikasikan.";
        }else{
            $data['status']  = 0;
            $pesan          = "disembunyikan.";
        }

        Testimony::where('id', $id)->update($data);

        $flash['info'] = 'Testimoni dari <strong>'.$dataTestimony->username.'</strong> berhasil '.$pesan;
        $flash['kelas']= 'success';
        session($flash);

        return redirect()->back();
    }

    public function destroy($id, Request $request)
    {
        $dataTestimony    = Testimony::find($id);
        $date             = date_create($dataTestimony->created_at);
        $date_ori         = date_format($date,"Y/m/d");

        $flash['info'] = 'Testimoni pada <strong>'.$date_ori.'</strong> berhasil dihapus!';
        $flash['kelas']= 'warning';
        session($flash);

        $dataTestimony->delete();

        return redirect()->back();
    }
}
