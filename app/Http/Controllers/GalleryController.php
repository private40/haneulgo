<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use App\Models\Admin;
use App\Models\Gallery;
use App\Models\Message;
use App\Models\Order;
use App\Models\Testimony;

class GalleryController extends Controller
{
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    private $testimony    = array();
    private $photo_testi  = array();
    private $pesanCount   = '';

    public function __construct()
    {
        //construct backend
        $this->unreadMsg    = Message::where('read_admin', 0)->count();
        $this->lastMsg      = Message::getLast(4);
        $this->msgDetail    = Message::msgDetail($this->lastMsg);
        $this->countConfirm = Order::where('status', 'Pembayaran')->count();


        //construct frontend
        $this->testimony   = Testimony::getPublication();
        $this->pesanCount  = Message::getUnread(session()->get('username'))->count();

        foreach ($this->testimony as $testimony) {
            $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }
    }

    //frontend
    public function index()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;
        $data['gallery']     = true;
        $data['jenis']       = Gallery::getJenis();
        $data['galleryList'] = Gallery::select('*')->paginate(16);
        $data['fotoGallery'] = array();
        foreach ($data['galleryList'] as $gallery) {
            $data['fotoGallery'][$gallery->id]  = Testimony::checkPhoto('gallery', $gallery->foto_galeri, $gallery->id);
            $data['thumbGallery'][$gallery->id] = Testimony::checkPhoto('gallery', 'thumb-'.$gallery->foto_galeri, $gallery->id);
        }

        return view('frontend/gallery/Gallery', $data);
    }

    //backend
    public function show()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $data['galleryList']  = Gallery::select('*')->paginate(15);
        $data['fotoGallery']  = array();
        $data['_modal']       = view('backend/gallery/_modal');
        foreach ($data['galleryList'] as $gallery) {
            $data['fotoGallery'][$gallery->id]  = Testimony::checkPhoto('gallery', $gallery->foto_galeri, $gallery->id);
            $data['thumbGallery'][$gallery->id] = Testimony::checkPhoto('gallery', 'thumb-'.$gallery->foto_galeri, $gallery->id);
        }
        return view('backend/gallery/MainGallery', $data);
    }

    public function upload(Request $request)
    {
        $id        = Gallery::getAutoID();
        $image     = $request->file('file');
        $directory = 'gallery' . '/' . $id;
        $time      = Carbon::now();
        $extension = $image->getClientOriginalExtension();
        $filename  = $id."-".date_format($time,'md').date_format($time,'Hi').".".$extension;
        $upload    = $image->storeAs($directory, $filename, 'public');

        $path      = public_path().'/images/' . $directory . '/thumb-'. $filename;
        $thumb     = Image::make($image->getRealPath());
        $thumbSize = 261;
        $upThumb   = $thumb->fit($thumbSize)->save($path);

        if($upload) {
            $data['id']          = $id;
            $data['nama_galeri'] = $request->nama_galeri;
            $data['jenis']       = $request->jenis;
            $data['foto_galeri'] = $filename;
            $data['username']    = session()->get('username');
            Gallery::create($data);

            return response()->json($upload, 200);
        }else{
            return response()->json('error', 400);
        }
    }

    public function destroy(Request $request, $username)
    {
        $dataPengguna   = Pengguna::where('username', $username)->first();
        $lokasi         = 'pengguna/'.$username.'/'.$dataPengguna->foto;

        Pengguna::where('username', $username)->delete();
        Admin::deleteImage($lokasi);

        $flash['info'] = 'Data <strong>'.$username.'</strong> berhasil dihapus!';
        $flash['kelas']= 'warning';
        session($flash);

        return redirect()->back();
    }

    public function remove(Request $request)
    {
        Admin::deleteImage($request->lokasi);
        $dataRequest = Gallery::normalization($request->lokasi);
        $destroy     = Gallery::find($dataRequest[1])->delete();

        if($destroy){
            $flash['info'] = 'Data Galeri <strong>'.$dataRequest[2].'</strong> berhasil dihapus!';
            $flash['kelas']= 'warning';
        }else{
            $flash['info'] = 'Data Galeri <strong>'.$dataRequest[3].'</strong> gagal dihapus!';
            $flash['kelas']= 'danger';
        }

        session($flash);

        return redirect()->back();
    }

    public function detail(Request $request)
    {
        $dataGallery     = Gallery::find($request->id);

        if($dataGallery){
            $data['id']          = $request->id;
            $data['nama_galeri'] = $dataGallery->nama_galeri;
            $data['jenis']       = $dataGallery->jenis;
            $data['foto_galeri'] = $dataGallery->foto_galeri;
        }else{
            $data['id']          = $request->id;
            $data['nama_galeri'] = '-';
            $data['jenis']       = '-';
            $data['foto_galeri'] = '-';
        }

        echo json_encode($data);

    }

    public function store(Request $request)
    {
        $dataGallery     = Gallery::find($request->id);

        if($dataGallery){
            $data['nama_galeri'] = $request->nama_galeri;
            $data['jenis']       = $request->jenis;

            Gallery::where('id', $request->id)->update($data);

            $flash['info']       = 'Data foto <strong>'.$dataGallery->foto_galeri.'</strong> berhasil diperbarui!';
            $flash['kelas']      = 'success';
        }else{
            $flash['info']       = 'Data foto <strong>'.$request->id.'</strong> gagal diperbarui!';
            $flash['kelas']      = 'danger';
        }

        session($flash);
        return redirect()->back();
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }
}
