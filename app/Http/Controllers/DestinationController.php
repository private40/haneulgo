<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Admin;
use App\Models\Destination;
use App\Models\Message;
use App\Models\Order;
use App\Models\Package;
use App\Models\Pengguna;
use App\Models\Testimony;

class DestinationController extends Controller
{
    //front end
    private  $testimony    = array();
    private  $photo_testi  = array();
    private  $pesanCount   = '';

    //back end
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    function __construct ()
    {
        //front end
        $this->testimony   = Testimony::getPublication();
        $this->pesanCount  = Message::getUnread(session()->get('username'))->count();

        foreach ($this->testimony as $testimony) {
            $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }

        //backend
        $this->unreadMsg    = Message::where('read_admin', 0)->count();
        $this->lastMsg      = Message::getLast(4);
        $this->msgDetail    = Message::msgDetail($this->lastMsg);
        $this->countConfirm = Order::where('status', 'Pembayaran')->count();
    }

    public function index()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $columnHeader         = array('No', 'Nama Destinasi', 'Keterangan', 'Action');
        $columnIndex          = array('nama_destinasi', 'sub-keterangan_destinasi');
        $destinationList      = Destination::all();

        $data['_modal']       = view('backend/destination/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $destinationList, $columnIndex, 'destination');

        return view('backend/destination/MainDestination', $data);
    }

    public function form($id, Request $request)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $data['menu_destination'] = true;

        if($id != 'new'){
            $dataDestinasi    = Destination::find($id);
            $foto             = $dataDestinasi->foto_destinasi;
        }else{
            $dataDestinasi    = Destination::where([
                                    ['username',    '=', session()->get('username')],
                                    ['nama_destinasi', '=', ''],
                                ])->first();

            if(!$dataDestinasi){
                $new['username'] = session()->get('username');
                Destination::create($new);

                $dataDestinasi   = Destination::where([
                                    ['username',    '=', session()->get('username')],
                                    ['nama_destinasi', '=', ''],
                                ])->first();
            }

            $foto                = 'default.png';
        }

        $data['id']                   = $dataDestinasi->id;
        $data['nama_destinasi']       = $dataDestinasi->nama_destinasi;
        $data['keterangan_destinasi'] = $dataDestinasi->keterangan_destinasi;
        $data['foto_destinasi']       = Testimony::checkPhoto('destination', $foto, $id);
        $data['type']                 = $request->type;

        return view('backend/destination/FormDestination', $data);
    }

    public function destroy($id)
    {
        $dataDestinasi = Destination::find($id);
        $lokasi        = 'destination/' . $id.'/'.$dataDestinasi->foto_destinasi;

        $dataDestinasi->delete();
        Admin::deleteImage($lokasi);

        $flash['info']  = 'Data <strong>'.$dataDestinasi->nama_destinasi.'</strong> telah dihapus!';
        $flash['kelas'] = 'warning';
        session($flash);

        return redirect()->back();
    }

    public function store(Request $request)
    {
        $validasi['nama_destinasi']   = 'required';
        $msgError['required']         = 'Tidak boleh kosong!';

        $flash['info']  = 'Data tidak bisa disimpan! Terdapat kesalahan dalam pengisian data!';
        $flash['kelas'] = 'warning';
        session($flash);

        $this->validate($request,$validasi,$msgError);

        $id                           = $request->id;
        $flash['type']                = $request->type;
        $data['nama_destinasi']       = $request->nama_destinasi;
        $data['keterangan_destinasi'] = $request->keterangan_destinasi;

        if(empty($id)){
            $data['foto_destinasi']   = 'default.png';
            Destination::create($data);
            $flash['info']            = 'Data destinasi <strong>'.$data['nama_destinasi'].'</strong> telah disimpan.';
        }else{
            Destination::find($id)->update($data);
            $flash['info']            = 'Data destinasi <strong>'.$data['nama_destinasi'].'</strong> telah disimpan.';
        }

        $flash['kelas'] = 'success';
        session($flash);

        return redirect('admin/destination.html');
    }

    public function upload(Request $request, $id)
    {
        $time      = Carbon::now();
        $image     = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        $directory = 'destination' . '/' . $id;
        $filename  = $id."-".date_format($time,'md').date_format($time,'hi').".".$extension;
        $upload    = $image->storeAs($directory, $filename, 'public');

        if ($upload) {
            $dataDestinasi = Destination::where('id', $id)->first();
            $lokasi        = $directory.'/'.$dataDestinasi->foto_destinasi;
            Admin::deleteImage($lokasi);

            $data['foto_destinasi']   = $filename;
            Destination::where('id', $id)->update($data);
            return response()->json($upload, 200);
        }else{
            return response()->json('error', 400);
        }
    }

    public function remove(Request $request)
    {
        Admin::deleteImage($request->lokasi);
    }

    public function selectList(Request $request)
    {
        $dataDestinasi = Destination::all();
        $select        = '';

        foreach($dataDestinasi as $destination){
            $select   .= '<option value="$destination->id">'.$destination->nama_destinasi.'</option>';
        }

        echo $select;
    }
}
