<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Message;
use App\Models\Order;
use App\Models\Product_category;

class ProductCategoryController extends Controller
{
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    public function __construct()
    {
        $this->unreadMsg      = Message::where('read_admin', 0)->count();
        $this->lastMsg        = Message::getLast(4);
        $this->msgDetail      = Message::msgDetail($this->lastMsg);
        $this->countConfirm   = Order::where('status', 'Pembayaran')->count();
    }

    public function index()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;
        
        $columnHeader         = array('No', 'Nama Kategori', 'Jumlah Produk', 'Action');
        $columnIndex          = array('nama_kategori');
        $categoryList         = Product_category::all();
        
        $data['_modal']       = view('development/backend/product_category/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $categoryList, $columnIndex, 'category');
        
        return view('development/backend/product_category/MainCategory', $data);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $validasi['nama_kategori'] = 'required';
        $msgError['required']      = 'Tidak boleh kosong!';
        
        $flash['info']  = 'Data tidak bisa disimpan! Terdapat kesalahan dalam pengisian data!';
        $flash['kelas'] = 'warning';
        session($flash);

        $this->validate($request,$validasi,$msgError);
        
        $id                    = $request->id;
        $data['nama_kategori'] = $request->nama_kategori;
        
        if(!empty($id)){
            Product_category::find($id)->update($data);
            $flash['info']   = 'Kategori <strong>'.$data['nama_kategori'].'</strong> telah diperbarui!';
        }else{
            Product_category::create($data);
            $flash['info']   = 'Kategori <strong>'.$data['nama_kategori'].'</strong> telah disimpan!';
        }

        $flash['kelas']     = 'success';
        session($flash);
        return redirect()->back();
    }

    public function destroy($id)
    {
        $dataCategory = Product_category::find($id);
        $dataCategory->delete();

        $flash['info'] = 'Kategori <strong>'.$dataCategory->nama_kategori.'</strong> telah dihapus!';
        $flash['kelas']= 'warning';
        session($flash);

        return redirect()->back();
    }
    
    public function selectList(Request $request)
    {
        $data['nama_kategori'] = trim($request->nama_kategori);
        
        if(!empty($data['nama_kategori'])){
            Product_category::create($data);
            
            $dataCategory = Product_category::all();
            $select       = '';
            
            foreach($dataCategory as $category){
                $select   .= '<option value="$category->id">'.$category->nama_kategori.'</option>';
            }
            
            echo $select;
        }else{
            echo 'empty';
        }
    }
}
