<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Admin;
use App\Models\Message;
use App\Models\Order;
use App\Models\Package;
use App\Models\Pengguna;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Testimony;

class ProductController extends Controller
{
    //front end
    private  $testimony    = array();
    private  $photo_testi  = array();
    private  $pesanCount   = '';

    //back end
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    function __construct ()
    {
        //front end
        $this->testimony   = Testimony::getPublication();
        $this->pesanCount  = Message::getUnread(session()->get('username'))->count();

        foreach ($this->testimony as $testimony) {
            $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }

        //backend
        $this->unreadMsg    = Message::where('read_admin', 0)->count();
        $this->lastMsg      = Message::getLast(4);
        $this->msgDetail    = Message::msgDetail($this->lastMsg);
        $this->countConfirm = Order::where('status', 'Pembayaran')->count();
    }

    public function userShop()
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        return view('development/frontend/shop/ProductList', $data);
    }

    public function product()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $columnHeader         = array('No', 'Nama Produk', 'Harga Produk', 'Keterangan', 'Action');
        $columnIndex          = array('nama_produk', 'harga_produk', 'keterangan_produk');
        $productList          = Product::where('nama_produk', '<>', '')->get();

        $data['_modal']       = view('backend/product/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $productList, $columnIndex, 'products');

        return view('backend/product/MainProduct', $data);
    }

    public function form($id)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $data['menu_product'] = true;

        if($id != 'new'){
            $dataProduk       = Product::find($id);
            $foto             = $dataProduk->foto_produk;
        }else{
            $dataProduk       = Product::where([
                                    ['username',    '=', session()->get('username')],
                                    ['nama_produk', '=', ''],
                                ])->first();

            if(!$dataProduk){
                $new['username'] = session()->get('username');
                Product::create($new);

                $dataProduk       = Product::where([
                                    ['username',    '=', session()->get('username')],
                                    ['nama_produk', '=', ''],
                                ])->first();
            }

            $foto             = 'default.png';
        }

        $data['id']                = $dataProduk->id;
        $data['nama_produk']       = $dataProduk->nama_produk;
        $data['harga_produk']      = $dataProduk->harga_produk;
        $data['stok']              = $dataProduk->stok;
        $data['keterangan_produk'] = $dataProduk->keterangan_produk;
        $data['foto_produk']       = Testimony::checkPhoto('product', $foto, $id);
        $data['kategori_produk']   = json_decode($dataProduk->kategori_produk);
        $data['_modal']            = view('backend/product/_modalForm');
        $data['categoryList']      = Product_category::all();

        return view('backend/product/FormProduct', $data);
    }

    public function destroy($id)
    {
        $dataProduk = Product::find($id);
        $lokasi     = 'product/' . $id.'/'.$dataProduk->foto_produk;

        $dataProduk->delete();
        Admin::deleteImage($lokasi);

        $flash['info']  = 'Data <strong>'.$dataProduk->nama_produk.'</strong> telah dihapus!';
        $flash['kelas'] = 'warning';
        session($flash);

        return redirect()->back();
    }

    public function store(Request $request)
    {
        $validasi['nama_produk']  = 'required';
        $validasi['harga_produk'] = 'required|numeric';

        $msgError['required']    = 'Tidak boleh kosong!';
        $msgError['numeric']     = 'Inputan harus berupa angka!';

        $this->validate($request,$validasi,$msgError);

        $id                        = $request->id;
        $data['nama_produk']       = $request->nama_produk;
        $data['harga_produk']      = $request->harga_produk;
        $data['keterangan_produk'] = $request->keterangan_produk;
        $data['kategori_produk']   = json_encode($request->kategori_produk);

        if(empty($id)){
            $data['foto_produk']   = 'default.png';
            Product::create($data);
            $flash['info']         = 'Data produk <strong>'.$data['nama_produk'].'</strong> telah disimpan.';
        }else{
            Product::find($id)->update($data);
            $flash['info']         = 'Data produk <strong>'.$data['nama_produk'].'</strong> telah diperbarui.';
        }

        $flash['kelas'] = 'success';
        session($flash);

        return redirect('admin/shop/product.html');
    }

    public function upload(Request $request, $id)
    {
        $time      = Carbon::now();
        $image     = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        $directory = 'product' . '/' . $id;
        $filename  = $id."-".date_format($time,'md').date_format($time,'hi').".".$extension;
        $upload    = $image->storeAs($directory, $filename, 'public');

        if ($upload) {
            $dataProduct = Product::where('id', $id)->first();
            $lokasi      = $directory.'/'.$dataProduct->foto_produk;
            Admin::deleteImage($lokasi);

            $data['foto_produk']   = $filename;
            Product::where('id', $id)->update($data);
            return response()->json($upload, 200);
        }else{
            return response()->json('error', 400);
        }
    }

    public function remove(Request $request)
    {
        Admin::deleteImage($request->lokasi);
    }
}
