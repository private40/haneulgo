<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Admin;
use App\Models\Destination;
use App\Models\Message;
use App\Models\Order;
use App\Models\Package;
use App\Models\Package_detail;
use App\Models\Pengguna;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Testimony;

class DevelopmentController extends Controller
{
    //front end
    private  $testimony    = array();
    private  $photo_testi  = array();
    private  $pesanCount   = '';

    //back end
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    function __construct ()
    {
        //front end
        $this->testimony   = Testimony::getPublication();
        $this->pesanCount  = Message::getUnread(session()->get('username'))->count();

        foreach ($this->testimony as $testimony) {
            $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }

        //backend
        $this->unreadMsg    = Message::where('read_admin', 0)->count();
        $this->lastMsg      = Message::getLast(4);
        $this->msgDetail    = Message::msgDetail($this->lastMsg);
        $this->countConfirm = Order::where('status', 'Pembayaran')->count();
    }

    //frontend
    public function shopProduct(Request $request)
    {
        $data['testimony']     = $this->testimony;
        $data['photo_testi']   = $this->photo_testi;
        $data['pesanCount']    = $this->pesanCount;

        $data['shop_menu']     = true;
        $data['kategori']      = Product_category::all();
        $data['kategoriCount'] = Product_category::getProductCount();
        $productList           = Product::select('*')->orderBy('id', 'desc');

        if($request->search){
            $productList   = $productList->where('nama_produk', 'like', '%'.$request->search.'%')
                                        ->orWhere('keterangan_produk', 'like', '%'.$request->search.'%');
            $data['search'] = $request->search;
            $data['recomend'] = Product::select('*')->paginate(8);
        }

        $data['productCount']  = $productList->count();
        $data['productList']   = $productList->paginate(16);

        if($request->search){
            $data['productList']->withPath(url()->current().'?search='.$request->search);
        }

        $data['productPhoto']  = array();
        foreach ($data['productList'] as $product) {
            $data['productPhoto'][$product->id] = Testimony::checkPhoto('product', $product->foto_produk, $product->id);
        }

        if(isset($data['recomend'])){
            foreach ($data['recomend'] as $product) {
                $data['productPhoto'][$product->id] = Testimony::checkPhoto('product', $product->foto_produk, $product->id);
            }
        }

        return view('development/frontend/product/MainProduct', $data);
    }

    public function shopProductDetail($id)
    {
        $data['testimony']   = $this->testimony;
        $data['photo_testi'] = $this->photo_testi;
        $data['pesanCount']  = $this->pesanCount;

        $data['shop_menu']       = true;
        $data['kategori']        = Product_category::all();
        $data['kategoriCount']   = Product_category::getProductCount();
        $data['product']         = Product::find($id);
        $data['productPhoto']    = array();
        $data['productPhoto'][0] = Testimony::checkPhoto('product', $data['product']->foto_produk, $data['product']->id);
        $data['productCategory'] = Product_category::getCategory($data['product']->kategori_produk);

        return view('development/frontend/product/DetailProduct', $data);
    }

    //backend
    public function adminPackageIndex()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $columnHeader         = array('No', 'Nama Paket', 'Harga Paket', 'Action');
        $columnIndex          = array('nama_paket', 'harga_paket');
        $packageList          = Package::where('tipe', 'Developing')->get();

        $data['_modal']       = view('development/backend/package/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $packageList, $columnIndex, 'package');

        return view('development/backend/package/MainPackage', $data);
    }

    public function adminPackageForm($kode_paket)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $data['menu_package']    = true;
        $data['destinationList'] = Destination::orderBy('nama_destinasi', 'asc')->get();

        if($kode_paket != 'new'){
            $dataPaket               = Package::where('kode_paket', $kode_paket)->first();
            $foto                    = Testimony::checkPhoto('package', $dataPaket->foto_paket, $kode_paket);
            $detailPaket             = Package_detail::where('kode_paket', $kode_paket)->get();

            if($detailPaket){
              foreach ($detailPaket as $detail) {
                $data['day_'.$detail->hari] = json_decode($detail->destination);
              }
            }
        }else{
            $dataPaket      = Package::where([
                                    ['username',   '=', session()->get('username')],
                                    ['nama_paket', '=', ''],
                                    ['tipe',       '=', 'Developing'],
                                ])->first();

            if(!$dataPaket){
                $new['username'] = session()->get('username');
                $new['tipe']     = 'Developing';
                Package::create($new);

                $dataPaket       = Package::where([
                                    ['username',   '=', session()->get('username')],
                                    ['nama_paket', '=', ''],
                                    ['tipe',       '=', 'Developing'],
                                ])->first();

                $foto       = "default.png";
            }
        }

        $data['kode_paket']      = $dataPaket->kode_paket;
        $data['nama_paket']      = $dataPaket->nama_paket;
        $data['harga_paket']     = $dataPaket->harga_paket;
        $data['deskripsi_paket'] = $dataPaket->deskripsi_paket;

        return view('development/backend/package/FormPackage', $data);
    }

    public function adminPackageDestroy(Request $request, $kode_paket)
    {
        $dataPaket = Package::where('kode_paket', $kode_paket)->first();
        $lokasi    = 'package/' . $kode_paket.'/'.$dataPaket->foto_paket;

        Package::where('kode_paket', $kode_paket)->delete();
        Package_detail::where('kode_paket', $kode_paket)->delete();
        Admin::deleteImage($lokasi);

        $flash['info']  = 'Data <strong>'.$dataPaket->nama_paket.'</strong> telah dihapus!';
        $flash['kelas'] = 'warning';
        session($flash);

        return redirect()->back();
    }

    public function adminPackageStore(Request $request)
    {
        $validasi['nama_paket']  = 'required';
        $validasi['harga_paket'] = 'required|numeric';

        $msgError['required']    = 'Tidak boleh kosong!';
        $msgError['numeric']     = 'Inputan harus berupa angka!';

        $this->validate($request,$validasi,$msgError);

        $kode_paket              = $request->kode_paket;
        $data['nama_paket']      = $request->nama_paket;
        $data['deskripsi_paket'] = $request->deskripsi_paket;
        $data['harga_paket']     = $request->harga_paket;
        $data['tipe']            = 'Developing';

        if(empty($kode_paket)){
            $data['foto_paket']  = 'default.jpg';
            Package::create($data);
            $kode_paket          = Package::getKodePaket();
            $flash['info']       = 'Data paket <strong>'.$data['nama_paket'].'</strong> telah disimpan.';
        }else{
            Package::where('kode_paket', $kode_paket)->update($data);
            $flash['info']       = 'Data paket <strong>'.$data['nama_paket'].'</strong> telah diperbarui.';
        }

        for ($i=1; $i < 6; $i++) {
          $detail['kode_paket'] = $kode_paket;
          $detail['hari']       = $i;
          $detail['destination']= json_encode($request->$i);

          $cek = Package_detail::where([ ['kode_paket', '=', $detail['kode_paket']], ['hari', '=', $detail['hari']] ])->count();

          if($cek > 0){
            Package_detail::where([ ['kode_paket', '=',$detail['kode_paket']], ['hari', '=', $detail['hari']] ])->update($detail);
          }else{
            Package_detail::create($detail);
          }
        }

        $flash['kelas']= 'success';
        session($flash);

        return redirect('admin/dev/package.html');
    }

    public function adminPackageUpload(Request $request, $kode_paket)
    {
        $time      = Carbon::now();
        $image     = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        $directory = 'package' . '/' . $kode_paket;
        $filename  = $kode_paket."-".date_format($time,'md').date_format($time,'hi').".".$extension;
        $upload    = $image->storeAs($directory, $filename, 'public');

        if ($upload) {
            $dataPaket = Package::where('kode_paket', $kode_paket)->first();
            $lokasi    = $directory.'/'.$dataPaket->foto_paket;
            Admin::deleteImage($lokasi);

            $data['foto_paket']   = $filename;
            Package::where('kode_paket', $kode_paket)->update($data);
            return response()->json($upload, 200);
        }else{
            return response()->json('error', 400);
        }
    }

    public function adminPackageRemove(Request $request)
    {
        Admin::deleteImage($request->lokasi);
    }
}
