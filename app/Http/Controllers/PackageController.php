<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use App\Models\Admin;
use App\Models\Destination;
use App\Models\Message;
use App\Models\Order;
use App\Models\Package;
use App\Models\Pengguna;
use App\Models\Package_detail;
use App\Models\Testimony;

class PackageController extends Controller
{
    //backend
    private $unreadMsg    = array();
    private $lastMsg      = array();
    private $msgDetail    = array();
    private $countConfirm = array();

    //frontend
    private $testimony    = array();
    private $photo_testi  = array();

    public function __construct()
    {
        //backend
        $this->unreadMsg      = Message::where('read_admin', 0)->count();
        $this->lastMsg        = Message::getLast(4);
        $this->msgDetail      = Message::msgDetail($this->lastMsg);
        $this->countConfirm   = Order::where('status', 'Pembayaran')->count();

        //frontend
        $this->testimony      = Testimony::getPublication();
        foreach ($this->testimony as $testimony) {
          $this->photo_testi[$testimony->id] = Testimony::checkPhoto('pengguna', $testimony->foto, $testimony->username);
        }
    }

    //backend package
    public function index()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $columnHeader         = array('No', 'Nama Paket', 'Harga Paket', 'Action');
        $columnIndex          = array('nama_paket', 'harga_paket');
        $packageList          = Package::where('tipe', 'Package')->get();

        $data['_modal']       = view('backend/package/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $packageList, $columnIndex, 'package');

        return view('backend/package/MainPackage', $data);
    }

    public function form($kode_paket)
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $data['menu_package']    = true;
        $data['destinationList'] = Destination::where('nama_destinasi', '<>', '')->orderBy('nama_destinasi', 'asc')->get();

        if($kode_paket != 'new'){
            $dataPaket               = Package::where('kode_paket', $kode_paket)->first();
            $foto                    = Testimony::checkPhoto('package', $dataPaket->foto_paket, $kode_paket);
            $detailPaket             = Package_detail::where('kode_paket', $kode_paket)->get();

            if($detailPaket){
              foreach ($detailPaket as $detail) {
                $data['day_'.$detail->hari] = json_decode($detail->destination);
              }
            }
        }else{
            $dataPaket      = Package::where([
                                    ['username',   '=', session()->get('username')],
                                    ['nama_paket', '=', ''],
                                    ['tipe',       '=', 'Developing'],
                                ])->first();

            if(!$dataPaket){
                $new['username'] = session()->get('username');
                $new['tipe']     = 'Developing';
                Package::create($new);

                $dataPaket       = Package::where([
                                    ['username',   '=', session()->get('username')],
                                    ['nama_paket', '=', ''],
                                    ['tipe',       '=', 'Developing'],
                                ])->first();

                $foto            = asset('images/package/default.png');
            }else{
                $foto            = Testimony::checkPhoto('package', $dataPaket->foto_paket, $kode_paket);
            }
        }

        $data['kode_paket']      = $dataPaket->kode_paket;
        $data['nama_paket']      = $dataPaket->nama_paket;
        $data['harga_paket']     = $dataPaket->harga_paket;
        $data['deskripsi_paket'] = $dataPaket->deskripsi_paket;
        $data['foto_paket']      = $foto;

        return view('backend/package/FormPackage', $data);
    }

    public function destroy(Request $request, $kode_paket)
    {
        $dataPaket    = Package::where('kode_paket', $kode_paket)->first();
        $lokasi       = 'package/' . $kode_paket.'/'.$dataPaket->foto_paket;
        $lokasi_thumb = 'package/' . $kode_paket.'/thumb-'.$dataPaket->foto_paket;

        Package::where('kode_paket', $kode_paket)->delete();
        Package_detail::where('kode_paket', $kode_paket)->delete();
        Admin::deleteImage($lokasi);
        Admin::deleteImage($lokasi_thumb);

        $flash['info']  = 'Data <strong>'.$dataPaket->nama_paket.'</strong> telah dihapus!';
        $flash['kelas'] = 'warning';
        session($flash);

        return redirect()->back();
    }

    public function packageStore(Request $request)
    {
        $validasi['nama_paket']  = 'required';
        $validasi['harga_paket'] = 'required|numeric';

        $msgError['required']    = 'Tidak boleh kosong!';
        $msgError['numeric']     = 'Inputan harus berupa angka!';

        $this->validate($request,$validasi,$msgError);

        $kode_paket              = $request->kode_paket;
        $data['nama_paket']      = $request->nama_paket;
        $data['deskripsi_paket'] = $request->deskripsi_paket;
        $data['harga_paket']     = $request->harga_paket;
        $data['tipe']            = 'Package';

        if(empty($kode_paket)){
            $data['foto_paket']  = 'default.png';
            Package::create($data);
            $kode_paket          = Package::getKodePaket();
            $flash['info']       = 'Data paket <strong>'.$data['nama_paket'].'</strong> telah disimpan.';
        }else{
            Package::where('kode_paket', $kode_paket)->update($data);
            $flash['info']       = 'Data paket <strong>'.$data['nama_paket'].'</strong> telah disimpan.';
        }

        for ($i=1; $i < 6; $i++) {
          $detail['kode_paket'] = $kode_paket;
          $detail['hari']       = $i;
          $detail['destination']= json_encode($request->$i);

          $cek = Package_detail::where([ ['kode_paket', '=', $detail['kode_paket']], ['hari', '=', $detail['hari']] ])->count();

          if($cek > 0){
            Package_detail::where([ ['kode_paket', '=',$detail['kode_paket']], ['hari', '=', $detail['hari']] ])->update($detail);
          }else{
            Package_detail::create($detail);
          }
        }

        $flash['kelas']= 'success';
        session($flash);

        return redirect('admin/package.html');
    }

    public function upload(Request $request, $kode_paket)
    {
        $time      = Carbon::now();
        $image     = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        $directory = 'package' . '/' . $kode_paket;
        $filename  = $kode_paket."-".date_format($time,'md').date_format($time,'hi').".".$extension;
        $upload    = $image->storeAs($directory, $filename, 'public');

        if ($upload) {
            $dataPaket = Package::where('kode_paket', $kode_paket)->first();
            $lokasi    = $directory.'/'.$dataPaket->foto_paket;
            Admin::deleteImage($lokasi);

            $lokasi    = $directory.'/thumb-'.$dataPaket->foto_paket;
            Admin::deleteImage($lokasi);

            $data['foto_paket']   = $filename;
            Package::where('kode_paket', $kode_paket)->update($data);
            return response()->json($upload, 200);
        }else{
            return response()->json('error', 400);
        }
    }

    public function remove(Request $request)
    {
        Admin::deleteImage($request->lokasi);
    }

    //backend feature
    public function feature()
    {
        $data['unreadMsg']    = $this->unreadMsg;
        $data['lastMsg']      = $this->lastMsg;
        $data['msgDetail']    = $this->msgDetail;
        $data['countConfirm'] = $this->countConfirm;

        $columnHeader         = array('No', 'Nama Fitur Paket', 'Harga Fitur Paket', 'Action');
        $columnIndex          = array('nama_paket', 'harga_paket');
        $packageList          = Package::where('tipe', 'Feature')->get();

        $data['_modal']       = view('backend/package/_modal');
        $data['_table']       = Admin::createTable($columnHeader, $packageList, $columnIndex, 'feature');
        $data['tipe']         = 'Fitur';

        return view('backend/package/MainPackage', $data);
    }

    public function featureStore(Request $request)
    {
        $validasi['nama_paket']  = 'required';
        $validasi['harga_paket'] = 'required|numeric';

        $msgError['required']    = 'Tidak boleh kosong!';
        $msgError['numeric']     = 'Inputan harus berupa angka!';

        $this->validate($request,$validasi,$msgError);

        $kode_paket              = $request->kode_paket;
        $data['nama_paket']      = $request->nama_paket;
        $data['harga_paket']     = $request->harga_paket;
        $data['tipe']            = 'Feature';

        if(empty($kode_paket)){
            $data['foto_paket']  = 'default.jpg';
            Package::create($data);
            $flash['info']       = 'Data fitur paket <strong>'.$data['nama_paket'].'</strong> telah disimpan.';
        }else{
            Package::where('kode_paket', $kode_paket)->update($data);
            $flash['info']       = 'Data fitur paket <strong>'.$data['nama_paket'].'</strong> telah diperbarui.';
        }

        $flash['kelas']= 'success';
        session($flash);

        return redirect()->back();
    }

    public function featureDetail(Request $request)
    {
        $kode_paket          = $request->kode_paket;
        $dataPaket           = Package::where('kode_paket', $kode_paket)->first();
        $data['nama_paket']  = $dataPaket->nama_paket;
        $data['harga_paket'] = $dataPaket->harga_paket;

        echo json_encode($data);
    }

    //frontend package
    public function show($kode_paket)
    {
        $dataPaket               = Package::where('kode_paket', $kode_paket)->first();
        $data['pesanCount']      = Message::getUnread(session()->get('username'))->count();
        $data['testimony']       = $this->testimony;
        $data['photo_testi']     = $this->photo_testi;
        $data['packages']        = true;

        if(!$dataPaket){
            $data['title']       = "Paket Travel";
            $data['description'] = "Paket travel yang Anda cari tidak ditemukan!";
            $data['textBack']    = "Paket travel yang Anda cari tidak ditemukan! <br><a href='".url('packages.html')."' > Klik disini untuk melihat daftar paket</a>";

            return view('frontend/main/NotFound', $data);
        }

        $notes['syarat_ketentuan'] = Admin::getInfoPage('syarat_dan_ketentuan');
        $notes['hubungi_kami']     = Admin::getInfoPage('hubungi_kami');
        $data['_notes']            = view('frontend/main/NotesTravel', $notes);

        $detailPaket             = Package_detail::where('kode_paket', $kode_paket)->get();
        $data['kode_paket']      = $kode_paket;
        $data['nama_paket']      = $dataPaket->nama_paket;
        $data['deskripsi_paket'] = $dataPaket->deskripsi_paket;
        $data['harga_paket']     = 'Rp. '.number_format($dataPaket->harga_paket,0,'','.').',-';
        $data['harga']           = $dataPaket->harga_paket;
        $data['total']           = number_format($dataPaket->harga_paket,0,'','.');
        $data['foto']            = Package::photoPackage($dataPaket->foto_paket, $kode_paket);
        $data['pengguna']        = Pengguna::where('username', session()->get('username'))->first();
        $data['featureList']     = Package::where('tipe', 'feature')->get();

        foreach ($detailPaket as $detail) {
            $destinasi           = json_decode($detail->destination);
            $data['day_'.$detail->hari] = '';
            $urut                = 1;

            if(is_array($destinasi)){
                foreach($destinasi as $destination){
                    $dataDestinasi   = Destination::find($destination);

                    if($dataDestinasi){
                        if($urut == count($destinasi)){
                            $data['day_'.$detail->hari] .= $dataDestinasi->nama_destinasi;
                        }else{
                            $data['day_'.$detail->hari] .= $dataDestinasi->nama_destinasi.", ";
                        }
                    }

                    $urut++;
                }
            }else{
                $data['day_'.$detail->hari] = $destinasi;
            }
        }

        return view('frontend/package/DetailPackage', $data);
    }
}
