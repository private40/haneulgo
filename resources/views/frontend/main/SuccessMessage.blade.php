@extends('frontend/main/HeadLayout')

@section('content')
<section id="coming-soon">
    <div class="container">
       <div class="row">
           <div class="col-sm-8 col-sm-offset-2">
               <div class="text-center coming-content">
                   <h1>Account Verified</h1>
                   <p>Akun Anda berhasil diverifikasi dengan username <strong>{{ $username }}</strong>. Silahkan <a href="#" data-toggle="modal" data-target="#login">login</a> untuk masuk ke akun Anda. </p>
                   <div class="social-link">
                       <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                       <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                       <span><a href="#"><i class="fa fa-google-plus"></i></a></span>
                   </div>
               </div>
           </div>
       </div>
   </div>
</section>
@endsection
