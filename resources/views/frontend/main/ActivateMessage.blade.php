@extends('frontend/main/HeadLayout')

@section('content')
<section id="coming-soon">
    <div class="container">
       <div class="row">
           <div class="col-sm-8 col-sm-offset-2">
               <div class="text-center coming-content">
                   <h1>CONGRATULATIONS</h1>
                   <p>Selamat! Anda berhasil menjadi member Haneul Go! Email verifikasi telah dikirimkan ke email Anda. Aktifkan akun Anda dengan menekan tombol <strong>aktifasi akun pada email Anda</strong>.</p>
                   <p>Belum mendapatkan email? <a href="{{ url('resend/verification') }}">Kirim ulang verifikasi email.</a></p>
                   <div class="social-link">
                       <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                       <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                       <span><a href="#"><i class="fa fa-google-plus"></i></a></span>
                       <span><a href="https://www.instagram.com/koreahaneulgo/" target="_blank"><i class="fa fa-instagram"></i></a></span>
                   </div>
               </div>
           </div>
       </div>
   </div>
</section>
@endsection
