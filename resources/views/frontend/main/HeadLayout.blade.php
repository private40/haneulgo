<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Agen Travel tour Korea Selatan dengan paket yang murah tetapi mengutamakan kenyamanan Anda">
    <meta name="author"      content="">
    <meta name="csrf-token"  content="{{ csrf_token() }}">
    <meta name="default"     content="{{ url('/') }}">
    <meta name="keywords"    content="korea,korea selatan,tour ke korea,backpacker korea,seoul korea selatan,travel murah,wisata korea selatan,tempat wisata di korea selatan,ongkos pulang pergi ke korea,tiket ke korea" />
    <title>{{ (isset($title) ? $title : '') }}Haneul Go</title>
    <link href="{{ asset('triangle/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('triangle/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('triangle/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('triangle/css/lightbox.css') }}" rel="stylesheet">
  	<link href="{{ asset('triangle/css/main.css') }}" rel="stylesheet">
   	<link href="{{ asset('triangle/css/responsive.css') }}" rel="stylesheet">
   	<link href="{{ asset('css/custom_validator.css') }}" rel="stylesheet">

    <link href="{{ asset('gentellela/js/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('gentellela/js/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('gentellela/js/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('gentellela/js/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('gentellela/js/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dropzone/dist/dropzone.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('triangle/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/haneul-front-1.0.5.js') }}"></script>
    <script src="{{ asset('dropzone/dist/dropzone.js') }}"></script>

    <!-- <link rel="stylesheet" href="{{ asset('dist/css/ion-icon.css') }}"> -->

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('images/main/logo.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('triangle/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('triangle/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('triangle/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('triangle/images/ico/apple-touch-icon-57-precomposed.png') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67683979-6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-67683979-6');
    </script>
</head><!--/head-->

<body>
	<header id="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.instagram.com/koreahaneulgo/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="{{ url('/') }}">
                    	<h1><img src="{{ asset('images/main/haneul.png') }}" alt="Haneul Go Logo" width="30%"></h1>
                    </a>

                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="{{ (isset($packages) || isset($register) || isset($menu_pengguna) || isset($gallery) ? '' : 'active') }}"><a href="{{ url('/') }}">Home</a></li>
                        <li class="{{ (isset($packages) ? 'active' : '') }}"><a href="{{ url('packages.html') }}">Paket Travel</a></li>
                        <li class="{{ (isset($shop)  ? 'active' : '') }}"><a href="{{ url('shop.html') }}">Haneul Shop</a></li>
                        <li class="{{ (isset($money)  ? 'active' : '') }}"><a href="{{ url('money.html') }}">Haneul Money</a></li>
                        <li class="{{ (isset($gallery)  ? 'active' : '') }}"><a href="{{ url('gallery.html') }}">Galeri</a></li>
                        @if(session()->has('username'))
                        <li class="{{ (isset($menu_pengguna)  ? 'active' : '') }} dropdown"><a href="#">{{ session()->get('username') }} &nbsp;&nbsp;<span class="label label-info">{{ ($pesanCount > 0 ? $pesanCount : '') }}</span> <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="{{ url('pengguna/'.session()->get('username')) }}">Halaman Akun</a></li>
                                <li><a href="{{ url('signout') }}">Keluar</a></li>
                            </ul>
                        </li>
                        @else
                        <li class="{{ (isset($register) ? 'active' : '' ) }}"><a href="#" data-toggle="modal" data-target="#login">Masuk / Daftar</a></li>

                        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">

                              <div class="modal-body">
                                <div class="contact-form bottom">
                                  {!! Form::open(['action' => 'PenggunaController@login', 'method' => 'post', 'id' => 'formLogin']) !!}
                                        <h2>Login ke Akun Haneul Go</h2>
                                        <div class="alert alert-warning hidden" id="div_login">
                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                             <span id="error_login"><strong>Perhatian!</strong></span>.
                                        </div>
                                        <div class="form-group">
                                            {!! Form::text('username', '', ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'Email / Username', 'required' => 'true']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'true', 'id' => 'password']) !!}
                                        </div>
                                        <div class="row">
                                          <div class="col-sm-8">
                                            <button type="submit" name="login" class="btn btn-submit">Login</button>
                                          </div>
                                          <div class="col-sm-4">
                                            Belum punya akun?<br>
                                            <a href="{{ url('register.html') }}">Daftar</a>
                                          </div>
                                        </div>
                                  {!! Form::close() !!}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endif
                    </ul>
                </div>

                <!-- <div class="search">
                    <form role="form">
                        <i class="fa fa-search"></i>
                        <div class="field-toggle">
                            <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                        </div>
                    </form>
                </div> -->
            </div>
        </div>
    </header>

    @yield('content')

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center bottom-separator">
                    <img src="{{ asset('triangle/images/home/under.png') }}" class="img-responsive inline" alt="">
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="contact-form bottom">
                        <h2>Partner Pembayaran</h2>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4">
                                <img src="{{ asset('images/rekening/BCA.jpg') }}" alt="BCA-Logo" width="100%"/>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <img src="{{ asset('images/rekening/BNI.jpg') }}" alt="BNI-Logo" width="100%"/>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <img src="{{ asset('images/rekening/Mandiri.jpg') }}" alt="Mandiri-Logo" width="100%"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="contact-info bottom">
                        <h2>Tentang Haneul Go</h2>
                        <h4><a href="{{ url('main/cara_pesan.html') }}">Cara Pesan</a></h4>
                        <h4><a href="{{ url('main/syarat_dan_ketentuan.html') }}">Syarat & Ketentuan</a></h4>
                        <h4><a href="{{ url('main/hubungi_kami.html') }}">Hubungi Kami</a></h4>
                        <!-- <h4><a href="{{ url('main/pusat_bantuan.html') }}">Pusat Bantuan</a></h4> -->
                        <h4><a href="{{ url('main/kebijakan_privasi.html') }}">Kebijakan Privasi</a></h4>
                        <h4><a href="{{ url('main/about.html') }}">Tentang Kami</a></h4>
                    </div>
                </div>

                <div class="col-md-5 col-sm-6">
                    <div class="testimonial bottom">
                        <h2>Kirim Pesan</h2>
                        <div class="row"><div class="col-xs-12">
                        {!! Form::open(['action' => 'MessageController@sendMessage', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea name="pesan" class="resizable_textarea form-control" placeholder="Ketikkan pesan" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                          </div>
                        </div>
                        <div class="compose-btn pull-right">
                          {!! Form::submit('Kirim', ['class' => "btn btn-submit", 'id' => 'btnSimpan']) !!}
                        </div>
                        {!! Form::close() !!}
                        </div></div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Haneul Go Travel 2018. All Rights Reserved.</p>
                        <!-- <p>Crafted by <a target="_blank" href="http://designscrazed.org/">Allie</a></p> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <label id="error" style="color: Red; display: none; margin-bottom: 10px">* Input number</label>

    <script type="text/javascript" src="{{ asset('triangle/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('triangle/js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('triangle/js/lightbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('triangle/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('triangle/js/main.js') }}"></script>
    <script src="{{ asset('gentellela/build/validator/validator.js') }}"></script>
    <script src="{{ asset('js/custom_validation.js') }}"></script>

    <script src="{{ asset('gentellela/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/responsive.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentellela/js/datatables/dataTables.scroller.min.js') }}"></script>
    </body>
    </html>
