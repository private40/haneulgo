@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">{{ $page['title'] }} Haneul Go</h1>
                        <p>{{ $page['description'] }}</p>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <img src="{{ asset('images/main/logo_ver.png') }}" class="margin-bottom" alt="">
                <h1 class="margin-bottom">{{ $page['title'] }} Haneul Go</h1>
            </div>
            <div class="col-sm-6 col-sm-offset-3 text-center">
                {!! $page['content'] !!}
                <br>
            </div>
        </div>
    </div>
</section>
@endsection
