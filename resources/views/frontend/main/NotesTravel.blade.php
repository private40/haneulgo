<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
              <div id="accordion-container">
                  <h2 class="page-header">Syarat & Ketentuan</h2>
                  <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                      Ketentuan Paket
                                  </a>
                              </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <p>Biaya travel sudah termasuk : </p>
                                <ul class="elements">
                                    <li class="wow fadeInUp" data-wow-duration="900ms" data-wow-delay="100ms"><i class="fa fa-angle-right"></i> Biaya guide.</li>
                                    <li class="wow fadeInUp" data-wow-duration="800ms" data-wow-delay="200ms"><i class="fa fa-angle-right"></i> Biaya penginapan.</li>
                                    <li class="wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms"><i class="fa fa-angle-right"></i> Biaya internet - wifi portable.</li>
                                    <li class="wow fadeInUp" data-wow-duration="600ms" data-wow-delay="400ms"><i class="fa fa-angle-right"></i> Biaya transportasi. (Day 1 - Day 3 dan Day 5)</li>
                                    <li class="wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms"><i class="fa fa-angle-right"></i> Biaya konsumsi. (Day 1 - Day 3 dan Day 5)</li>
                                    <li class="wow fadeInUp" data-wow-duration="500ms" data-wow-delay="600ms"><i class="fa fa-angle-right"></i> Biaya tiket masuk destinasi. (Day 1 - Day 3 dan Day 5)</li>
                                </ul>
                                <br>
                                <p style="color:red;">Biaya travel tidak termasuk : </p>
                                <ul class="elements">
                                    <li class="wow fadeInUp" data-wow-duration="500ms" data-wow-delay="700ms"><i class="fa fa-angle-right"></i> Tiket pesawat. (Apabila membutuhkan layanan pemesanan tiket pesawat, harap hubungi Customer Service)</li>
                                    <li class="wow fadeInUp" data-wow-duration="500ms" data-wow-delay="800ms"><i class="fa fa-angle-right"></i> Pengurusan VISA. (Optional)</li>
                                </ul>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                      Syarat dan Ketentuan
                                  </a>
                              </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                              <div class="panel-body">
                                <ul class="elements">
                                  {!! $syarat_ketentuan['content'] !!}
                                    <!-- <li ><i class="fa fa-angle-right"></i> Peserta trip diharuskan dalam keadaan fit dan tidak memiliki riwayat penyakit parah yang bisa mengakibatkan terjadinya hal yang tidak diinginkan selama trip berlangsung.
                                      Apabila terjadi hal sedemikian rupa, seluruh resiko bukan tanggung jawab Haneul Go.</li>
                                    <li><i class="fa fa-angle-right"></i> Jadwal dan itenerary dari kami sudah termasuk dalam biaya trip. Apabila dalam kasus peserta trip sepakat untuk mengganti jadwal,
                                      maka dapat dikenai biaya tambahan, atau apabila terjadi keterlambatan waktu karena faktor dari peserta, maka resiko bukan tanggung jawab Haneul Go.</li>
                                    <li><i class="fa fa-angle-right"></i> Seluruh biaya yang sudah ditetapkan berlaku final untuk masing masing paket.</li>
                                    <li><i class="fa fa-angle-right"></i> Apabila menginginkan perubahan jadwal atau tujuan destinasi, bisa menghubungi Customer Service pada nomor yang tertera. (Syarat dan Kondisi Berlaku)</li>
                                    <li><i class="fa fa-angle-right"></i> Selama trip, peserta diharuskan memenuhi SOP yang kami berikan selama trip.</li>
                                    <li><i class="fa fa-angle-right"></i> Diharapkan untuk lebih dulu konsultasi kepada Customer Service sebelum melakukan booking trip.</li> -->
                                </ul>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                      Customer Service
                                  </a>
                              </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                {!! $hubungi_kami['content'] !!}
                                <!-- <ul class="elements">
                                  <li><i class="fa fa-angle-right"></i> WA : +82 10-2417-9654 / +82 10-2540-0591</li>
                                  <li><i class="fa fa-angle-right"></i> Instagram : @koreahaneulgo</li>
                                </ul> -->
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</section>
