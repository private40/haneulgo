@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
     <div class="vertical-center sun">
          <div class="container">
             <div class="row">
                 <div class="action">
                     <div class="col-sm-12">
                         <h1 class="title">Daftar Paket Travel</h1>
                         <p>Pilih sesuai kebutuhanmu.</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
</section>

<section class="container" style="margin-top: 40px">
     <div class="price-table2">
         <div class="row">
             <div class="col-md-3">
             </div>
             @foreach($packageList as $list)
             <div class="col-sm-6 col-md-3">
                 <div class="single-table {{ $stylePackage[$list->kode_paket] }}">
                     <div class="table-header">
                         <h2>{{ $list->nama_paket }}</h2>
                         <p class="plan-price"><span class="dollar-sign">Rp. </span><span class="price">{{ number_format($list->harga_paket,0,'','.') }},-</span><span class="month">min 3 pax</span></p>
                     </div>
                      <ul>
                         <li>Guide <span><i class="fa fa-check"></i></span></li>
                         <li>Transportasi <span><i class="fa fa-check"></i></span></li>
                         <li>Konsumsi <span><i class="fa fa-check"></i></span></li>
                         <li>Internet <span><i class="fa fa-check"></i></span></li>
                         <li>Tiket Destinasi <span><i class="fa fa-check"></i></span></li>
                         <li>Tiket Pesawat <span><i class="fa fa-times"></i></span></li>
                     </ul>
                     <span class="btn-signup"><a href="{{ url('packages_detail/'.$list->kode_paket) }}">Lihat</a></span>
                 </div>
             </div>
             @endforeach
         </div>
     </div><!--/#price-table2-->
 </section>
@endsection
