@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">{{ $nama_paket }}</h1>
                        <p>{{ $deskripsi_paket." ".$harga_paket }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="company-information" class="choose">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                <img src="{{ $foto[0] }}" alt="{{ $nama_paket }}" width="100%">
            </div>
            <div class="col-sm-6 padding-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                <h2>{{ $nama_paket }}</h2>
                <p>Daftar Destinasi</p>
                <ul class="elements">
                    <li class="wow fadeInUp" data-wow-duration="900ms" data-wow-delay="100ms"><i class="fa fa-angle-right"></i> <strong>Day 1</strong> <br> {{ $day_1 }}</li>
                    <li class="wow fadeInUp" data-wow-duration="800ms" data-wow-delay="200ms"><i class="fa fa-angle-right"></i> <strong>Day 2</strong> <br> {{ $day_2 }}</li>
                    <li class="wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms"><i class="fa fa-angle-right"></i> <strong>Day 3</strong> <br> {{ $day_3 }}</li>
                    <li class="wow fadeInUp" data-wow-duration="600ms" data-wow-delay="400ms"><i class="fa fa-angle-right"></i> <strong>Day 4</strong> <br> {{ $day_4 }}</li>
                    <li class="wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms"><i class="fa fa-angle-right"></i> <strong>Day 5</strong> <br> {{ $day_5 }}</li>
                </ul>
                <div class="row padding-top">
                    <div class="col-sm-9">
                        <h2>{{ $harga_paket }} <small>/orang</small></h2>
                    </div>

                    <div class="col-sm-3 pull-right">
                        <!-- <button class="btn btn-common uppercase" data-toggle="modal" data-target="#pesan">Pesan</button> -->
                        <a href="{{ url('order_travel/'.$kode_paket) }}" class="btn btn-common uppercase">Pesan</a>
                    </div>
                </div>
                <p class="padding-top">* Anda diharapkan membaca syarat dan ketentuan travel.</p>
            </div>
        </div>
    </div>
</section>

{!! $_notes !!}
@endsection
