@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
     <div class="vertical-center sun">
          <div class="container">
             <div class="row">
                 <div class="action">
                     <div class="col-sm-12">
                         <h1 class="title">Reset Password</h1>
                         <p>Masukkan email Anda untuk mereset password.</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
</section>
 <!--/#action-->

 <section class="container">
   <div class="row">
     <div class="col-md-6 col-sm-12 col-md-offset-4">
         <div class="contact-form bottom">
             <h2>&nbsp;</h2>
             {!! Form::open(['action' => 'PenggunaController@forgetPassword', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="item form-group">
                     <div class="col-sm-8">
                         <input type="email" name="email" class="form-control" required="required" placeholder="Email" id="email">
                         <label for="" class="hidden" id="error_email"> <span class="info" id="info_email">Email harus diisi. </span> </label>
                     </div>
                 </div>
                  <div class="form-group">
                     <div class="col-sm-8">
                         <button type="submit" class="btn btn-submit">Kirim</button>
                    </div>
                 </div>
                  <div class="form-group">
                     <div class="col-sm-8">
                   <center><label for="">Belum punya akun? <a href="{{ url('register.html') }}">Silahkan daftar.</a> </label></center>
                 </div>
              {!! Form::close() !!}
             <!-- </form> -->
         </div>
     </div>
   </div>
 </section>
@endsection
