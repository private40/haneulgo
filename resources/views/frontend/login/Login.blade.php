@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
     <div class="vertical-center sun">
          <div class="container">
             <div class="row">
                 <div class="action">
                     <div class="col-sm-12">
                         <h1 class="title">Masuk sebagai member Haneul!</h1>
                     </div>
                 </div>
             </div>
         </div>
     </div>
</section>
 <!--/#action-->

 <section class="container" style="margin-top: 20px">;
   <div class="row">
     <div class="col-md-4 col-sm-12 col-md-offset-4">
         <div class="alert alert-warning hidden" id="page_login">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
             <span id="errorpage_login"><strong>Perhatian!</strong></span>.
         </div>
         @if(session()->has('info'))
             <div class="alert alert-warning alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                 {!! session()->get('info') !!}
              </div>
         @endif
         <div class="contact-form bottom">
             <h2>&nbsp;</h2>
             {!! Form::open(['action' => 'PenggunaController@login', 'method' => 'post', 'id' => 'formLogin']) !!}
                 <div class="form-group">
                     {!! Form::text('username', (session()->has('info_user') ? session()->get('info_user') : ''), ['class' => 'form-control', 'id' => 'user', 'placeholder' => 'Email / Username', 'required' => 'true', 'autofocus' => 'autofocus']) !!}
                 </div>
                 <div class="form-group">
                     {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'true', 'id' => 'pass']) !!}
                 </div>
                 <div class="form-group">
                     <button type="submit" name="login" class="btn btn-submit" >Login</button>
                 </div>
                 <div class="form-group">
                     <a href="{{ url('login/google') }}" class="btn btn-danger" style="width: 100%" ><i class="fa fa-google-plus"></i> | Sign in with Google+</a>
                 </div>
                 <div class="form-group">
                   <center><label for="">Belum punya akun? <a href="{{ url('register.html') }}">Silahkan daftar.</a> </label></center>
                 </div>
            {!! Form::close() !!}
            @if(session()->has('info'))
                <script type="text/javascript">
                $(document).ready(function(){
                  destroy();
                });
                </script>
            @endif
             <!-- </form> -->
         </div>
     </div>
   </div>
 </section>
@endsection
