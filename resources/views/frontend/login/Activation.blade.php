<html lang=id>
    <head>
        <meta content="date=no" name="format-detection">
        <meta content="email=no" name="format-detection">
        <style>
            a {
                color: #FFFFFF !important; 
                text-decoration: none;
            }
            .abml a {
                color: #000000; 
                font-family: Roboto-Medium,Helvetica,Arial,sans-serif; 
                font-weight: bold; 
                text-decoration: none;
            }
            .adgl a {
                color: rgba(0, 0, 0, 0.87); 
                text-decoration: none;
            }
            .afal a {
                color: #b0b0b0; 
                text-decoration: none;
            }

            @media screen and (min-width: 600px) 
            {
                .v2sp {padding: 6px 30px 0px;} 
                .v2rsp {padding: 0px 10px;}
            }
        </style>
    </head>
    <body bgcolor="#FFFFFF" style="margin: 0; padding: 0;">
        <table border=0 cellpadding=0 cellspacing=0 height="100%" style="min-width: 348px;" width="100%">
            <Tbody>
                <tr height=32px></tr>
                <tr align=center><td>
                    <div itemscope>
                        <div itemprop=action itemscope>
                            <link href="{{ url('activation/'.$kode_unik) }}" itemprop=url>
                            <meta content="Aktivasi Akun" itemprop=name>
                        </div>
                    </div>
                    <table border=0 cellpadding=0 cellspacing=0 style="padding-bottom: 20px; max-width: 600px; min-width: 220px;">
                        <Tbody>
                            <tr>
                                <td>
                                    <table cellpadding=0 cellspacing=0>
                                        <Tbody>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <table border=0 cellpadding=0 cellspacing=0 style="direction: ltr; padding-bottom: 7px;" width="100%">
                                                        <Tbody>
                                                            <tr>
                                                                <td align=left>
                                                                    <img src="https://haneul-go.com/images/main/haneul.png" style="width: 122px;" width=122px>
                                                                </td>
                                                                <td align=right style="font-family: Roboto-Light,Helvetica,Arial,sans-serif;">
                                                                {{ $nama_lengkap }}
                                                                </td>
                                                                <td align=right width=35>
                                                                    <img height=28 src="https://haneul-go.com/images/pengguna/default.png" style="width: 28px; height: 28px; border-radius: 50%;;" width=28>
                                                                </td>
                                                            </tr>
                                                        </Tbody>
                                                    </table>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td height=5 style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-corner-nw.png&#39;) top left no-repeat;" width=6>
                                                    <div></div>
                                                </td>
                                                <td height=5 style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-pixel-n.png&#39;) top center repeat-x;">
                                                    <div></div>
                                                </td>
                                                <td height=5 style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-corner-ne.png&#39;) top right no-repeat;" width=6>
                                                    <div></div>
                                                </td>
                                            </tr>
                                            <tr style=" border-color: #37C4CD">
                                                <td style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-pixel-w.png&#39;) center left repeat-y; border-color: #37C4CD" width=6>
                                                    <div></div>
                                                </td>
                                                <td style="border-color: #37C4CD">
                                                    <div style="font-family: Roboto-Regular,Helvetica,Arial,sans-serif;padding-left: 20px; padding-right: 20px; color: #f4f4f4; font-size: 24px;padding-bottom: 38px;padding-top: 40px;text-align: center; word-break: break-word; background-color: #37c4cd">
                                                        <div class=v2sp style="border-color: #37C4CD">
                                                            Selamat, {{ $nama_lengkap }}! Anda berhasil mendaftar menjadi member Haneul Go dengan email<br><a style="font-family: Roboto-Regular,Helvetica,Arial,sans-serif;color: #f4f4f4; font-size: 16px; line-height: 1.8;">{{ $email }}</a>
                                                        </div>
                                                    </div>
                                                    <div style="font-family: Roboto-Regular,Helvetica,Arial,sans-serif; font-size: 13px; color: #f4f4f4; line-height: 1.6;padding-left: 20px; padding-right: 20px;padding-bottom: 32px; padding-top: 24px; background-color: #37c4cd">
                                                        <div class=v2sp>
                                                            <center>Klik tombol dibawah ini untuk menverifikasi akun Anda.
                                                            <div style="padding-top: 24px; text-align: center;">
                                                                <center>
                                                                <a href="{{ url('activation/'.$kode_unik.'?email='.$email) }}" target="_blank">
                                                                    <table border=0 cellpadding=0 cellspacing=0 style="background-color: #4184F3; border-radius: 2px; min-width: 90px;">
                                                                        <tbody>
                                                                            <tr style="height: 6px;"></tr>
                                                                            <tr>
                                                                                <td style="padding-left: 8px; padding-right: 8px; text-align: center; margin: 0 40%">
                                                                                    <center>
                                                                                        <a href="{{ url('activation/'.$kode_unik.'?email='.$email) }}" target="_blank" style="color: #FFFFF">
                                                                                        Verifikasi Akun
                                                                                        </a>
                                                                                    </center>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="height: 6px;">
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </a>
                                                                </center>
                                                            </div>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-pixel-e.png&#39;) center left repeat-y;" width=6>
                                                    <div></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height=5 style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-corner-sw.png&#39;) top left no-repeat;" width=6>
                                                    <div></div>
                                                </td>
                                                <td height=5 style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-pixel-s.png&#39;) top center repeat-x">
                                                    <div></div>
                                                </td>
                                                <td height=5 style="background:url(&#39;https://www.gstatic.com/accountalerts/email/hodor/4-corner-se.png&#39;) top left no-repeat;" width=6>
                                                    <div></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <div style="text-align: left;">
                                                        <div style="font-family: Roboto-Regular,Helvetica,Arial,sans-serif;color: rgba(0,0,0,0.54); font-size: 12px; line-height: 20px; padding-top: 10px;">
                                                            <div>
                                                                Anda menerima email ini sebagai pemberitahuan tentang verifikasi pengaktifan Akun Member Haneul Go.
                                                            </div>
                                                            <div style="direction: ltr;">
                                                                &copy; 2018 Haneul Go - Incheon, South Korea
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </Tbody>
                                    </table>
                                </td>
                            </tr>
                        </Tbody>
                    </table>
                </td>
            </tr>
            <tr height=32px></tr>
        </Tbody>
    </table>
</body>
</html>