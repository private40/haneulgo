@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
     <div class="vertical-center sun">
          <div class="container">
             <div class="row">
                 <div class="action">
                     <div class="col-sm-12">
                         <h1 class="title">Gabung jadi member Haneul!</h1>
                         <p>Data Anda dilindungi dan tidak akan disebarluaskan.</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
</section>
 <!--/#action-->

 <section class="container">
   <div class="row">
     <div class="col-md-6 col-sm-12 col-md-offset-4">
         <div class="contact-form bottom">
            <div class="alert alert-danger {{ (count($errors) > 0 ? '': 'hidden') }}">
              Ada kesalahan dalam peng-inputan data! Silahkan cek kembali inputan Anda. <br>
            </div>
             <h2>&nbsp;</h2>
             {!! Form::open(['action' => 'PenggunaController@register', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                 <div class="item form-group">
                     <div class="col-sm-8">
                        <input type="text" name="nama_lengkap" class="form-control" required="required" placeholder="Nama Lengkap" id="nama_lengkap" autofocus>
                        <label for="" class="hidden" id="error_nama_lengkap"> <span class="info">Nama Lengkap harus diisi.</span></label>
                     </div>
                 </div>
                 <div class="item form-group">
                     <div class="col-sm-8">
                         <input type="email" name="email" class="form-control" required="required" placeholder="Email" id="email">
                         <label for="" class="hidden" id="error_email"> <span class="info" id="info_email">Email harus diisi. </span> <i class="ion ion-help-circled" data-toggle="tooltip" title="Email akan digunakan untuk konfirmasi akun."></i></label>
                     </div>
                 </div>
                 <div class="item form-group">
                     <div class="col-sm-8">
                         <input type="text" name="username" class="form-control" required="required" placeholder="Username" id="user" onkeypress="return pengguna(event)">
                         <label id="error" class="info hidden">Karakter tersebut tidak diperkenankan</label>
                         <label class="" id="error_username"> <span class="info" id="info_user">Username tidak dapat diubah. </span><i class="ion ion-help-circled" data-toggle="tooltip" title="Username tidak dapat diubah"></i></label>
                     </div>
                 </div>
                 <div class="item form-group">
                     <div class="col-sm-8">
                        <input type="password" name="password" class="form-control" required="required" placeholder="Password Haneul" id="pass">
                     </div>
                 </div>
                 <div class="item form-group">
                    <div class="col-sm-8">
                        <label for="">Dengan klik daftar, kamu telah menyetujui <a href="{{ url('main/aturan_penggunaan.html') }}">Aturan Penggunaan</a> dan <a href="{{ url('main/kebijakan_privasi.html') }}">Kebijakan Privasi</a> dari Haneul.</label>
                    </div>
                 </div>
                 <div class="form-group">
                     <div class="col-sm-8">
                         <button type="submit" class="btn btn-submit">Daftar</button>
                    </div>
                 </div>
                 <div class="form-group">
                     <div class="col-sm-8">
                        <center><label for="">Sudah punya akun? <a href="#" data-toggle="modal" data-target="#login">Silahkan login</a> </label></center>
                    </div>
                 </div>
             {!! Form::close() !!}
             <!-- </form> -->
         </div>
     </div>
   </div>
 </section>
@endsection
