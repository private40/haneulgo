@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-2">
                        <img class="profile-user-img img-responsive" src="{{ (isset($foto) ? $foto : url('storage/pengguna/default.png')) }}" alt="User profile picture" width="100%">
                    </div>
                    <div class="col-sm-10">
                        <h1 class="title">Pesan - {{ session()->get('username') }}</h1>
                        <p>Halaman Pengguna</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#action-->

<section id="blog" class="padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-5">
                <div class="sidebar blog-sidebar">
                    <div class="sidebar-item categories">
                        <h3>Menu</h3>
                        <ul class="nav navbar-stacked">
                            <li><a href="{{ url('pengguna/'.$username) }}">Data Pengguna</a></li>
                            <li class="active"><a href="{{ url('pengguna/'.$username.'/message') }}" onclick="read('{{ (isset($kodePesan) ? $kodePesan : '') }}')">Pesan <span class="pull-right" id="count_pesan">{{ ($pesan > 0 ? '('.$pesan.')' : '') }}</span></a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/order') }}">Transaksi <span class="pull-right">{{ ($transaksi > 0 ? '('.$transaksi.')' : '') }}</span></a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/testimony') }}">Testimoni</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-sm-offset-1 col-md-offset-1">
                <div class="row">
                     <div class="col-md-12 col-sm-12">
                        <div class="single-blog two-column">
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="#">Pesan Kepada Admin</a></h2><br>
                                <div class="row">
                                  <div class="col-md-12">
                                  @if($data_pesan)
                                    @foreach($list_pesan as $test)
                                        @if($tglPesan != $dateTimeMsg[$test->id]['dateMsg'])
                                          <div class="row">
                                            <div class="alert">
                                              <center><span class="label label-warning">{{ $dateTimeMsg[$test->id]['dateMsg'] }}</span></center>
                                            </div>
                                          </div>
                                        @endif
                                        @if($test->status == "Admin")
                                          <div class="row">
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                              <div class="alert alert-info alert-dismissible">
                                                {{ $test->pesan }}
                                              </div>
                                            </div>
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                              {{ $dateTimeMsg[$test->id]['timeMsg'] }}
                                            </div>
                                          </div>
                                        @else
                                          <div class="row">
                                            <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-4 col-sm-offset-4 col-xs-offset-4 text-right">
                                              {{ $dateTimeMsg[$test->id]['timeMsg'] }}
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                              <div class="alert alert-success alert-dismissible text-right">
                                                {{ $test->pesan }}
                                              </div>
                                            </div>
                                          </div>
                                        @endif
                                        <?php $tglPesan = $dateTimeMsg[$test->id]['dateMsg']; ?>
                                    @endforeach
                                @endif
                                </div>
                                </div>
                                {!! Form::open(['action' => 'MessageController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                                <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    {!! Form::hidden('kode_pesan', (isset($kodePesan) ? $kodePesan : '') , ['class' => 'form-control', 'placeholder' => 'Kode Pesan']) !!}
                                    <textarea name="pesan" class="resizable_textarea form-control" placeholder="Ketikkan balasan" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                                  </div>
                                </div>
                                <div class="compose-btn pull-right">
                                  {!! Form::submit('Balas', ['class' => "btn btn-submit", 'id' => 'btnSimpan']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
             </div>
        </div>
    </div>
</section>
<!--/#blog-->
@endsection
