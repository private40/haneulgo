@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-2">
                        <img class="profile-user-img img-responsive" src="{{ (isset($foto) ? $foto : url('storage/pengguna/default.png')) }}" alt="User profile picture" width="100%">
                    </div>
                    <div class="col-sm-10">
                        <h1 class="title">Transaksi - {{ session()->get('username') }}</h1>
                        <p>Halaman Pengguna</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#action-->

<section id="blog" class="padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-5">
                <div class="sidebar blog-sidebar">
                    <div class="sidebar-item categories">
                        <h3>Menu</h3>
                        <ul class="nav navbar-stacked">
                            <li><a href="{{ url('pengguna/'.$username) }}">Data Pengguna</a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/message') }}" onclick="read('{{ (isset($kodePesan) ? $kodePesan : '') }}')">Pesan <span class="pull-right" id="count_pesan">{{ ($pesan > 0 ? '('.$pesan.')' : '') }}</span></a></li>
                            <li class="active"><a href="{{ url('pengguna/'.$username.'/order') }}">Transaksi <span class="pull-right">{{ ($transaksi > 0 ? '('.$transaksi.')' : '') }}</span></a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/testimony') }}">Testimoni</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-sm-offset-1 col-md-offset-1">
                <div class="row">
                     <div class="col-md-12 col-sm-12">
                       <div class="single-blog two-column">
                           <div class="post-content overflow">
                               <h2 class="post-title bold"><a href="#">Daftar Transaksi Anda</a></h2><br>
                                  <table id="datatable-keytable" class="table table-striped table-bordered">
                         <thead>
                           <tr>
                             <th>No</th>
                             <th>Kode Transaksi</th>
                             <th>Tanggal Pemesanan</th>
                             <th>Biaya</th>
                             <th>Status</th>
                             <th>Action</th>
                           </tr>
                         </thead>
                         <tbody>
                           @foreach($list_transaksi as $transaksi)
                             <tr>
                               <td>{{ $no }}</td>
                               <td>{{ $transaksi->kode_order }}</td>
                               <td>{{ $detailTransaksi[$transaksi->kode_order]['tanggal'] }}</td>
                               <td>{{ $detailTransaksi[$transaksi->kode_order]['biaya'] }}</td>
                               <td>{{ $transaksi->status }}</td>
                               <td>
                                  <a href="{{ url('order/'.$transaksi->kode_order) }}" target="_blank" class="btn btn-primary btn-xs"> Detail</a>
                               </td>
                             </tr>
                             <?php $no++; ?>
                           @endforeach
                         </tbody>
                       </table>
                           </div>
                       </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
</section>
<!--/#blog-->
@endsection
