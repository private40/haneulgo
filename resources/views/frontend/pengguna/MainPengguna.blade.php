@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-2">
                        <img class="profile-user-img img-responsive" src="{{ (isset($foto) ? $foto : url('storage/pengguna/default.png')) }}" alt="User profile picture" width="100%">
                    </div>
                    <div class="col-sm-10">
                        <h1 class="title">{{ session()->get('username') }}</h1>
                        <p>Halaman Pengguna</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#action-->

<section id="blog" class="padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-5">
                <div class="sidebar blog-sidebar">
                    <div class="sidebar-item categories">
                        <h3>Menu</h3>
                        <ul class="nav navbar-stacked">
                            <li class="active"><a href="{{ url('pengguna/'.$username) }}">Data Pengguna</a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/message') }}" onclick="read('{{ (isset($kodePesan) ? $kodePesan : '') }}')">Pesan <span class="pull-right" id="count_pesan">{{ ($pesan > 0 ? '('.$pesan.')' : '') }}</span></a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/order') }}">Transaksi <span class="pull-right">{{ ($transaksi > 0 ? '('.$transaksi.')' : '') }}</span></a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/testimony') }}">Testimoni</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-sm-offset-1 col-md-offset-1">
                <div class="row">
                     <div class="col-md-12 col-sm-12">
                         <ul id="tab1" class="nav nav-tabs">
                            <li class="active"><a href="#tab1-item1" data-toggle="tab">Data Pengguna</a></li>
                            <li><a href="#tab1-item2" data-toggle="tab">Foto</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab1-item1">
                                <div class="single-blog two-column">
                                    <div class="post-content overflow">
                                        <h2 class="post-title bold"><a href="#">Data Pengguna</a></h2><br>
                                        {!! Form::open(['action' => 'PenggunaController@update', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-3">Username</label>
                                          <div class="col-md-8 col-sm-8 col-xs-8">
                                            {!! Form::text('username', $username, ['class' => 'form-control', 'placeholder' => 'Username', 'required' => 'true', 'readonly' => 'true']) !!}
                                            {!! Form::label('nama', 'Username tidak dapat diganti.' , ['class' => 'col-sm-12 control-label error']) !!}
                                          </div>
                                        </div>
        
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-3">Nama Lengkap</label>
                                          <div class="col-md-8 col-sm-8 col-xs-8">
                                            {!! Form::text('nama_lengkap', $nama_lengkap, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap', 'required' => 'true']) !!}
                                            {!! ($errors->has('nama_lengkap')) ? Form::label('nama_lengkap', $errors->first('nama_lengkap') , ['class' => 'col-sm-12 control-label error']) : '' !!}
                                          </div>
                                        </div>
        
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-3">Email</label>
                                          <div class="col-md-8 col-sm-8 col-xs-8">
                                            {!! Form::email('email', $email, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'true']) !!}
                                            {!! ($errors->has('email')) ? Form::label('email', $errors->first('email') , ['class' => 'col-sm-12 control-label error']) : '' !!}
                                          </div>
                                        </div>
        
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-3">Jenis Kelamin</label>
                                          <div class="col-md-8 col-sm-8 col-xs-8">
                                            {!! Form::select('jenis_kelamin', ['1' => 'Laki Laki', '2' => 'Perempuan'], $jenis_kelamin, ['class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                                            {!! ($errors->has('jenis_kelamin')) ? Form::label('jenis_kelamin', $errors->first('jenis_kelamin') , ['class' => 'col-sm-12 control-label error']) : '' !!}
                                          </div>
                                        </div>
        
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-3">Password</label>
                                          <div class="col-md-8 col-sm-8 col-xs-8">
                                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'true']) !!}
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-3"></label>
                                          <div class="col-md-8 col-sm-8 col-xs-8">
                                            <button type="submit" class="btn btn-submit">Simpan</button>
                                          </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab1-item2">
                                <div class="row">
                                    <div class="col-md-4 padding-top-bottom">
                                      <center>
                                        <img class="profile-user-img img-responsive" src="{{ (isset($foto) ? $foto : url('storage/pengguna/default.png')) }}" alt="User profile picture" width="60%" id="fotonya">
                                      </center>
                                    </div>
                                    <div class="col-md-8 padding-top-bottom">
                                        <form action="{{ (isset($username) ? url('/admin/pengguna/upload/'.$username) : url('/admin/pengguna/upload/new')) }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                                            {{ csrf_field() }}
                                            <div class="dz-message">
                                              <div>
                                                <h5>Letakkan foto Anda disini atau klik untuk mengunggah.</h5>
                                              </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
             </div>
        </div>
    </div>
</section>

<script>
    Dropzone.options.myDropzone = {
        paramName: 'file',
        maxFilesize: 5, // MB
        maxFiles: 20,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        init: function() {
            this.on("success", function(file, response) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = response;
                  var lokasi = aa.innerHTML;
                  $('#fotonya').attr('src', "{{ asset('/images') }}" + "/" + lokasi);
            }),
            this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 1 Gambar");
            }),
            this.on("removedfile", function(file) {
                var name   = file.previewElement.querySelector("[data-dz-name]");
                var lokasi = name.innerHTML;
                var _token = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: 'POST',
                    url: "{{ url('/admin/pengguna/remove/') }}",
                    data: { lokasi, kode, _token },
                    dataType: 'html',
                });
                $('#fotonya').attr('src', "{{ asset('/images/pengguna/default.png') }}");
            }),
            this.on("addedfile", function(file) {
              var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
              var _this = this;

              removeButton.addEventListener("click", function(e) {
                  e.preventDefault();
                  e.stopPropagation();
                  var conf = confirm("Apakah anda yakin menghapus foto ini?");
                  if (conf == true) {
                     _this.removeFile(file);
                }
              });

              file.previewElement.appendChild(removeButton);
            });
        }
    };
</script>
<!--/#blog-->
@endsection
