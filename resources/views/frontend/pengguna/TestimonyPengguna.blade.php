@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-2">
                        <img class="profile-user-img img-responsive" src="{{ (isset($foto) ? $foto : url('storage/pengguna/default.png')) }}" alt="User profile picture" width="100%">
                    </div>
                    <div class="col-sm-10">
                        <h1 class="title">Testimoni - {{ session()->get('username') }}</h1>
                        <p>Halaman Pengguna</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#action-->

<section id="blog" class="padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-5">
                <div class="sidebar blog-sidebar">
                    <div class="sidebar-item categories">
                        <h3>Menu</h3>
                        <ul class="nav navbar-stacked">
                            <li><a href="{{ url('pengguna/'.$username) }}">Data Pengguna</a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/message') }}" onclick="read('{{ (isset($kodePesan) ? $kodePesan : '') }}')">Pesan <span class="pull-right" id="count_pesan">{{ ($pesan > 0 ? '('.$pesan.')' : '') }}</span></a></li>
                            <li><a href="{{ url('pengguna/'.$username.'/order') }}">Transaksi <span class="pull-right">{{ ($transaksi > 0 ? '('.$transaksi.')' : '') }}</span></a></li>
                            <li class="active"><a href="{{ url('pengguna/'.$username.'/testimony') }}">Testimoni</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-sm-offset-1 col-md-offset-1">
                 @if(session()->has('info'))
                    <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                       </button>
                        {!! session()->get('info') !!}
                     </div>
                     <script type="text/javascript">
                         $(document).ready(function(){
                           destroy();
                         });
                     </script>
                @endif
                
                <div class="row">
                    {!! Form::open(['action' => 'TestimonieController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                    <div class="row">
                      <label class="col-md-12 col-sm-12 col-xs-12">Silahkan beri testimoni Anda.</label>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::hidden('id', '' , ['class' => 'form-control', 'placeholder' => 'id', 'id' => 'id_testi']) !!}
                        <textarea name="testimoni" class="resizable_textarea form-control" placeholder="Ketikkan testimoni" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;" id="testimoni"></textarea>
                      </div>
                    </div>
                    <div class="compose-btn pull-right">
                      {!! Form::submit('Kirim', ['class' => "btn btn-submit", 'id' => 'btnSimpan']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="row" style="padding-bottom: 20px">
                     <div class="col-md-12 col-sm-12">
                       <div class="single-blog two-column">
                           <div class="post-content overflow">
                               <h2 class="post-title bold"><a href="#">Daftar Testimoni Anda</a></h2><br>
                                  <table id="datatable-testimony" class="table table-striped table-bordered">
                                   <thead>
                                     <tr>
                                       <th>No</th>
                                       <th>Tanggal</th>
                                       <th>Testimoni</th>
                                       <th width="20%">Action</th>
                                     </tr>
                                   </thead>
                                   <tbody>
                                     @foreach($list_testimoni as $testimoni)
                                       <tr>
                                         <td>{{ $no_testi }}</td>
                                         <td>{{ $detailTestimoni[$testimoni->id]['tanggal'] }}</td>
                                         <td>{{ $testimoni->testimoni }}</td>
                                         <td>
                                            <button class="btn btn-primary btn-xs" onclick="editTesti('{{ $testimoni->id }}')"> Edit</button>
                                            <button class="btn btn-danger btn-xs" onclick="deleteTesti('{{ $testimoni->id }}')" data-toggle="modal" data-target="#delete"> Delete</button>
                                         </td>
                                       </tr>
                                       <?php $no_testi++; ?>
                                     @endforeach
                                   </tbody>
                                 </table>
                           </div>
                       </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
</section>

  <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Testimoni ini? </h4>
        </div>
        <div class="modal-body">
          Apakah benar Anda ingin menghapus testimoni ini ?
          <blockquote id="isi_testi">
            For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.
          </blockquote>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <a href="{{ url('testimony/delete') }}" id="delete_testi" class="btn btn-danger"> Delete</a>
        </div>
      </div>
    </div>
  </div>
<!--/#blog-->
@endsection
