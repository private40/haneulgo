@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">Pemesanan</h1>
                        <p>Detail Pemesanan</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="company-information" class="choose">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 padding-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                <h2>Data Pemesan</h2>
                <div class="row">
                  <table class="table table-bordered">
                      <tbody>
                          <tr>
                            <td width="40%">Username</td>
                            <td><strong>{{ $pengguna->username }}</strong></td>
                          </tr>
                          <tr>
                            <td>Nama Lengkap</td>
                            <td>{{ $pengguna->nama_lengkap }}</td>
                          </tr>
                          <tr>
                            <td>Email</td>
                            <td>{{ $pengguna->email }}</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
            </div>

            <div class="col-sm-7 col-sm-offset-1 padding-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                <strong><h2>Data Travel</h2></strong>
              {!! Form::open(['action' => 'OrderController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="row" style="margin-top:20px">
                  <div class="col-sm-12">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Paket
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {{ $nama_paket }}
                          {!! Form::hidden('kode_paket', (isset($kode_paket) ? $kode_paket : ''), ['class' => 'form-control', 'placeholder' => 'Kode Paket']) !!}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Paket
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {{ $harga_paket }}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Travel
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!! Form::date('tgl_berangkat', \Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'true']) !!}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jumlah Orang
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!! Form::number('jumlah_orang', 1, ['class' => 'form-control', 'placeholder' => 'Jumlah Orang', 'required' => 'true', 'min' => 1, 'max' => 100, 'id' => 'jumlah_orang']) !!}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fitur Tambahan
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          @foreach($featureList as $feature)
                            <input type="checkbox" value="{{ $feature->kode_paket }}" name="fitur[]" class="feature"> {{ $feature->nama_paket.' (Rp. '.number_format($feature->harga_paket,0,'','.').',-)' }} <br>
                          @endforeach
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!! Form::textarea('keterangan', '', ['class' => 'form-control', 'placeholder' => 'Keterangan', 'rows' => 3]) !!}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Total
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon">Rp. </span>
                            {!! Form::text('total', $total, ['class' => 'form-control', 'placeholder' => 'Total', 'readonly' => 'true', 'id' => 'total']) !!}
                            {!! Form::hidden('harga_paket', $harga, ['class' => 'form-control', 'placeholder' => 'Harga Paket', 'readonly' => 'true', 'id' => 'harga_paket']) !!}
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-common uppercase pull-right">Pesan</button>
              {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>

{!! $_notes !!}
@endsection
