@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">{{ $kode_order }}</h1>
                        <p>Bayar Pemesanan</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="company-information" class="choose">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 padding-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                <h2>Perhatian</h2>
                <p style="color:red; margin-bottom:20px">Apabila Anda menambahkan keterangan, harap hubungi Customer Service terlebih dahulu.</p>
                <div class="row">
                  <div class="col-sm-4"><strong>Kode Pemesanan</strong></div>
                  <div class="col-sm-8">{{ $kode_order }}</div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><strong>Tanggal Pemesanan</strong></div>
                  <div class="col-sm-8">{{ $created_at }}</div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><strong>Username</strong></div>
                  <div class="col-sm-8">{{ $username }}</div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><strong>Nama Lengkap</strong></div>
                  <div class="col-sm-8">{{ $nama_lengkap }}</div>
                </div>
                <div class="row" style="margin-bottom: 25px">
                  <div class="col-sm-4"><strong>Email</strong></div>
                  <div class="col-sm-8">{{ $email }}</div>
                </div>

                <div class="row" style="margin-bottom: 25px">
                  <table class="table table-bordered">
                      <tbody>
                          <tr>
                            <td width="40%"><strong>Nama Paket</strong></td>
                            <td>{{ $nama_paket }}</td>
                          </tr>
                          <tr>
                            <td><strong>Harga Paket</strong></td>
                            <td>{{ $harga_paket }}</td>
                          </tr>
                          <tr>
                            <td><strong>Jumlah Orang</strong></td>
                            <td>{{ $jumlah_orang }}</td>
                          </tr>
                          <tr>
                            <td><strong>Fitur Tambahan</strong></td>
                            <td>{{ $fitur_tambahan }}</td>
                          </tr>
                          <tr>
                            <td><strong>Total</strong></td>
                            <td>{{ $total }}</td>
                          </tr>
                          <tr>
                            <td><strong>Keterangan</strong></td>
                            <td>{{ $keterangan }}</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                {!! Form::open(['action' => 'OrderController@pay', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="row mar-bottom">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Transfer
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        {!! Form::date('tgl_transfer', \Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'true']) !!}
                      </div>
                    </div>
                </div>
                <div class="row mar-bottom">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> Nominal Transfer
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp. </span>
                          {!! Form::text('nominal_transfer', $nominal_transfer, ['class' => 'form-control', 'placeholder' => 'Nominal Transfer', 'onkeypress' => 'return numeric(event);']) !!}
                        </div>
                        {!! Form::hidden('kode_order', $kode_order, ['class' => 'form-control', 'placeholder' => 'Kode Paket']) !!}
                      </div>
                    </div>
                </div>
                <div class="row mar-bottom">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Rekening Tujuan
                      </label>
                    </div>
                </div>
                @foreach ($rekening as $rekening)
                    <div class="row mar-bottom">
                        <div class="col-md-1">
                          <div class="radio">
                            <label><input type="radio" name="rek_tujuan" value="{{ $rekening->id }}"></label>
                          </div>
                        </div>
                        <div class="col-md-5">
                            <img src="{{ asset('images/rekening/'.$rekening->nama_bank.'.png') }}" width="80%">
                        </div>
                        <div class="col-md-6">
                            {{ $rekening->nama_bank }} <br>
                            <strong>{{ $rekening->no_rek }}</strong> <br>
                            a/n {{ $rekening->pemilik_rek }}
                        </div>
                      </div>
                @endforeach
                <div class="row pull-right">
                    <button type="submit" class="btn btn-common">Bayar</button>
                </div>
                {!! Form::close() !!}
                <!-- <ul class="elements">
                    <li class="wow fadeInUp" data-wow-duration="900ms" data-wow-delay="100ms"><i class="fa fa-angle-right"></i> <strong>Day 1</strong> <br> </li>
                    <li class="wow fadeInUp" data-wow-duration="800ms" data-wow-delay="200ms"><i class="fa fa-angle-right"></i> <strong>Day 2</strong> <br> </li>
                    <li class="wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms"><i class="fa fa-angle-right"></i> <strong>Day 3</strong> <br> </li>
                    <li class="wow fadeInUp" data-wow-duration="600ms" data-wow-delay="400ms"><i class="fa fa-angle-right"></i> <strong>Day 4</strong> <br> </li>
                    <li class="wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms"><i class="fa fa-angle-right"></i> <strong>Day 5</strong> <br> </li>
                </ul> -->
            </div>
        </div>
    </div>
</section>

{!! $_notes !!}

@endsection
