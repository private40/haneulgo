@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">{{ $kode_order }}</h1>
                        <p>Detail Pemesanan</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="company-information" class="choose">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 padding-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                <h2>Detail Pemesanan</h2>
                <div class="row">
                  <div class="col-sm-4"><strong>Kode Pemesanan</strong></div>
                  <div class="col-sm-8">{{ $kode_order }}</div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><strong>Tanggal Pemesanan</strong></div>
                  <div class="col-sm-8">{{ $created_at }}</div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><strong>Status Pemesanan</strong></div>
                  <div class="col-sm-8"><span class="label {{ $label_status }}">{{ $status }}</span></div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><strong>Username</strong></div>
                  <div class="col-sm-8">{{ $username }}</div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><strong>Nama Lengkap</strong></div>
                  <div class="col-sm-8">{{ $nama_lengkap }}</div>
                </div>
                <div class="row" style="margin-bottom: 25px">
                  <div class="col-sm-4"><strong>Email</strong></div>
                  <div class="col-sm-8">{{ $email }}</div>
                </div>

                <div class="row" style="margin-bottom: 25px">
                  <table class="table table-bordered">
                      <tbody>
                          <tr>
                            <td width="40%"><strong>Nama Paket</strong></td>
                            <td>{{ $nama_paket }}</td>
                          </tr>
                          <tr>
                            <td><strong>Harga Paket</strong></td>
                            <td>{{ $harga_paket }}</td>
                          </tr>
                          <tr>
                            <td><strong>Jumlah Orang</strong></td>
                            <td>{{ $jumlah_orang }}</td>
                          </tr>
                          <tr>
                            <td><strong>Total</strong></td>
                            <td>{{ $total }}</td>
                          </tr>
                          <tr>
                            <td><strong>Keterangan</strong></td>
                            <td>{{ $keterangan }}</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="row" style="margin-bottom: 25px">
                  <table class="table table-bordered">
                      <tbody>
                          <tr>
                            <td width="40%"><strong>Tanggal Transfer</strong></td>
                            <td>{!! $tgl_transfer !!}</td>
                          </tr>
                          <tr>
                            <td><strong>Rekening Tujuan</strong></td>
                            <td>{!! $tujuan_transfer !!}</td>
                          </tr>
                          <tr>
                            <td><strong>Nominal Transfer</strong></td>
                            <td>{!! $nominal_transfer !!}</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="row pull-right">
                  @if($status == "Pemesanan")
                    <a href="{{ url('pay_order/'.$kode_order) }}" class="btn btn-common"> Bayar Pemesanan</a>
                  @endif
                </div>
            </div>
        </div>
    </div>
</section>

{!! $_notes !!}
@endsection
