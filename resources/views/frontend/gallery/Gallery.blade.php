@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">Gallery</h1>
                        <p>Kumpulan galeri member Haneul Go.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#action-->

<section id="portfolio">
    <div class="container">
        <div class="row">
            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-default active" href="#" data-filter="*">All</a></li>
                @foreach($jenis as $jenis_gal)
                    <li><a class="btn btn-default" href="#" data-filter=".{{ $jenis_gal['class'] }}">{{ ucwords($jenis_gal['name']) }}</a></li>
                @endforeach
            </ul><!--/#portfolio-filter-->
           
            <div class="portfolio-items">
                @foreach($galleryList as $gallery)
                <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item {{ $jenis[$gallery->jenis]['class'] }}">
                    <div class="portfolio-wrapper">
                        <div class="portfolio-single">
                            <div class="portfolio-thumb">
                                <img src="{{ $thumbGallery[$gallery->id] }}" class="img-responsive" alt="{{ $gallery->nama_galeri }}" height="100%">
                            </div>
                            <div class="portfolio-view">
                                <ul class="nav nav-pills">
                                    <li><a href="{{ $fotoGallery[$gallery->id] }}" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="portfolio-info ">
                            <h2>{{ $gallery->nama_galeri }}</h2>
                        </div>
                    </div>
                </div>
                @endforeach`
            </div>
        </div>
        <div class="row">
            <div class="portfolio-pagination">
                {!! $galleryList->links() !!}
            </div>
        </div>
    </div>
</section>
<!--/#portfolio-->
@endsection
