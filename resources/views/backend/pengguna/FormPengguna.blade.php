@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      @if(isset($username))
      <div class="col-md-4">
        <div class="x_panel">
          <div class="x_title">
            <h2>Foto <small>Pengguna</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row padding-top-bottom">
              <center>
                <img class="profile-user-img img-responsive" src="{{ (isset($foto) ? $foto : url('storage/pengguna/default.png')) }}" alt="User profile picture" width="60%" id="fotonya">
              </center>
            </div>
            <form action="{{ (isset($username) ? url('/admin/pengguna/upload/'.$username) : url('/admin/pengguna/upload/new')) }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                {{ csrf_field() }}
                <div class="dz-message">
                  <div>
                    <h5>Letakkan foto pengguna disini atau klik untuk mengunggah.</h5>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
      @endif

      <div class="col-md-8 {{ (!isset($username) ? 'col-md-offset-2' : '') }}">

        @if(session()->has('info'))
            <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
               </button>
                {!! session()->get('info') !!}
             </div>
             <script type="text/javascript">
                $(document).ready(function(){
                  destroy();
                });
             </script>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>Profile <small>Data</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              {!! Form::open(['action' => 'PenggunaController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="item form-group">
                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Username
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    @if(isset($username))
                      {!! Form::text('username', $username, ['class' => 'form-control', 'placeholder' => 'Username', 'readonly' => 'true', 'id' => 'user', 'onkeypress' => 'return pengguna(event)']) !!}
                    @else
                      {!! Form::text('username', '', ['class' => 'form-control', 'placeholder' => 'Username', 'required' => 'true', 'id' => 'user', 'onkeypress' => 'return pengguna(event)']) !!}
                      <label for="" id="info_user"><i class="fa fa-info-circle"></i> <span class="info">Username akan ter-convert menjadi lowercase.</span></label>
                    @endif
                     <label id="error" class="info hidden">Karakter tersebut tidak diperkenankan</label>
                     {!! Form::hidden('id', (isset($id) ? $id : ''), ['class' => 'form-control']) !!}
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Email
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::email('email', (isset($email) ? $email : ''), ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'true', 'id' => 'email']) !!}
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama Lengkap
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('nama_lengkap', (isset($nama_lengkap) ? $nama_lengkap : ''), ['class' => 'form-control', 'placeholder' => 'Nama Lengkap', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Jenis Kelamin
                  </label>
                  <div class="col-md-3 col-sm-3 col-xs-6">
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="1" {{ ($jenis_kelamin == 1 ? 'checked="true"' : '') }} /> Laki Laki
                  </div>
                  <div class="col-md-5 col-sm-5 col-xs-6">
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="2" {{ ($jenis_kelamin == 2 ? 'checked="true"' : '') }} /> Perempuan
                  </div>
                </div>
                @if(!isset($username))
                <div class="item form-group">
                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Password
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'true']) !!}
                  </div>
                </div>
                @endif
                <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3">
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>

              {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <script>
    Dropzone.options.myDropzone = {
        paramName: 'file',
        maxFilesize: 5, // MB
        maxFiles: 20,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        init: function() {
            this.on("success", function(file, response) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = response;
                  var lokasi = aa.innerHTML;
                  $('#fotonya').attr('src', "{{ asset('/images') }}" + "/" + lokasi);
            }),
            this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 1 Gambar");
            }),
            this.on("removedfile", function(file) {
                var name   = file.previewElement.querySelector("[data-dz-name]");
                var lokasi = name.innerHTML;
                var _token = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: 'POST',
                    url: "{{ url('/admin/pengguna/remove/') }}",
                    data: { lokasi, kode, _token },
                    dataType: 'html',
                });
                $('#fotonya').attr('src', "{{ asset('/images/pengguna/default.png') }}");
            }),
            this.on("addedfile", function(file) {
              var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
              var _this = this;

              removeButton.addEventListener("click", function(e) {
                  e.preventDefault();
                  e.stopPropagation();
                  var conf = confirm("Apakah anda yakin menghapus foto ini?");
                  if (conf == true) {
                     _this.removeFile(file);
                }
              });

              file.previewElement.appendChild(removeButton);
            });
        }
    };
</script>
@endsection
