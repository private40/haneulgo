  <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus <span class="span_name"></span> </h4>
        </div>
        <div class="modal-body">
          Apakah benar Anda ingin menghapus admin <strong class="span_name"></strong>?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <a href="{{ url('admin/admin/delete') }}" id="konfirm_delete" class="btn btn-danger"> Delete</a>
        </div>
      </div>
    </div>
  </div>

  <div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Data Pengguna - <span id="span_title"></span> </h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-2">
              <label>Username</label></br>
              <label>Nama Lengkap</label></br>
              <label>Email</label></br>
              <label>Jenis Kelamin</label></br>
              <label>Status</label></br>
              <label>Foto Member</label>
            </div>

            <div class="col-xs-4" style="border-right : 2px solid grey">
              <label id="username">-</label></br>
              <label id="nama_lengkap">-</label></br>
              <label id="email">-</label></br>
              <label id="jenis_kelamin">-</label></br>
              <label id="status">-</label></br>
              <img style="width: 60%; display: block;" src="{{ asset('/gentellela/images/user.png') }}" alt="image" id="foto"/>
            </div>

            <div class="col-xs-3">
              <label>Tanggal Daftar</label></br>
              <label>Jumlah Pemesanan Travel Berhasil</label></br>
            </div>

            <div class="col-xs-3">
              <label id="created_at">-</label></br>
              <label id="count_travel">-</label></br>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
        </div>
      </div>
    </div>
  </div>