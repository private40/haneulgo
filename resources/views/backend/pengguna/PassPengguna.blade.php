@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        @if(session()->has('info'))
          <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
             <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
             </button>
              {!! session()->get('info') !!}
           </div>
           <script type="text/javascript">
              $(document).ready(function(){
                destroy();
              });
           </script>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>Profile <small>Data</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              {!! Form::open(['action' => 'PenggunaController@storePass', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password Lama
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::password('password_lama', ['class' => 'form-control', 'placeholder' => 'Password Lama']) !!}
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password Baru
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password Baru']) !!}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3">
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>

              {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
