<div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kode Transaksi - <span id="span_title"></span> </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-6" style="border-right : 2px solid grey">
            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Kode Transaksi</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="kode_order">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Tanggal Pemesanan</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="created_at">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Username</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="username">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Nama Lengkap</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="nama_lengkap">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Email</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="email">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Foto Member</label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <img style="width: 60%; display: block;" src="{{ asset('/gentellela/images/user.png') }}" alt="image" id="foto"/>
              </div>
            </div>
          </div>

          <div class="col-xs-6">
            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Status</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12 label label-{{ $label }}" id="status">-</label>
            </div>
            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Tanggal Travel</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="tgl_travel">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Paket</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="nama_paket">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Jumlah Orang</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="jumlah_orang">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Harga Paket</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="harga_paket">-</label>
            </div>

            <div class="row padding-top-bottom">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Total Biaya</label>
              @if($tipe == "Pemesanan")
              <div class="col-md-8 col-sm-8 col-xs-12">
                {!! Form::text('total', '', ['class' => 'form-control has-feedback-left', 'placeholder' => 'Total Biaya', 'id' => 'total_biaya', 'onkeypress' => 'return numeric(event)']) !!}
                <span class=" form-control-feedback left" aria-hidden="true">Rp.</span>
              </div>
              @else
                <label class="control-label col-md-8 col-sm-8 col-xs-12" id="total_biaya">-</label>
              @endif
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Nominal Transfer</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="nominal_transfer">belum dibayar</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Transfer Ke</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="tujuan_transfer">belum dibayar</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Tanggal Transfer</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="tgl_transfer">belum dibayar</label>
            </div>

            <div class="row padding-top-bottom">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Keterangan</label>
              @if($tipe == "Pemesanan")
                <div class="col-md-8 col-sm-8 col-xs-12">
                  {!! Form::textarea('keterangan', '', ['class' => 'form-control', 'placeholder' => 'Keterangan', 'id' => 'keterangan', 'rows' => 3]) !!}
                </div>
              @else
                <label class="control-label col-md-8 col-sm-8 col-xs-12" id="keterangan">-</label>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        @if($tipe == "Pemesanan")
          <button type="button" class="btn btn-primary" onclick="perbaruiOrder()">Perbarui Order</button>
        @elseif($tipe == "Konfirmasi Pembayaran")
          <button type="button" class="btn btn-success" onclick="konfirmDetail()">Konfirmasi</button>
          <!-- <a href="#" class="btn btn-success" id="konfirm_detail"><i class="fa fa-check-circle"></i> Konfirmasi</a> -->
        @elseif($tipe == "Siap Berangkat")
          <button type="button" class="btn btn-warning" onclick="orderSelesai()">Order Selesai</button>
        @endif
      </div>
    </div>
  </div>
</div>