@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">

    <div class="row">
      <div class="col-md-8">

        @if(session()->has('info'))
          <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
             <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
             </button>
              {!! session()->get('info') !!}
           </div>
           <script type="text/javascript">
              $(document).ready(function(){
                destroy();
              });
           </script>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2><span id="tipe_order">{{ $tipe }}</span><small>Daftar</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Transaksi</th>
                  @if($tipe == "Siap Berangkat")
                    <th>Tanggal Travel</th>
                  @else
                    <th>Tanggal Pemesanan</th>
                  @endif
                  <th>Username</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                <?php $i = 1; ?>
                  @foreach ($orderList as $order)
                <?php
                    $tanggal      = date_create($order->updated_at);
                    $tgl          = date_format($tanggal, "d M Y, H:i:s");
                    $tanggal      = date_create($order->tgl_berangkat);
                    $tglBerangkat = date_format($tanggal, "d M Y");
                    $tanggal      = date_create($order->tgl_pulang);
                    $tglPulang    = date_format($tanggal, "d M Y");
                ?>
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $order->kode_order }}</td>
                    @if($tipe == "Siap Berangkat")
                      <td>{{ $tglBerangkat.' s/d '.$tglPulang }}</td>
                    @else
                      <td>{{ $tgl }}</td>
                    @endif
                    <td>{{ $order->username }}</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail" onclick="detail_transaksi('{{ $order->kode_order }}')"><i class="fa fa-file-text"></i> Detail</button>
                        @if($tipe == "Konfirmasi Pembayaran")
                        <a href="{{ url('admin/order/confirmation/'.$order->kode_order) }}" class="btn btn-success btn-xs" ><i class="fa fa-check-circle"></i> Konfirmasi</a>
                        @endif
                    </td>
                  </tr>
                <?php $i++; ?>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <!-- <div class="x_panel">
          <div class="x_title">
            <h2>Kurs <small>KRW-USD-IDR</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-4 col-sm-4 col-xs-6 tile text-center">
                <span>KRW</span>
                <h6>1</h6> -->
                <!-- <input type="text" name="" value="0" class="text-center form-control"> -->
              <!-- </div>

              @foreach($listKurs as $kurs)
              <div class="col-md-4 col-sm-4 col-xs-6 tile text-center">
                <span>{{ $kurs->country_to }}</span>
                <h6>{{ $kurs->kurs }}</h6> -->
                <!-- <input type="text" name="" value="0" class="text-center form-control"> -->
              <!-- </div>
              @endforeach
            </div>
          </div>
        </div> -->
        <div class="x_panel">
          <div class="x_title">
            <h2>Rekening <small>Daftar</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @foreach($listRekening as $rekening)
            <article class="media event">
              <a class="pull-left date">
                <p class="month">{{ $rekening->nama_bank }}</p>
                <p class="day"></p>
              </a>
              <div class="media-body">
                <a class="title" href="#">a/n {{ $rekening->pemilik_rek }}</a>
                <p>{{ $rekening->no_rek }}</p>
              </div>
            </article>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kode Transaksi - <span id="span_title"></span> </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-6" style="border-right : 2px solid grey">
            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Kode Transaksi</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="kode_order">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Tanggal Pemesanan</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="created_at">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Username</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="username">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Nama Lengkap</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="nama_lengkap">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Email</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="email">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Foto Member</label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <img style="width: 60%; display: block;" src="{{ asset('/gentellela/images/user.png') }}" alt="image" id="foto"/>
              </div>
            </div>
          </div>

          <div class="col-xs-6">
            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Status</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12 label label-{{ $label }}" id="status">-</label>
            </div>
            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Tanggal Travel</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="tgl_travel">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Paket</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="nama_paket">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Jumlah Orang</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="jumlah_orang">-</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Harga Paket</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="harga_paket">-</label>
            </div>

            <div class="row padding-top-bottom">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Total Biaya</label>
              @if($tipe == "Pemesanan")
              <div class="col-md-8 col-sm-8 col-xs-12">
                {!! Form::text('total', '', ['class' => 'form-control has-feedback-left', 'placeholder' => 'Total Biaya', 'id' => 'total_biaya', 'onkeypress' => 'return numeric(event)']) !!}
                <span class=" form-control-feedback left" aria-hidden="true">Rp.</span>
              </div>
              @else
                <label class="control-label col-md-8 col-sm-8 col-xs-12" id="total_biaya">-</label>
              @endif
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Nominal Transfer</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="nominal_transfer">belum dibayar</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Transfer Ke</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="tujuan_transfer">belum dibayar</label>
            </div>

            <div class="row">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Tanggal Transfer</label>
              <label class="control-label col-md-8 col-sm-8 col-xs-12" id="tgl_transfer">belum dibayar</label>
            </div>

            <div class="row padding-top-bottom">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Keterangan</label>
              @if($tipe == "Pemesanan")
                <div class="col-md-8 col-sm-8 col-xs-12">
                  {!! Form::textarea('keterangan', '', ['class' => 'form-control', 'placeholder' => 'Keterangan', 'id' => 'keterangan', 'rows' => 3]) !!}
                </div>
              @else
                <label class="control-label col-md-8 col-sm-8 col-xs-12" id="keterangan">-</label>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        @if($tipe == "Pemesanan")
          <button type="button" class="btn btn-primary" onclick="perbaruiOrder()">Perbarui Order</button>
        @elseif($tipe == "Konfirmasi Pembayaran")
          <button type="button" class="btn btn-success" onclick="konfirmDetail()">Konfirmasi</button>
          <!-- <a href="#" class="btn btn-success" id="konfirm_detail"><i class="fa fa-check-circle"></i> Konfirmasi</a> -->
        @elseif($tipe == "Siap Berangkat")
          <button type="button" class="btn btn-warning" onclick="orderSelesai()">Order Selesai</button>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
