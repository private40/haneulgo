@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      <div class="col-md-8 {{ (isset($kode_paket) ? '' : 'col-md-offset-2') }}">

        @if(session()->has('info'))
            <div class="alert alert-success alert-dismissible fade in" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
               </button>
                {!! session()->get('info') !!}
             </div>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>Paket <small>Detail</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              {!! Form::open(['action' => 'PackageController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Paket
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('nama_paket', (isset($nama_paket) ? $nama_paket : ''), ['class' => 'form-control', 'placeholder' => 'Nama Paket', 'required' => 'true']) !!}
                    {!! Form::hidden('kode_paket', (isset($kode_paket) ? $kode_paket : ''), ['class' => 'form-control', 'placeholder' => 'Kode Paket']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Paket
                  </label>
                  <div class="col-md-3 col-sm-3 col-xs-12">
                    {!! Form::select('deskripsi_paket', ['Only' => 'Only', 'Start from' => 'Start from'], (isset($deskripsi_paket) ? $deskripsi_paket :''), ['class' => 'form-control select2 col-md-5']) !!}
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('harga_paket', (isset($harga_paket) ? $harga_paket : ''), ['class' => 'form-control has-feedback-left', 'placeholder' => 'Harga Paket', 'required' => 'true', 'onkeypress' => 'return numeric(event)']) !!}
                    <span class=" form-control-feedback left" aria-hidden="true">Rp.</span>
                  </div>
                </div>

                <div class="form-group" style="margin-top:20px">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Destinasi
                  </label>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Day 1
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('1', (isset($day_1) ? $day_1 : ''), ['class' => 'form-control', 'placeholder' => 'Day 1 Destination', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Day 2
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('2', (isset($day_2) ? $day_2 : ''), ['class' => 'form-control', 'placeholder' => 'Day 2 Destination', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Day 3
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('3', (isset($day_3) ? $day_3 : ''), ['class' => 'form-control', 'placeholder' => 'Day 3 Destination', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Day 4
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('4', (isset($day_4) ? $day_4 : 'Freeday'), ['class' => 'form-control', 'placeholder' => 'Day 4 Destination', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Day 5
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('5', (isset($day_5) ? $day_5 : 'Back to Airport'), ['class' => 'form-control', 'placeholder' => 'Day 5 Destination', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">&nbsp;</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {{Form::submit('Simpan', array('class'=>'btn btn-success'))}}
                  </div>
                </div>
              {!! Form::close() !!}
          </div>
        </div>
      </div>
      
       @if(isset($kode_paket))
      <div class="col-md-4">
        <div class="x_panel">
          <div class="x_title">
            <h2>Foto <small>Paket</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row padding-top-bottom">
              <center>
                <img class="profile-user-img img-responsive" src="{{ (isset($foto_paket) ? $foto_paket : url('images/main/logo_ver.png')) }}" alt="User profile picture" width="60%" id="fotonya">
              </center>
            </div>
            <form action="{{ (isset($kode_paket) ? url('/admin/package/upload/'.$kode_paket) : url('/admin/package/upload/new')) }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                {{ csrf_field() }}
                <div class="dz-message">
                  <div>
                    <h5>Letakkan foto paket disini atau klik untuk mengunggah.</h5>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</div>
<script>
  $(function () {
    $('.select2').select2();
  });
</script>

  <script>
    Dropzone.options.myDropzone = {
        paramName: 'file',
        maxFilesize: 5, // MB
        maxFiles: 20,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        init: function() {
            this.on("success", function(file, response) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = response;
                  var lokasi = aa.innerHTML;
                  $('#fotonya').attr('src', "{{ asset('/images') }}" + "/" + lokasi);
            }),
            this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 1 Gambar");
            }),
            this.on("removedfile", function(file) {
                var name   = file.previewElement.querySelector("[data-dz-name]");
                var lokasi = name.innerHTML;
                var _token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: 'POST',
                    url: "{{ url('/admin/package/remove') }}",
                    data: { lokasi, _token },
                    dataType: 'html',
                });
                $('#fotonya').attr('src', "{{ asset('/images/package/default.png') }}");
            }),
            this.on("addedfile", function(file) {
              var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
              var _this = this;

              removeButton.addEventListener("click", function(e) {
                  e.preventDefault();
                  e.stopPropagation();
                  var conf = confirm("Apakah anda yakin menghapus foto ini?");
                  if (conf == true) {
                     _this.removeFile(file);
                }
              });

              file.previewElement.appendChild(removeButton);
            });
        }
    };
</script>
@endsection
