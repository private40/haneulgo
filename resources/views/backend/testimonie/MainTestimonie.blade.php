@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">

    <div class="row">
      <div class="col-md-12">

        @if(session()->has('info'))
            <div class="alert alert-{{ (session()->has('kelas') ? session()->get('kelas') : 'success') }} alert-dismissible fade in" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
               </button>
                {!! session()->get('info') !!}
             </div>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>Testimoni <small>Daftar</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Testimoni</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                <?php $i = 1; ?>
                  @foreach ($testimonyList as $testimony)
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $testimony->username }}</td>
                    <td>{{ $testimony->testimoni }}</td>
                    <td>{{ ($testimony->status == 0 ? 'Tidak Dipublikasikan' : 'Dipublikasikan') }}</td>
                    <td>
                        @if($testimony->status == 0)
                        <a href="{{ url('admin/testimony_status/'.$testimony->id) }}" class="btn btn-primary btn-xs" > Publish</a>
                        @else
                        <a href="{{ url('admin/testimony_status/'.$testimony->id) }}" class="btn btn-danger btn-xs" > Don't Publish</a>
                        @endif
                    </td>
                  </tr>
                <?php $i++; ?>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
