<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="judul">Kategori Baru</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Kategori
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('nama_kategori', '', ['class' => 'form-control', 'placeholder' => 'Nama Kategori', 'id' => 'nama_kategori']) !!}
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">Batal</button>
          <button type="button" class="btn btn-success" onclick="newCategory()">Simpan</button>
        </div>
      </div>
    </div>
  </div>
  
  <script>
      function newCategory(){
          var nama_kategori = $('#nama_kategori').val();
          
          $.ajax({
            type: 'POST',
            url: "{{ url('/admin/shop/category/selectList') }}",
            data: { nama_kategori, _token },
            success : function(done){
                if(done == 'empty'){
                    alert('Nama kategori harus diisi!');
                    $('#nama_kategori').focus();
                }else{
                    $('#pemberitahuan').removeClass('hidden');
                    $("#pemberitahuan").addClass("animated");
                    $("#pemberitahuan").addClass("fadeIn");
                    setTimeout(function () {
                      $("#pemberitahuan").addClass("animated");
                      $("#pemberitahuan").addClass("fadeOut");
                        }, 4000
                    );
                    setTimeout(function () {
                      $("#pemberitahuan").addClass("hidden");
                        }, 4500
                    );
                    
                    $('#nama_kategori').val('');
                    $("#kategori_produk").html(done);
                    $("#closeModal").click();
                }
            }, error: function(fail){
                alert('Terjadi kesalahan : \n ' + fail.responseText);
            }
         });
      }
  </script>