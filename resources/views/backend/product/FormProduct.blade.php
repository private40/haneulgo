@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      <div class="col-md-8">
        <div class="alert alert-success alert-dismissible fade in hidden" role="alert" id="pemberitahuan">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
           </button>
            Kategori baru berhasil ditambahkan!
        </div>

        <div class="x_panel">
          <div class="x_title">
            <h2>Produk <small>Detail</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              {!! Form::open(['action' => 'ProductController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Produk
                  </label>
                  <div class="col-md-7 col-sm-7 col-xs-12">
                    {!! Form::text('nama_produk', (isset($nama_produk) ? $nama_produk : ''), ['class' => 'form-control', 'placeholder' => 'Nama Produk', 'required' => 'true', 'autofocus' => 'true']) !!}
                    {!! Form::hidden('id', (isset($id) ? $id : ''), ['class' => 'form-control', 'placeholder' => 'ID']) !!}
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Produk
                  </label>
                  <div class="col-md-7 col-sm-7 col-xs-12">
                    {!! Form::text('harga_produk', (isset($harga_produk) ? $harga_produk : ''), ['class' => 'form-control has-feedback-left', 'placeholder' => 'Harga Produk', 'required' => 'true', 'onkeypress' => 'return numeric(event)']) !!}
                    <span class=" form-control-feedback left" aria-hidden="true">Rp.</span>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Stok Produk
                  </label>
                  <div class="col-md-7 col-sm-7 col-xs-12">
                    {!! Form::text('stok', (isset($stok) && $stok > 0 ? $stok : ''), ['class' => 'form-control', 'placeholder' => 'Jumlah Stok Produk', 'required' => 'true', 'onkeypress' => 'return numeric(event)']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan Produk
                  </label>
                  <div class="col-md-7 col-sm-7 col-xs-12">
                    {!! Form::textarea('keterangan_produk', (isset($keterangan_produk) ? $keterangan_produk : ''), ['class' => 'form-control', 'placeholder' => 'Keterangan Produk', 'rows' => 3]) !!}
                  </div>
                </div>
                 <div class="form-group" style="margin-top:20px">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kategori
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                      <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#form"><i class="fa fa-plus"></i> New Category</button>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kategori Produk
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <select class="form-control select2" multiple="multiple" data-placeholder="Select some Category" style="width: 100%;" name="kategori_produk[]" id="kategori_produk">
                        @foreach($categoryList as $category)
                            <option value="{{ $category->id }}" {{ (isset($kategori_produk) ? (in_array($category->id, $kategori_produk) ? 'selected' : '') : '') }}>{{ $category->nama_kategori }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">&nbsp;</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {{Form::submit('Simpan', ['class'=>'btn btn-success'])}}
                  </div>
                </div>
              {!! Form::close() !!}
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="x_panel">
          <div class="x_title">
            <h2>Foto <small>Produk</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row padding-top-bottom">
              <center>
                <img class="profile-user-img img-responsive" src="{{ (isset($foto_produk) ? $foto_produk : url('images/product/default.png')) }}" alt="User profile picture" width="60%" id="fotonya">
              </center>
            </div>
            <form action="{{ (isset($id) ? url('/admin/shop/product/upload/'.$id) : url('/admin/shop/product/upload/new')) }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                {{ csrf_field() }}
                <div class="dz-message">
                  <div>
                    <h5>Letakkan foto produk disini atau klik untuk mengunggah.</h5>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  {!! $_modal !!}

  <script>
      $(function () {
        $('.select2').select2();
      });

    Dropzone.options.myDropzone = {
        paramName: 'file',
        maxFilesize: 5, // MB
        maxFiles: 20,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        init: function() {
            this.on("success", function(file, response) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = response;
                  var lokasi = aa.innerHTML;
                  $('#fotonya').attr('src', "{{ asset('/images') }}" + "/" + lokasi);
            }),
            this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 20 Gambar");
            }),
            this.on("removedfile", function(file) {
                var name   = file.previewElement.querySelector("[data-dz-name]");
                var lokasi = name.innerHTML;
                var _token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: 'POST',
                    url: "{{ url('/admin/shop/product/remove') }}",
                    data: { lokasi, _token },
                    dataType: 'html',
                });
                $('#fotonya').attr('src', "{{ asset('/images/product/default.png') }}");
            }),
            this.on("addedfile", function(file) {
              var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
              var _this = this;

              removeButton.addEventListener("click", function(e) {
                  e.preventDefault();
                  e.stopPropagation();
                  var conf = confirm("Apakah anda yakin menghapus foto ini?");
                  if (conf == true) {
                     _this.removeFile(file);
                }
              });

              file.previewElement.appendChild(removeButton);
            });
        }
    };
</script>
@endsection
