<div id="delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus Produk - <span class="span_name"></span> </h4>
      </div>
      <div class="modal-body">
        Apakah benar, Anda ingin menghapus Produk <strong class="span_name"></strong>?
      </div>
      <div class="modal-footer">
        <a href="{{ url('admin.html') }}" class="btn btn-danger" id="konfirm_delete"><i class="fa fa-check-times"></i> Delete</a>
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
    </div>
  </div>
</div>
