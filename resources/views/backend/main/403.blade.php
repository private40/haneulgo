@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      <div class="col-md-12">

        <div class="x_panel">
          <div class="x_title">
            <h2>Forbidden <small>Page</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <h3>Sorry, you haven't access on this page. This page is under maintenance by Developer.</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
