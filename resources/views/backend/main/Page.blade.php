@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      <div class="col-md-12">

        @if(session()->has('info'))
            <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
               </button>
                {!! session()->get('info') !!}
             </div>
             <script type="text/javascript">
                 $(document).ready(function(){
                   destroy();
                 });
             </script>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>{{ $page['title'] }} <small>Teks</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            {!! Form::open(['action' => 'AdminController@storePage', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
            {!! Form::hidden('nama_file', $nama_file, ['class' => 'form-control', 'placeholder' => 'Nama File']) !!}
            <div class="row"><div class="col-md-12 padding-top-bottom">
              <textarea name="text" class="textarea" autofocus id="editor1" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                  {{ $page['content'] }}
              </textarea>
              </div>
            </div>
            <div class="row pull-right"> <div class="col-md-12">
              <button class="btn btn-success" type="submit">Simpan</button>
              </div>
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
  <script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
    });
  </script>
@endsection
