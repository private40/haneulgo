<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="url" content="{{ url('/admin/') }}">
  <meta name="home" content="{{ url('/') }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Haneul Go | {{ (isset($title) ? $title : "Admin Page" ) }}</title>
  <link rel="shortcut icon" href="{{ asset('images/main/logo.ico') }}">
  <link href="{{ asset('gentellela/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('gentellela/fonts/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('gentellela/css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('gentellela/build/nprogress/nprogress.css') }}" rel="stylesheet">
  <link href="{{ asset('gentellela/build/select2/css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('gentellela/build/css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('gentellela/css/maps/jquery-jvectormap-2.0.3.css') }}"  rel="stylesheet" type="text/css"/>
  <link href="{{ asset('gentellela/css/icheck/flat/green.css') }}" rel="stylesheet">
  <link href="{{ asset('gentellela/css/floatexamples.css') }}" rel="stylesheet" />
  <link href="{{ asset('gentellela/js/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('gentellela/js/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('gentellela/js/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('gentellela/js/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('gentellela/js/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('dropzone/dist/dropzone.css') }}" rel="stylesheet">
  <script src="{{ asset('gentellela/js/jquery.min.js') }}"></script>
  <script src="{{ asset('gentellela/build/select2/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('dropzone/dist/dropzone.js') }}"></script>
  <script src="{{ asset('js/haneul-back-1.0.4.js') }}"></script>
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title"><i class="fa fa-globe"></i> Haneul Go</a>
          </div>
          <div class="clearfix"></div>

          <div class="profile">
            <div class="profile_pic">
              <img src="{{ session()->get('foto') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <h2>{{ session()->get('username') }}</h2>
              <span>{{ session()->get('status') }}</span>
            </div>
          </div>

          <br />
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <h3>&nbsp;</h3>
              <ul class="nav side-menu">
                <li><a href="{{ url('admin.html') }}"><i class="fa fa-home"></i> Home</a></li>
                <li><a><i class="fa fa-plane"></i> Order Travel <span class="fa fa-chevron-down"></span><span class="label label-success pull-right">{{ ($countConfirm > 0 ? $countConfirm : '') }}</span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{ url('admin/order_list.html') }}">Pemesanan</a>
                    </li>
                    <li><a href="{{ url('admin/needconfirm_list.html') }}">Konfirmasi Pembayaran <span class="label label-success pull-right">{{ ($countConfirm > 0 ? $countConfirm : '') }}</span></a>
                    </li>
                    <li><a href="{{ url('admin/readytogo_list.html') }}">Siap Berangkat</a>
                    </li>
                    <li><a href="{{ url('admin/completed_list.html') }}">Travel Selesai</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-shopping-cart"></i> Order Produk <span class="fa fa-chevron-down"></span><span class="label label-success pull-right"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{ url('dev/shop.html') }}">Pemesanan</a>
                    </li>
                    <li><a href="{{ url('dev/shop.html') }}">Konfirmasi Pembayaran <span class="label label-success pull-right"></span></a>
                    </li>
                    <li><a href="{{ url('dev/shop.html') }}">Proses Pengiriman</a>
                    </li>
                    <li><a href="{{ url('dev/shop.html') }}">Barang Diterima</a>
                    </li>
                  </ul>
                </li>
                <li class="{{ (isset($menu_msg) ? 'active' : '') }}"><a><i class="fa fa-envelope"></i> Pesan & Testimoni  <span class="fa fa-chevron-down"></span> <span class="label label-success pull-right">{{ ($unreadMsg > 0 ? $unreadMsg : '') }}</span></a>
                  <ul class="nav child_menu" style="{{ (isset($menu_msg) ? 'display: block' : 'display: none') }}">
                    <li class="{{ (isset($menu_msg) ? 'active' : '') }}"><a href="{{ url('admin/message_list.html') }}">Pesan <span class="label label-success pull-right">{{ ($unreadMsg > 0 ? $unreadMsg : '') }}</span></a>
                    </li>
                    <li><a href="{{ url('admin/testimony_list.html') }}">Testimoni</a>
                    </li>
                  </ul>
                </li>
                <li class="{{ (isset($menu_package) || isset($menu_product) || isset($menu_destination) ? 'active' : '') }}"><a><i class="fa fa-dropbox"></i> Paket & Produk  <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="{{ (isset($menu_package) || isset($menu_product) ? 'display: block' : 'display: none') }}">
                    <li><a>Paket & Fitur<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="{{ (isset($menu_package) ? 'display: block' : 'display: none') }}">
                        <li class="{{ (isset($menu_package) ? 'active' : '') }}"><a href="{{ url('admin/package.html') }}">Paket Wisata</a>
                        </li>
                        <li><a href="{{ url('admin/feature.html') }}">Fitur Paket</a>
                        </li>
                      </ul>
                    </li>
                    <li class="{{ (isset($menu_product) ? 'active' : '') }}"><a href="{{ url('admin/shop/product.html') }}">Produk</a>
                    </li>
                    <li class="{{ (isset($menu_destination) ? 'active' : '') }}"><a href="{{ url('admin/destination.html') }}"> Destinasi Wisata</a>
                    </li>
                    <li><a href="{{ url('admin/shop/category.html') }}"> Kategori Produk</a>
                    </li>
                  </ul>
                </li>
                <li class="{{ (isset($menu_user) ? 'active' : '') }}"><a><i class="fa fa-users"></i> Pengguna <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="{{ (isset($menu_user) ? 'display: block' : 'display: none') }}">
                    <li><a href="{{ url('admin/member_list.html') }}">Member</a>
                    </li>
                    <li class="{{ (isset($menu_user) ? 'active' : '') }}"><a href="{{ url('admin/admin_list.html') }}">Admin</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-table"></i> Data Haneul Go <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <!-- <li><a href="{{ url('admin/kurs.html') }}">Kurs</a>
                    </li> -->
                    <li><a href="{{ url('admin/account_list.html') }}">Rekening Bank</a>
                    </li>
                    <li><a href="{{ url('admin/gallery.html') }}">Gallery</a>
                    </li>
                    <li><a>Info Haneul<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: none;">
                        <li class="sub_menu"><a href="{{ url('admin/info_haneul/cara_pesan.html') }}">Cara Pesan</a>
                        </li>
                        <li><a href="{{ url('admin/info_haneul/syarat_dan_ketentuan.html') }}">Syarat & Ketentuan</a>
                        </li>
                        <li><a href="{{ url('admin/info_haneul/hubungi_kami.html') }}">Hubungi Kami</a>
                        </li>
                        <li><a href="{{ url('admin/info_haneul/aturan_penggunaan.html') }}">Aturan Penggunaan</a>
                        </li>
                        <!-- <li><a href="{{ url('admin/info_haneul/pusat_bantuan.html') }}">Pusat Bantuan</a>
                        </li> -->
                        <li><a href="{{ url('admin/info_haneul/kebijakan_privasi.html') }}">Kebijakan Privasi</a>
                        </li>
                        <li><a href="{{ url('admin/info_haneul/about.html') }}">Tentang Haneul</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-gear"></i> Pengaturan Akun <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{ url('admin/profile/'.session()->get('username')) }}">Profil</a>
                    </li>
                    <li><a href="{{ url('admin/password.html') }}">Password</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-wrench"></i> Development<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none;">
                    <li><a> Haneul Shop<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{ url('dev/shop.html') }}">User Layout</a>
                        </li>
                        <li><a href="{{ url('dev/shop.html') }}">Order Product</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="{{ url('signout') }}"><i class="fa fa-sign-out"></i> Keluar</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="{{ session()->get('foto') }}" alt="">{{ session()->get('username') }}
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="{{ url('admin/profile/'.session()->get('username')) }}">  Profile</a>
                  </li>
                  <li><a href="{{ url('signout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
              </li>

              <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-envelope-o"></i>
                  <span class="badge bg-green">{{ ($unreadMsg > 0 ? $unreadMsg : '') }}</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                  @foreach ($lastMsg as $msg)
                    <li>
                      <a href="{{ url('admin/message_list/'.$msg->kode_pesan) }}">
                        <span class="image">
                          <img src="{{ $msgDetail[$msg->kode_pesan]['foto'] }}" alt="Profile Image" />
                        </span>
                        <span>
                          <span>{{ $msg->username }}</span>
                          <span class="time">{{ $msgDetail[$msg->kode_pesan]['ketWaktu'] }}</span>
                        </span>
                        <span class="message">
                          {{ $msgDetail[$msg->kode_pesan]['pesan'] }}
                        </span>
                      </a>
                    </li>
                  @endforeach
                    <li>
                      <div class="text-center">
                        <a href="{{ url('admin/message_list.html') }}">
                          <strong>Lihat semua</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                </ul>
              </li>

            </ul>
          </nav>
        </div>
      </div>

      @yield('content')

      <footer>
  <div class="copyright-info">
    <p class="pull-right">@2018 All Rights Reserved Bagus Aulia. Template by <a href="https://colorlib.com">Colorlib</a>
    </p>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->

</div>
<!-- /page content -->
</div>


</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
</ul>
<div class="clearfix"></div>
<div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="{{ asset('gentellela/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('gentellela/js/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('gentellela/js/progressbar/bootstrap-progressbar.min.js') }}"></script>
<script src="{{ asset('gentellela/js/icheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentellela/js/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentellela/js/datepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('gentellela/js/chartjs/chart.min.js') }}"></script>
<script src="{{ asset('gentellela/js/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('gentellela/build/js/custom.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('gentellela/js/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('gentellela/build/nprogress/nprogress.js') }}"></script>
<script src="{{ asset('gentellela/build/validator/validator.js') }}"></script>
<script src="{{ asset('gentellela/js/textarea/autosize.min.js') }}"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
<script src="{{ asset('gentellela/js/pace/pace.min.js') }}"></script>
</body>

</html>
