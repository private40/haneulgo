@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        @if(session()->has('info'))
            <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
               </button>
                {!! session()->get('info') !!}
             </div>
             <script type="text/javascript">
                 $(document).ready(function(){
                   destroy();
                 });
             </script>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>Kurs <small>Data</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              {!! Form::open(['action' => 'KursController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="col-md-5 col-sm-5 col-xs-12 form-group has-feedback">
                  <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="First Name" disabled value="1">
                  <span class=" form-control-feedback left" aria-hidden="true">W</span>
                </div>

                <div class="col-md-1 col-sm-1 col-xs-12 form-group has-feedback">
                  <span class="fa fa-arrows-h form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" class="form-control has-feedback-left" id="inputSuccess3" placeholder="Last Name">
                  <span class=" form-control-feedback left" aria-hidden="true">Rp.</span>
                </div>

              {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
