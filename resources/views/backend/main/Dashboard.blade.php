@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row"><div class="col-xs-12">
        <h2>Haneul Go! <small>Informasi</small></h2>
    </div></div>
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-bell-o"></i>
          </div>
          <div class="count">{{ $countConfirm }}</div>

          <h3>Konfirmasi Bayar Travel</h3>
          <a href="{{ url('admin/needconfirm_list.html') }}"><p>Lihat semua.</p></a>
        </div>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-globe"></i>
          </div>
          <div class="count">{{ $countReady }}</div>

          <h3>Siap Berangkat</h3>
          <a href="{{ url('admin/readytogo_list.html') }}"><p>Lihat semua.</p></a>
        </div>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-users"></i>
          </div>
          <div class="count">{{ $countNew }}</div>

          <h3>Member Baru</h3>
          <a href="{{ url('admin/member_list.html') }}"><p>Lihat semua.</p></a>
        </div>
      </div>
    </div>
    
    <div class="row"><div class="col-xs-12">
        <h2>Haneul Shop <small>Informasi</small></h2>
    </div></div>
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-bell-o"></i>
          </div>
          <div class="count">0</div>

          <h3>Konfirmasi Haneul Shop</h3>
          <a href="#"><p>Lihat semua.</p></a>
        </div>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-truck"></i>
          </div>
          <div class="count">0</div>

          <h3>Siap Kirim</h3>
          <a href="#"><p>Lihat semua.</p></a>
        </div>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-dropbox"></i>
          </div>
          <div class="count">0</div>

          <h3>Produk</h3>
          <a href="#"><p>Lihat semua.</p></a>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8">

        @if(session()->has('info'))
          <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
             <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
             </button>
              {!! session()->get('info') !!}
           </div>
           <script type="text/javascript">
              $(document).ready(function(){
                destroy();
              });
           </script>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>Konfirmasi Pembayaran <small>Daftar</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Transaksi</th>
                  <th>Tanggal</th>
                  <th>Username</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                <?php $i = 1;
                  foreach ($listConfirm as $confirm) {
                    $tanggal  = date_create($confirm->updated_at);
                    $tgl      = date_format($tanggal, "d M Y, H:i:s");
                ?>
                  <tr>
                    <td><?= $i ?></td>
                    <td><?= $confirm->kode_order ?></td>
                    <td><?= $tgl ?></td>
                    <td><?= $confirm->username ?></td>
                    <td> <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail" onclick="detail_transaksi('{{ $confirm->kode_order }}')"><i class="fa fa-file-text"></i> Detail</button>
                         <a href="{{ url('admin/order/confirmation/'.$confirm->kode_order) }}" class="btn btn-success btn-xs" ><i class="fa fa-check-circle"></i> Konfirmasi</a> </td>
                  </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <!-- <div class="x_panel">
          <div class="x_title">
            <h2>Kurs <small>KRW-USD-IDR</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-4 col-sm-4 col-xs-6 tile text-center">
                <span>KRW</span>
                <h6>1</h6> -->
                <!-- <input type="text" name="" value="0" class="text-center form-control"> -->
              <!-- </div>

              @foreach($listKurs as $kurs)
              <div class="col-md-4 col-sm-4 col-xs-6 tile text-center">
                <span>{{ $kurs->country_to }}</span>
                <h6>{{ $kurs->kurs }}</h6> -->
                <!-- <input type="text" name="" value="0" class="text-center form-control"> -->
              <!-- </div>
              @endforeach
            </div>
          </div>
        </div> -->
        <div class="x_panel">
          <div class="x_title">
            <h2>Rekening <small>Daftar</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @foreach($listRekening as $rekening)
            <article class="media event">
              <a class="pull-left date">
                <p class="month">{{ $rekening->nama_bank }}</p>
                <p class="day"></p>
              </a>
              <div class="media-body">
                <a class="title" href="#">a/n {{ $rekening->pemilik_rek }}</a>
                <p>{{ $rekening->no_rek }}</p>
              </div>
            </article>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kode Transaksi - <span id="span_title"></span> </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-2">
            <label>Kode Transaksi</label></br>
            <label>Tanggal Transaksi</label></br>
            <label>Username</label></br>
            <label>Nama Member</label></br>
            <label>Email</label></br>
            <label>Foto Member</label>
          </div>

          <div class="col-xs-4" style="border-right : 2px solid grey">
            <label id="kode_order">-</label></br>
            <label id="created_at">-</label></br>
            <label id="username">-</label></br>
            <label id="nama_lengkap">-</label></br>
            <label id="email">-</label></br>
            <img style="width: 60%; display: block;" src="{{ asset('/gentellela/images/user.png') }}" alt="image" id="foto"/>
          </div>

          <div class="col-xs-2">
            <label>Tanggal Travel</label></br>
            <label>Paket</label></br>
            <label>Jumlah Orang</label></br>
            <label>Harga Paket</label></br>
            <label>Total Biaya</label></br>
            <label>Nominal Transfer</label></br>
            <label>Transfer Ke</label></br>
            <label>Tanggal Transfer</label></br>
          </div>

          <div class="col-xs-4">
            <label id="tgl_travel">-</label></br>
            <label id="nama_paket">-</label></br>
            <label id="jumlah_orang">-</label></br>
            <label id="harga_paket">-</label></br>
            <label id="total_biaya">-</label></br>
            <label id="nominal_transfer">-</label></br>
            <label id="tujuan_transfer">-</label></br>
            <label id="tgl_transfer">-</label></br>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="konfirmDetail()">Konfirmasi</button>
        <!-- <a href="<?= url('admin.html') ?>" class="btn btn-success" id="konfirm_detail"><i class="fa fa-check-circle"></i> Konfirmasi</a> -->
      </div>
    </div>
  </div>
</div>

@endsection
