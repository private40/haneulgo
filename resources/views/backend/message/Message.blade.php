@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>
              Pesan
              <small>
                  Daftar
              </small>
          </h3>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2> Inbox </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a href="#"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a href="#"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <div class="row">

              <div class="col-sm-4 mail_list_column">
                  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>&nbsp;</th>
                          </tr>
                      </thead>
                      <tbody>
                            @foreach($messageList as $list)
                            <tr><td>
                                <div class="mail_list">
                                  <div class="left">
                                    {!! ($list->read_admin == 0 ? '<i class="fa fa-circle"></i>' : '&nbsp') !!}
                                  </div>
                                  <a href="{{ url('admin/message_list/'.$list->kode_pesan) }}">
                                    <div class="right">
                                      <h3>{{ $list->username }} <small>{{ $messageDetail[$list->kode_pesan]['ketWaktu'] }}</small></h3>
                                      <p>{{ $messageDetail[$list->kode_pesan]['pesan'] }}</p>
                                    </div>
                                  </a>
                                </div>
                            </td></tr>
                            @endforeach
                      </tbody>
                    </table>
              </div>
              <!-- /MAIL LIST -->


              <!-- CONTENT MAIL -->
              <div class="col-sm-8 mail_view">
                <div class="inbox-body">
                  <div class="mail_heading row">
                    <div class="col-md-11">
                      <h4 id="detUsername"> {{ (isset($dataMsg->username) ? $dataMsg->username : 'No Message Selected') }}</h4>
                    </div>
                    <div class="col-md-1 pull-right">
                      <button title="" data-placement="top" data-toggle="tooltip" data-original-title="Trash" class="btn btn-sm tooltips"><i class="fa fa-trash-o"></i>
                    </div>
                  </div>
                  @if(isset($test))
                  <div class="view-mail">
                    @foreach($test as $test)
                        @if($tglPesan != $dateTimeMsg[$test->id]['dateMsg'])
                          <div class="row">
                            <div class="alert">
                              <center><span class="label label-warning">{{ $dateTimeMsg[$test->id]['dateMsg'] }}</span></center>
                            </div>
                          </div>
                        @endif
                        @if($test->status == "Admin")
                          <div class="row">
                              <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-4 col-sm-offset-4 col-xs-offset-4 text-right">
                                {{ $dateTimeMsg[$test->id]['timeMsg'] }}
                              </div>
                              <div class="col-md-7 col-sm-7 col-xs-7">
                                <div class="alert alert-success alert-dismissible text-right">
                                  {{ $test->pesan }}
                                </div>
                              </div>
                          </div>
                        @else
                          <div class="row">
                              <div class="col-md-7 col-sm-7 col-xs-7">
                                <div class="alert alert-info alert-dismissible">
                                  {{ $test->pesan }}
                                </div>
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-1">
                                {{ $dateTimeMsg[$test->id]['timeMsg'] }}
                              </div>
                          </div>
                        @endif
                        <?php $tglPesan = $dateTimeMsg[$test->id]['dateMsg']; ?>
                    @endforeach
                  </div>
                  {!! Form::open(['action' => 'MessageController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                  <div class="attachment">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::hidden('kode_pesan', (isset($kodePesan) ? $kodePesan : '') , ['class' => 'form-control', 'placeholder' => 'Kode Pesan']) !!}
                        <textarea name="pesan" class="resizable_textarea form-control" placeholder="Ketikkan balasan" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="compose-btn pull-right">
                    {!! Form::submit('Balas', ['class' => "btn btn-primary", 'id' => 'btnSimpan']) !!}
                  </div>
                  {!! Form::close() !!}
                  @endif
                </div>

              </div>
              <!-- /CONTENT MAIL -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
