<div id="delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus {{ (isset($tipe) ? $tipe : '') }} Paket - <span class="span_name"></span> </h4>
      </div>
      <div class="modal-body">
        Apakah benar, Anda ingin menghapus {{ (isset($tipe) ? strtolower($tipe) : '') }} paket <strong class="span_name"></strong>?
      </div>
      <div class="modal-footer">
        <a href="<?= url('admin.html') ?>" class="btn btn-danger" id="konfirm_delete"><i class="fa fa-check-times"></i> Delete</a>
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="judul">Fitur Paket Baru</h4>
      </div>
      {!! Form::open(['action' => 'PackageController@featureStore', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Fitur Paket
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! Form::text('nama_paket', '', ['class' => 'form-control', 'placeholder' => 'Nama Paket', 'id' => 'nama_paket']) !!}
                  {!! Form::hidden('kode_paket', '', ['class' => 'form-control', 'placeholder' => 'Kode Paket', 'id' => 'kode_paket']) !!}
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Fitur Paket
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! Form::text('harga_paket', '', ['class' => 'form-control has-feedback-left', 'placeholder' => 'Harga Paket', 'id' => 'harga_paket', 'onkeypress' => 'return numeric(event)']) !!}
                  <span class=" form-control-feedback left" aria-hidden="true">Rp.</span>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>