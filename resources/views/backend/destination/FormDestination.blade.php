@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="row">
      <div class="col-md-8 col-sm-8">
        <div class="x_panel">
          <div class="x_title">
            <h2>Destinasi Wisata <small>Detail</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              {!! Form::open(['action' => 'DestinationController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Destinasi
                  </label>
                  <div class="col-md-7 col-sm-7 col-xs-10">
                    {!! Form::text('nama_destinasi', (isset($nama_destinasi) ? $nama_destinasi : ''), ['class' => 'form-control', 'placeholder' => 'Nama Destinasi', 'required' => 'true', 'autofocus' => 'true']) !!}
                    {!! Form::hidden('id', (isset($id) ? $id : ''), ['class' => 'form-control', 'placeholder' => 'ID']) !!}
                    {!! Form::hidden('type', (isset($type) ? $type : ''), ['class' => 'form-control', 'placeholder' => 'type']) !!}
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan Destinasi
                  </label>
                  <div class="col-md-7 col-sm-7 col-xs-10">
                    <textarea name="keterangan_destinasi" class="textarea" rows="8" id="editor1" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                          {{ (isset($keterangan_destinasi) ? $keterangan_destinasi : '') }}
                    </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">&nbsp;</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {{Form::submit('Simpan', ['class' => 'btn btn-success'])}}
                  </div>
                </div>
              {!! Form::close() !!}
          </div>
        </div>
      </div>
      
      <div class="col-md-4 col-sm-4">
        <div class="x_panel">
          <div class="x_title">
            <h2>Foto <small>Produk</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row padding-top-bottom">
              <center>
                <img class="profile-user-img img-responsive" src="{{ (isset($foto_destinasi) ? $foto_destinasi : url('images/destination/default.png')) }}" alt="User profile picture" width="60%" id="fotonya">
              </center>
            </div>
            <form action="{{ (isset($id) ? url('/admin/destination/upload/'.$id) : url('/admin/destination/upload/new')) }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                {{ csrf_field() }}
                <div class="dz-message">
                  <div>
                    <h5>Letakkan foto destinasi disini atau klik untuk mengunggah.</h5>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
  <script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
    });
  </script>

  <script>
    Dropzone.options.myDropzone = {
        paramName: 'file',
        maxFilesize: 5, // MB
        maxFiles: 20,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        init: function() {
            this.on("success", function(file, response) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = response;
                  var lokasi = aa.innerHTML;
                  $('#fotonya').attr('src', "{{ asset('/images') }}" + "/" + lokasi);
            }),
            this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 20 Gambar");
            }),
            this.on("removedfile", function(file) {
                var name   = file.previewElement.querySelector("[data-dz-name]");
                var lokasi = name.innerHTML;
                var _token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: 'POST',
                    url: "{{ url('/admin/destination/remove') }}",
                    data: { lokasi, _token },
                    dataType: 'html',
                });
                $('#fotonya').attr('src', "{{ asset('/images/destination/default.png') }}");
            }),
            this.on("addedfile", function(file) {
              var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
              var _this = this;

              removeButton.addEventListener("click", function(e) {
                  e.preventDefault();
                  e.stopPropagation();
                  var conf = confirm("Apakah anda yakin menghapus foto ini?");
                  if (conf == true) {
                     _this.removeFile(file);
                }
              });

              file.previewElement.appendChild(removeButton);
            });
        }
    };
</script>
@endsection
