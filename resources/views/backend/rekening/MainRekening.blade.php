@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">

    <div class="row">
      <div class="col-md-12">

        @if(session()->has('info'))
            <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
               </button>
                {!! session()->get('info') !!}
             </div>
             <script type="text/javascript">
                 $(document).ready(function(){
                   destroy();
                 });
             </script>
        @endif

        <div class="x_panel">
          <div class="x_title">
            <h2>Rekening<small>Daftar</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#form" onclick="formRekening('')"><i class="fa fa-plus"></i> Rekening Baru</button>
              </li>
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            {!! $_table !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  
{!! $_modal !!}

@endsection
