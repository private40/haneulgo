@extends('backend/main/HeadLayout')

@section('content')
<div class="right_col" role="main">
  <br />
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3> Gallery <small> List</small> </h3>
      </div>
    </div>

    <div class="clearfix"></div>

    @if(session()->has('info'))
        <div class="alert alert-{{ session()->get('kelas') }} alert-dismissible fade in" role="alert" id="pemberitahuan">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
           </button>
            {!! session()->get('info') !!}
         </div>
         <script type="text/javascript">
             $(document).ready(function(){
               destroy();
             });
         </script>
    @endif

    <div class="alert alert-info alert-dismissible fade in" role="alert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
       </button>
          <p>- Anda bisa mengisi nama foto dan jenis sebelum mengupload foto gallery. Nama foto dan jenis foto akan tersimpan secara otomatis dengan foto yang Anda upload.</p>
          <p>- Fitur ini juga bisa digunakan apabila Anda ingin mengisikan nama foto dan jenis yang berbeda tanpa memuat ulang halaman.</p>

    </div>


    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Upload <small> New Gallery </small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">
              <div class="col-xs-3">
                <div class="form-group">
                  <label class="control-label" for="first-name">Nama Foto
                  </label>
                    {!! Form::text('nama', '', ['class' => 'form-control', 'placeholder' => 'Nama Foto', 'id' => 'nama']) !!}
                </div>
                <div class="form-group">
                  <label class="control-label" for="first-name">Jenis Foto
                  </label>
                    {!! Form::text('jns', 'Traveling', ['class' => 'form-control', 'placeholder' => 'Jenis Foto', 'id' => 'jns', 'maxlength' => "15"]) !!}
                </div>
              </div>
              <div class="col-xs-9">
                <form action="{{ url('/admin/gallery/upload') }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                  {{ csrf_field() }}
                  {!! Form::hidden('nama_galeri', '', ['id' => 'nama_galeri_form']) !!}
                  {!! Form::hidden('jenis', 'Traveling', ['id' => 'jenis_form']) !!}
                  <div class="dz-message">
                    <div>
                      <h5>Letakkan foto galeri disini atau klik untuk mengunggah.</h5>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gallery <small> List</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">
              @foreach($galleryList as $gallery)
              <div class="col-md-55">
                <div class="thumbnail">
                  <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="{{ $thumbGallery[$gallery->id] }}" alt="{{ $gallery->nama_galeri }}" id="img_{{ $gallery->id }}"/>
                    <div class="mask">
                      <p>&nbsp;</p>
                      <div class="tools tools-bottom">
                        <a href="#" data-toggle="modal" data-target="#show" onclick="showGallery('{{ $gallery->id }}', '{{ $gallery->foto_galeri }}')"><i class="fa fa-eye"></i></a>
                        <a href="#" data-toggle="modal" data-target="#form" onclick="formGallery('{{ $gallery->id }}')"><i class="fa fa-pencil"></i></a>
                        <a href="#" data-toggle="modal" data-target="#delete" onclick="deleteGallery('{{ $gallery->id }}', '{{ $gallery->foto_galeri }}')"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="caption">
                    <p>{{ $gallery->nama_galeri }}</p>
                  </div>
                </div>
              </div>
              @endforeach
            
          </div>
          
          
          <div class="row">
               <center>
                {!! $galleryList->links() !!}
               </center>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {!! $_modal !!}

  <script type="text/javascript">
      $(document).ready(function(){
        $("#nama").change(function(){
            var nama = $('#nama').val();
            $('#nama_galeri_form').val(nama);
        });

        $("#jns").change(function(){
            var jenis = $('#jns').val();
            $('#jenis_form').val(jenis);
        });
      });
  </script>

  <script>
    Dropzone.options.myDropzone = {
        paramName: 'file',
        maxFilesize: 9, // MB
        maxFiles: 35,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        init: function() {
            this.on("success", function(file, response) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = response;
                  var lokasi = aa.innerHTML;
                  $('#fotonya').attr('src', "{{ asset('/images') }}" + "/" + lokasi);
            }),
            this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 35 Gambar");
            }),
            this.on("removedfile", function(file) {
                var name   = file.previewElement.querySelector("[data-dz-name]");
                var lokasi = name.innerHTML;
                var _token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: 'POST',
                    url: "{{ url('/admin/package/remove') }}",
                    data: { lokasi, _token },
                    dataType: 'html',
                });
            }),
            this.on("addedfile", function(file) {
              var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
              var _this = this;

              removeButton.addEventListener("click", function(e) {
                  e.preventDefault();
                  e.stopPropagation();
                  var conf = confirm("Apakah anda yakin menghapus foto ini?");
                  if (conf == true) {
                     _this.removeFile(file);
                }
              });

              file.previewElement.appendChild(removeButton);
            });
        }
    };
</script>
@endsection
