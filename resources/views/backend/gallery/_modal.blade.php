<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title">Hapus <span class="title_gallery"></span> </h4>
       </div>
       <div class="modal-body">
         Apakah benar Anda ingin menghapus foto <strong class="title_gallery"></strong>?
       </div>
       <div class="modal-footer">
           {!! Form::open(['action' => 'GalleryController@remove', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
           {!! Form::hidden('lokasi', '', ['class' => 'form-control', 'placeholder' => 'Lokasi', 'id' => 'lokasi']) !!}

           <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
           <button type="submit" class="btn btn-danger">Delete</button>
           {!! Form::close() !!}
       </div>
     </div>
   </div>
 </div>
 <div class="modal fade" id="show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Lihat <span class="title_gallery"></span> </h4>
     </div>
     <div class="modal-body">
         <div class="row">
             <img href="{{ asset('images/gallery/default.jpg') }}" id="img_gallery" style="width: 100%"/>
         </div>
     </div>
   </div>
 </div>
</div>

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Ubah keterangan - <span class="title_gallery"></span> </h4>
     </div>
     {!! Form::open(['action' => 'GalleryController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
     <div class="modal-body">
       <div class="row">
         <div class="col-sm-12">
             <div class="form-group">
               <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Foto
               </label>
               <div class="col-md-9 col-sm-9 col-xs-12">
                 {!! Form::text('nama_galeri', '', ['class' => 'form-control', 'placeholder' => 'Nama Foto', 'id' => 'nama_galeri']) !!}
                 {!! Form::hidden('id', '', ['class' => 'form-control', 'placeholder' => 'id', 'id' => 'id']) !!}
               </div>
             </div>
             <div class="form-group">
               <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Foto
               </label>
               <div class="col-md-9 col-sm-9 col-xs-12">
                 {!! Form::text('jenis', '', ['class' => 'form-control', 'placeholder' => 'Jenis Foto', 'id' => 'jenis', 'maxlength' => "15"]) !!}
               </div>
             </div>
         </div>
       </div>
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
       <button type="submit" class="btn btn-success">Simpan</button>
     </div>
     {!! Form::close() !!}
   </div>
 </div>
</div>
