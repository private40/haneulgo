@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">{{ $product->nama_produk }}</h1>
                        <p>{{ $productCategory }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="company-information" class="choose">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="row">
                    <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                        <img src="{{ $productPhoto[0] }}" alt="{{ $product->nama_produk }}" width="100%">
                    </div>
                    <div class="col-sm-8 padding-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0ms">
                        <h2>{{ $product->nama_produk }}</h2>
                        <p>Daftar Destinasi</p>
                        <div class="row padding-top">
                            <div class="col-sm-9">
                                <h2>Rp. {{ number_format($product->harga_produk,0,'','.') }},- <small>/pcs</small></h2>
                            </div>
        
                            <div class="col-sm-3 pull-right">
                                <!-- <button class="btn btn-common uppercase" data-toggle="modal" data-target="#pesan">Pesan</button> -->
                                <a href="{{ url('order_travel/'.$product->id) }}" class="btn btn-common uppercase">Pesan</a>
                            </div>
                        </div>
                        <p class="padding-top">* Anda diharapkan membaca syarat dan ketentuan travel.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="sidebar portfolio-sidebar">
                    <div class="sidebar-item categories">
                        <h3>Kategori Produk</h3>
                        <ul class="nav navbar-stacked">
                            <li class="active"><a href="{{ url('/dev/shop.html') }}">All<span class="pull-right">({{ $kategoriCount['all'] }})</span></a></li>
                            
                            @foreach($kategori as $kategori)
                            <li><a href="{{ url('/dev/shop/html?kategori='.$kategori->nama_kategori) }}">{{ $kategori->nama_kategori }}<span class="pull-right">({{ $kategoriCount[$kategori->id] }})</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
