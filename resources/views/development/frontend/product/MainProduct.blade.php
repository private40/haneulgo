@extends('frontend/main/HeadLayout')

@section('content')
<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-6">
                        <h1 class="title">Haneul Shop</h1>
                        <p>Merchandise asli Korea Selatan.</p>
                    </div>
                    <div class="col-sm-6" style="padding: 20px 0">
                        {!! Form::open(['action' => 'DevelopmentController@shopProduct', 'method' => 'get', 'class' => 'form-horizontal form-label-left']) !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i> </span>
                                {!! Form::text('search', '', ['class' => 'form-control', 'placeholder' => 'Cari produk']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#action-->

<section id="projects" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="sidebar portfolio-sidebar">
                        <div class="sidebar-item categories">
                            <h3>Kategori Produk</h3>
                            <ul class="nav navbar-stacked">
                                <li class="active"><a href="{{ url('/dev/shop.html') }}">All<span class="pull-right">({{ $kategoriCount['all'] }})</span></a></li>
                                
                                @foreach($kategori as $kategori)
                                <li><a href="{{ url('/dev/shop/html?kategori='.$kategori->nama_kategori) }}">{{ $kategori->nama_kategori }}<span class="pull-right">({{ $kategoriCount[$kategori->id] }})</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    @if(isset($search))
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Hasil pencarian <strong>"{{ $search }}"</strong></h3>.
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        @if($productCount == 0)
                            <div class="col-xs-12">
                                <div class="alert alert-warning">
                                     <center><span>Produk <strong>"{{ $search }}"</strong> tidak ditemukan</span>.</center>
                                </div>
                            </div>
                            
                            <div class="sidebar portfolio-sidebar">
                                <div class="sidebar-item categories">
                                    <h3>Produk Lainnya</h3>
                                </div>
                            </div>
                            
                            @foreach($recomend as $product)
                            <div class="col-xs-6 col-sm-6 col-md-3 portfolio-item branded logos">
                                <div class="portfolio-wrapper">
                                    <div class="portfolio-single">
                                        <div class="portfolio-thumb">
                                            <img src="{{ $productPhoto[$product->id] }}" class="img-responsive" alt="{{ $product->nama_produk }}">
                                        </div>
                                        <div class="portfolio-view">
                                            <ul class="nav nav-boxs">
                                                <li><a href="{{ url('/dev/shop/product/'.$product->id) }}"> Quickview</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-info ">
                                        <h2>{{ $product->nama_produk }}</h2>
                                        <p>Rp. {{ number_format($product->harga_produk,0,'','.') }},-</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                        
                        @foreach($productList as $product)
                        <div class="col-xs-6 col-sm-6 col-md-3 portfolio-item branded logos">
                            <div class="portfolio-wrapper">
                                <div class="portfolio-single">
                                    <div class="portfolio-thumb">
                                        <img src="{{ $productPhoto[$product->id] }}" class="img-responsive" alt="{{ $product->nama_produk }}">
                                    </div>
                                    <div class="portfolio-view">
                                        <ul class="nav nav-boxs">
                                            <li><a href="{{ url('/dev/shop/product/'.$product->id) }}"> Quickview</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="portfolio-info ">
                                    <h2>{{ $product->nama_produk }}</h2>
                                    <p>Rp. {{ number_format($product->harga_produk,0,'','.') }},-</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="portfolio-pagination">
                             {!! $productList->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
