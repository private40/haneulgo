<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus <span class="span_name"></span> </h4>
        </div>
        <div class="modal-body">
          Apakah benar Anda ingin menghapus kategori <strong class="span_name"></strong>?
        </div>
        <div class="modal-footer">
            <a href="<?= url('admin.html') ?>" class="btn btn-danger" id="konfirm_delete"><i class="fa fa-check-times"></i> Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Cancel</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="judul">Kategori Baru</h4>
        </div>
        {!! Form::open(['action' => 'ProductCategoryController@store', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Kategori
                  </label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('nama_kategori', '', ['class' => 'form-control', 'placeholder' => 'Nama Kategori', 'id' => 'nama_kategori']) !!}
                    {!! Form::hidden('id', '', ['class' => 'form-control', 'placeholder' => 'id', 'id' => 'id']) !!}
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>