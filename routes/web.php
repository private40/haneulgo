<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get ('/',                             'MainController@index');
Route::get ('/packages.html',                'MainController@packages');
Route::get ('/packages_detail/{kode_paket}', 'PackageController@show');
Route::get ('/shop.html',                    'MainController@comingSoon');
Route::get ('/money.html',                   'MainController@comingSoon');
Route::get ('/gallery.html',                 'GalleryController@index');
Route::get ('/main/{page}.html',             'MainController@page');
Route::post('/destroy',                      'AdminController@destroyFlash');

//login with google
Route::get('login/google',                   'LoginController@redirectToProvider');
Route::get('login/google/callback',          'LoginController@handleProviderCallback');

//login and friends
Route::get ('/activation.html',              'MainController@activeMsg');
Route::get ('/activation/{kode_unik}',       'MainController@activation');
Route::get ('/resend/verification',          'PenggunaController@resendVerification');
Route::get ('/register.html',                'MainController@register');
Route::get ('/login.html',                   'MainController@login');
Route::get ('/forget_password.html',         'MainController@forgetPassword');
Route::post('/signin',                       'PenggunaController@login');
Route::post('/admin/check_user',             'PenggunaController@checkUsername');
Route::post('/pengguna/register',            'PenggunaController@register');
Route::post('/pengguna/forget',              'PenggunaController@forgetPassword');

Route::middleware('pengguna')->group(function () {
    //data pengguna
    Route::post('/pengguna/simpan',                       'PenggunaController@update');
    Route::get ('/pengguna/order_list',                   'OrderController@orderByPengguna');
    Route::post('/admin/pengguna/upload/{username}',      'PenggunaController@upload');
    Route::post('/admin/pengguna/remove',                 'PenggunaController@remove');
    Route::post('/admin/message/store',                   'MessageController@store');
    Route::post('/admin/message/send',                    'MessageController@sendMessage');
    Route::post('/admin/read_message',                    'MessageController@read');

    //order pengguna
    Route::get ('/order_travel/{kode_paket}',             'OrderController@order');
    Route::post('/order/input',                           'OrderController@store');
    Route::post('/order/pay',                             'OrderController@pay');
    Route::get ('/order/{kode_order}',                    'OrderController@showOrder');
    Route::get ('/pay_order/{kode_order}',                'OrderController@payOrder');
    Route::post('/calculate',                             'OrderController@calculate');

    //testimoni pengguna
    Route::post('/testimony/save',                        'TestimonieController@store');
    Route::post('/testimony/detail',                      'TestimonieController@detail');
    Route::get ('/testimony/delete/{id}',                 'TestimonieController@destroy');

    //menu pengguna
    Route::get ('/pengguna/{username}',                   'PenggunaController@showPengguna');
    Route::get ('/pengguna/{username}/message',           'PenggunaController@showMessage');
    Route::get ('/pengguna/{username}/order',             'PenggunaController@showOrder');
    Route::get ('/pengguna/{username}/testimony',         'PenggunaController@showTestimony');
});

Route::middleware('admin')->group(function () {
    //dashboard
    Route::get ('/admin.html',                            'AdminController@index');
    Route::get ('/admin/403.html',                        'AdminController@forbidden');

    //transaction admin
    Route::get ('/admin/order_list.html',                 'OrderController@orderList');
    Route::get ('/admin/needconfirm_list.html',           'OrderController@confirmList');
    Route::get ('/admin/readytogo_list.html',             'OrderController@readyList');
    Route::get ('/admin/completed_list.html',             'OrderController@completedList');
    Route::post('/admin/order_detail',                    'OrderController@detail');
    Route::get ('/admin/order/confirmation/{kode_order}', 'OrderController@confirmation');
    Route::post('/admin/order/perbarui',                  'OrderController@update');
    Route::post('/admin/order/konfirm',                   'OrderController@updateDone');

    //pengguna admin
    Route::get ('/admin/member_list.html',                'PenggunaController@memberList');
    Route::get ('/admin/admin_list.html',                 'PenggunaController@adminList');
    Route::get ('/admin/pengguna/delete/{username}',      'PenggunaController@destroy');
    Route::get ('/admin/pengguna/form/{username}',        'PenggunaController@edit');
    Route::post('/admin/detail_pengguna',                 'PenggunaController@show');
    Route::post('/admin/pengguna/save',                   'PenggunaController@store');
    Route::post('/admin/pengguna/savePass',               'PenggunaController@storePass');
    Route::get ('/admin/profile/{username}',              'PenggunaController@edit');
    Route::get ('/admin/password.html',                   'PenggunaController@password');

    //package and feature
    Route::get ('/admin/package.html',                    'PackageController@index');
    Route::get ('/admin/package/form/{kode_paket}',       'PackageController@form');
    Route::get ('/admin/package/delete/{kode_paket}',     'PackageController@destroy');
    Route::post('/admin/package/save',                    'PackageController@packageStore');
    Route::post('/admin/package/upload/{kode_paket}',     'PackageController@upload');
    Route::post('/admin/package/remove',                  'PackageController@remove');

    Route::get ('/admin/feature.html',                    'PackageController@feature');
    Route::post('/admin/feature/save',                  'PackageController@featureStore');
    Route::post('/admin/feature/detail',                  'PackageController@featureDetail');

    //tour destination
    Route::get ('/admin/destination.html',                'DestinationController@index');
    Route::get ('/admin/destination/form/{id}',           'DestinationController@form');
    Route::get ('/admin/destination/delete/{id}',         'DestinationController@destroy');
    Route::post('/admin/destination/save',                'DestinationController@store');
    Route::post('/admin/destination/upload/{id}',         'DestinationController@upload');
    Route::post('/admin/destination/remove',              'DestinationController@remove');
    Route::post('/admin/destination/selectList',          'DestinationController@selectList');

    //kurs
    Route::get ('/admin/kurs.html',                       'KursController@index');
    Route::post('/admin/kurs/save',                       'KursController@store');

    //message and testimony
    Route::get ('/admin/message_list.html',               'MessageController@index');
    Route::get ('/admin/testimony_list.html',             'TestimonieController@index');
    Route::get ('/admin/message_list/{kode_order}',       'MessageController@show');
    Route::get ('/admin/testimony_status/{id}',           'TestimonieController@update');

    //bank account
    Route::get ('/admin/account_list.html',               'RekeningController@index');
    Route::post('/admin/rekening/save',                   'RekeningController@store');
    Route::post('/admin/rekening/detail',                 'RekeningController@show');
    Route::get ('/admin/rekening/delete/{id}',            'RekeningController@destroy');

    //gallery
    Route::get ('/admin/gallery.html',                    'GalleryController@show');
    Route::post('/admin/gallery/detail',                  'GalleryController@detail');
    Route::post('/admin/gallery/upload',                  'GalleryController@upload');
    Route::post('/admin/gallery/remove',                  'GalleryController@remove');
    Route::post('/admin/gallery/store',                   'GalleryController@store');

    //haneul info
    Route::get ('/admin/info_haneul/{page}.html',         'AdminController@pageSetting');
    Route::post('/admin/page/save',                       'AdminController@storePage');

    //haneul shop product
    Route::get ('/admin/shop/product.html',               'ProductController@product');
    Route::get ('/admin/shop/product/form/{id}',          'ProductController@form');
    Route::get ('/admin/shop/product/delete/{id}',        'ProductController@destroy');
    Route::post('/admin/shop/product/save',               'ProductController@store');
    Route::post('/admin/shop/product/upload/{id}',        'ProductController@upload');
    Route::post('/admin/shop/product/remove',             'ProductController@remove');

    //category Product
    Route::get ('/admin/shop/category.html',              'ProductCategoryController@index');
    Route::get ('/admin/shop/category/delete/{id}',       'ProductCategoryController@destroy');
    Route::post('/admin/shop/category/save',              'ProductCategoryController@store');
    Route::post('/admin/shop/category/selectList',        'ProductCategoryController@selectList');
});

Route::middleware('developer')->group(function () {
    //backend

    //frontend
    Route::get ('/dev/shop.html',                         'DevelopmentController@shopProduct');
    Route::get ('/dev/shop/product/{id}',                 'DevelopmentController@shopProductDetail');
});

Route::get('/signout', function(){
  Session::flush();
  return redirect()->to('/');
});
